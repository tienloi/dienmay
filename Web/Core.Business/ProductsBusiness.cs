﻿using AutoMapper;
using CommonHelper.DataContext;
using CommonHelper.Helper;
using Core.Models;
using Core.Models.EF;
using Core.Models.Messages;
using Core.Models.Share;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Core.Business
{
    public class ProductsBusiness
    {
        #region + Constructor

        private readonly BoopAppContext _context;
        public ProductsBusiness()
        {
            _context = new BoopAppContext();
        }
        #endregion

        #region  Method
        public async Task<IEnumerable<ProductView>> GetData(string name, DataTable filterDataTable, DataTable sortDataTable, int skip, int take, int? typeTransaction, int? typeProperty)
        {
            string methodName = "GetData";
            try
            {
                Repository<ProductView> repository = new Repository<ProductView>();
                SqlParameter[] paras =
                {
                    new SqlParameter { ParameterName = "@name",  Value = (object)name??DBNull.Value, DbType = DbType.AnsiString},
                    new SqlParameter { ParameterName = "@typeTransaction",  Value = typeTransaction.HasValue?typeTransaction.Value:0 , DbType = DbType.Int32},
                    new SqlParameter { ParameterName = "@typeProperty",  Value =  typeProperty.HasValue?typeProperty.Value: 0, DbType = DbType.Int32},
                    new SqlParameter { ParameterName =  "@filter" , Value = filterDataTable , TypeName = "FilterType" , SqlDbType = SqlDbType.Structured},
                    new SqlParameter { ParameterName = "@sort" , Value = sortDataTable , TypeName = "SortType" , SqlDbType = SqlDbType.Structured},
                    new SqlParameter { ParameterName = "@skip", Value = skip , DbType = DbType.Int32},
                    new SqlParameter {ParameterName = "@take", Value = take, DbType = DbType.Int32}
                };
                var results = await repository.Get(Constants.StoreProcedure.ProductsList, paras);
                return results;
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, methodName, ex, ex.Message);
                return null;
            }
        }

        public async Task<ResultBase<bool>> Create(ProductView model)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    var repository = new Repository<Product>();
                    var item = Mapper.Map<ProductView, Product>(model);
                    item.Id = Guid.NewGuid();
                    item.CreatedDate = DateTime.Now;
                    item.IsActive = true;
                    item.ModifiedDate = DateTime.Now;
                    item.Name = item.Name.Trim();
                    item.Address = !string.IsNullOrEmpty(item.Address) ? item.Address.Trim() : string.Empty;
                    item.Summary = !string.IsNullOrEmpty(item.Summary) ? item.Summary.Trim() : string.Empty;

                    if (model._imgs.Count > 0)
                    {
                        item.ImageList = JsonConvert.SerializeObject(model._imgs);
                    }

                    var result = await repository.InsertAndGetData(item);
                    if (result == null)
                    {
                        return new ResultBase<bool>()
                        {
                            Status = false,
                            Message = Constants.Message.SOMETHING_ERROR
                        };
                    }

                    scope.Complete();
                    return new ResultBase<bool>()
                    {
                        Status = true,
                        Message = Constants.Message.SAVED_SUCCESSFULLY
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Create(Product model)", ex);
                return new ResultBase<bool>()
                {
                    Status = false,
                    Message = ex.Message
                };
            }

        }
        public async Task<ResultBase<bool>> Update(ProductView data)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    #region + Update into 

                    var repository = new Repository<Product>();
                    var item = await repository.GetOne(x => x.Id == data.Id);

                    data.Name = data.Name.Trim();
                    data.Address = !string.IsNullOrEmpty(data.Address) ? data.Address.Trim() : string.Empty;
                    data.Summary = !string.IsNullOrEmpty(data.Summary) ? data.Summary.Trim() : string.Empty;
                    data.CreatedBy = item.CreatedBy;
                    data.CreatedDate = item.CreatedDate;
                    data.ModifiedDate = DateTime.Now;
                    data.IsActive = item.IsActive;
                    data.ImageList = JsonConvert.SerializeObject(data._imgs);

                    item = Mapper.Map(data, item);

                    var result = await repository.Edit(item);
                    if (!result) return new ResultBase<bool>()
                    {
                        Status = false
                    };

                    #endregion + Update into 

                    scope.Complete();
                    return new ResultBase<bool>()
                    {
                        Status = true,
                        Message = Constants.Message.SAVED_SUCCESSFULLY
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Update(Product data)", ex);
                return new ResultBase<bool>()
                {
                    Status = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<bool> Deletes(List<Guid> lstId)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {

                    if (lstId.Any())
                    {
                        var itemDeletes = _context.Products.Where(x => lstId.Any(p => p == x.Id));
                        if (itemDeletes.Any())
                        {
                            foreach (var item in itemDeletes)
                            {
                                item.IsDelete = true;
                            };
                            var result = await _context.SaveChangesAsync() > 0;
                            if (!result) return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                    scope.Complete();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Deletes(DeleteMasterDataParameter data)", ex);
                return false;
            }
        }
        public async Task<bool> SetStatus(List<Guid> lstId)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {

                    if (lstId.Any())
                    {
                        var items = _context.Products.Where(x => lstId.Any(p => p == x.Id));
                        if (items.Any())
                        {
                            foreach (var item in items)
                            {
                                if (item.IsActive.HasValue)
                                    item.IsActive = item.IsActive.Value ? false : true;
                                else
                                    item.IsActive = true;
                            };
                            var result = await _context.SaveChangesAsync() > 0;
                            if (!result) return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                    scope.Complete();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Deletes(DeleteMasterDataParameter data)", ex);
                return false;
            }
        }
        public async Task<bool> SetVIP(List<Guid> lstId)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {

                    if (lstId.Any())
                    {
                        var items = _context.Products.Where(x => lstId.Any(p => p == x.Id));
                        if (items.Any())
                        {
                            foreach (var item in items)
                            {
                                if (item.IsVIP.HasValue)
                                    item.IsVIP = item.IsVIP.Value ? false : true;
                                else
                                    item.IsVIP = true;
                            };
                            var result = await _context.SaveChangesAsync() > 0;
                            if (!result) return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                    scope.Complete();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Deletes(DeleteMasterDataParameter data)", ex);
                return false;
            }
        }
        public List<Product> GetTopData(int top, bool isVIP)
        {
            try
            {
                if (isVIP)
                {
                    var resultVip = _context.Products.Where(x => x.IsActive == true
                                                            && x.IsDelete.HasValue ? x.IsDelete.Value == false : x.IsDelete == null
                                                            && x.IsVIP.HasValue ? x.IsVIP.Value == false : x.IsVIP == null
                                                            ).OrderByDescending(x => x.ModifiedDate).Take(top).ToList();
                    return resultVip;
                }
                var result = _context.Products.Where(x => x.IsActive.Value == true && x.IsDelete.HasValue ? x.IsDelete.Value == false : x.IsDelete == null).OrderByDescending(x => x.ModifiedDate).Take(top).ToList();
                return result;
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Deletes(DeleteMasterDataParameter data)", ex);
                return null;
            }
        }
        public int GetTotal(bool isVIP)
        {
            try
            {
                var resultVip = _context.Products
                    .Where(c => (c.IsActive.HasValue ? c.IsActive.Value : false) == true
                    && (c.IsDelete.HasValue ? c.IsDelete.Value : false) == false
                    && (c.IsVIP.HasValue ? c.IsVIP.Value : false) == isVIP
                    ).ToList();
                    //.Where(x => x.IsActive.HasValue ? x.IsActive.Value == true : x.IsActive != null
                    //                                    && (x.IsDelete.HasValue ? x.IsDelete.Value == false : x.IsDelete == null)
                    //                                    && (x.IsVIP.HasValue ? x.IsVIP.Value == isVIP : isVIP == false)
                return resultVip.Count();
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Deletes(DeleteMasterDataParameter data)", ex);
                return 0;
            }
        }
        #endregion
    }
}
