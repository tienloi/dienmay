﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using CommonHelper.DataContext;
using CommonHelper.Helper;
using Core.Models;
using Core.Models.EF;
using Core.Models.Messages;
using Core.Models.Share;

namespace Core.Business
{
    public class ConstantBusiness
    {
        #region + Constructor

        private readonly BoopAppContext _context;
        private readonly CacheProvider _cacheProvider;
        public ConstantBusiness()
        {
            _context = new BoopAppContext();
            _cacheProvider = new CacheProvider();
        }
        #endregion

        #region  Method

        public async Task<IEnumerable<ConstantView>> GetData(string name, DataTable filterDataTable, DataTable sortDataTable, int skip, int take)
        {
            string methodName = "Constant";
            try
            {
                Repository<ConstantView> repository = new Repository<ConstantView>();
                SqlParameter[] paras =
                {
                    new SqlParameter { ParameterName = "@name",  Value = (object)name??DBNull.Value, DbType = DbType.AnsiString},
                    new SqlParameter { ParameterName =  "@filter" , Value = filterDataTable , TypeName = "FilterType" , SqlDbType = SqlDbType.Structured},
                    new SqlParameter { ParameterName = "@sort" , Value = sortDataTable , TypeName = "SortType" , SqlDbType = SqlDbType.Structured},
                    new SqlParameter { ParameterName = "@skip", Value = skip , DbType = DbType.Int32},
                    new SqlParameter {ParameterName = "@take", Value = take, DbType = DbType.Int32}
                };
                var results = await repository.Get(Constants.StoreProcedure.ConstantList, paras);
                return results;
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, methodName, ex, ex.Message);
                return null;
            }
        }
        public async Task<ResultBase<bool>> Create(ConstantView model)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    var repository = new Repository<Constant>();
                    var item = new Constant()
                    {
                        Name = model.Name.Trim(),
                        ValueString = !string.IsNullOrEmpty(model.ValueString) ? model.ValueString.Trim() : string.Empty,
                        ValueInt = model.ValueInt,
                        ParentId = model.ParentId,
                        Type = model.Type.Trim()
                    };
                    var result = await repository.Insert(item);
                    scope.Complete();
                    _cacheProvider.Set(Constants.CacheConstant.ConstantTable, null, Constants.CacheConstant.CacheTimeDefault);
                    if (!result)
                    {
                        return new ResultBase<bool>()
                        {
                            Status = false,
                            Message = Constants.Message.SOMETHING_ERROR
                        };
                    }
                    return new ResultBase<bool>()
                    {
                        Status = true,
                        Message = Constants.Message.SAVED_SUCCESSFULLY
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Create(Orders model)", ex);
                return new ResultBase<bool>()
                {
                    Status = false,
                    Message = ex.Message
                };
            }

        }
        public async Task<ResultBase<bool>> Update(ConstantView model)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    var repository = new Repository<Constant>();

                    var obj = await repository.GetById(model.Id);
                    obj.ValueString = !string.IsNullOrEmpty(model.ValueString) ? model.ValueString.Trim() : string.Empty;
                    obj.ValueInt = model.ValueInt;
                    obj.ParentId = model.ParentId;
                    obj.Type = model.Type.Trim();
                    obj.Name = model.Name.Trim();
                    var result = await repository.Edit(obj);
                    scope.Complete();
                    _cacheProvider.Remove(Constants.CacheConstant.ConstantTable);
                    return new ResultBase<bool>()
                    {
                        Status = true,
                        Message = Constants.Message.SAVED_SUCCESSFULLY
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Create(Orders model)", ex);
                return new ResultBase<bool>()
                {
                    Status = false,
                    Message = ex.Message
                };
            }

        }
        public Constant GetByName(string name)
        {
            try
            {

                //                var result = new Constant();
                //                var getCache = _cacheProvider.Get(Constants.CacheConstant.ConstantTable) as IEnumerable<Constant>;
                //                if (getCache == null)
                //                {
                //                    var list = _context.Constant.ToList();
                //                    _cacheProvider.Set(Constants.CacheConstant.ConstantTable, list, Constants.CacheConstant.CacheTimeDefault);
                //                    result = list.Where(x => x.Name.Equals(name)).FirstOrDefault();
                //                }
                //                else
                //                {
                //                    result = getCache.Where(x => x.Name.Equals(name)).FirstOrDefault();
                //                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "GetByName(name: " + name + ")", ex);
                return null;
            }
        }
        public async Task<IEnumerable<Constant>> GetConstants(string type)
        {
            var getCache = _cacheProvider.Get(Constants.CacheConstant.ConstantTable) as IEnumerable<Constant>;
            if (getCache == null)
            {
                var list = _context.Constant.ToList();
                _cacheProvider.Set(Constants.CacheConstant.ConstantTable, list, Constants.CacheConstant.CacheTimeDefault);
                var result = list.Where(x => x.Type.ToLower().Equals(type.ToLower())).OrderBy(x => x.ValueInt).AsEnumerable();
                return result;
            }
            else
            {
                var result = getCache.Where(x => x.Type.Trim().ToLower().Equals(type.ToLower())).OrderBy(x => x.ValueInt).AsEnumerable();
                return result;
            }
        }
        public async Task<IEnumerable<Constant>> GetConstants(string type, int id)
        {
            var getCache = _cacheProvider.Get(Constants.CacheConstant.ConstantTable) as IEnumerable<Constant>;
            if (getCache == null)
            {
                var list = _context.Constant.ToList();
                _cacheProvider.Set(Constants.CacheConstant.ConstantTable, list, Constants.CacheConstant.CacheTimeDefault);
                var result = list.Where(x => x.Type.Trim().ToLower().Equals(type.ToLower()) && x.ParentId == id).OrderBy(x => x.ValueInt).AsEnumerable();
                return result;
            }
            else
            {
                var result = getCache.Where(x => x.Type.Trim().ToLower().Equals(type.ToLower()) && x.ParentId == id).OrderBy(x => x.ValueInt).AsEnumerable();
                return result;
            }
        }
        public async Task<IEnumerable<KeyValueItem>> GetLocation(string name, int? id)
        {
            string methodName = "GetLocation";
            try
            {
                Repository<KeyValueItem> repository = new Repository<KeyValueItem>();
                SqlParameter[] paras =
                {
                    new SqlParameter { ParameterName = "@name",  Value = (object)name??DBNull.Value, DbType = DbType.AnsiString},
                    new SqlParameter {ParameterName = "@id", Value =id.HasValue?id.Value:0 , DbType = DbType.Int32}
                };
                var results = await repository.Get(Constants.StoreProcedure.GetLocation, paras);
                return results;
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, methodName, ex, ex.Message);
                return null;
            }
        }

        public async Task<IEnumerable<Contents>> GetContents()
        {
            var result = _context.Contents.AsEnumerable();
            return result;
        }
        public async Task<ResultBase<bool>> UpdateContents(Contents model)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    var repository = new Repository<Contents>();

                    var obj = await repository.GetById(model.Id);
                    if (obj == null)
                    {
                        await repository.Insert(model);
                        scope.Complete();
                        return new ResultBase<bool>()
                        {
                            Status = true,
                            Message = Constants.Message.SAVED_SUCCESSFULLY
                        };
                    }
                    obj.Content = model.Content;
                    var result = await repository.Edit(obj);
                    scope.Complete();
                    return new ResultBase<bool>()
                    {
                        Status = true,
                        Message = Constants.Message.SAVED_SUCCESSFULLY
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "UpdateContents(Orders model)", ex);
                return new ResultBase<bool>()
                {
                    Status = false,
                    Message = ex.Message
                };
            }
        }
        #endregion
    }
}
