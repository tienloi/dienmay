﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using AutoMapper;
using CommonHelper.DataContext;
using CommonHelper.Helper;
using Core.Models;
using Core.Models.EF;
using Core.Models.Messages;
using Core.Models.Share;

namespace Core.Business
{
    public class FilesBusiness
    {
        #region + Constructor

        private readonly BoopAppContext _context;
        public FilesBusiness()
        {
            _context = new BoopAppContext();
        }
        #endregion

        #region  Method

        public async Task<IEnumerable<Files>> GetData(string name, int idFolder, DataTable filterDataTable, DataTable sortDataTable, int skip, int take)
        {
            string methodName = "FilesBusiness";
            try
            {
                Repository<Files> repository = new Repository<Files>();
                SqlParameter[] paras =
                {
                    new SqlParameter { ParameterName = "@idFolder",  Value = idFolder, DbType = DbType.Int32},
                    new SqlParameter { ParameterName = "@type",  Value = (int)Enums.FileType.Images, DbType = DbType.Int32},
                    new SqlParameter { ParameterName = "@name",  Value = (object)name??DBNull.Value, DbType = DbType.AnsiString},
                    new SqlParameter { ParameterName =  "@filter" , Value = filterDataTable , TypeName = "FilterType" , SqlDbType = SqlDbType.Structured},
                    new SqlParameter { ParameterName = "@sort" , Value = sortDataTable , TypeName = "SortType" , SqlDbType = SqlDbType.Structured},
                    new SqlParameter { ParameterName = "@skip", Value = skip , DbType = DbType.Int32},
                    new SqlParameter {ParameterName = "@take", Value = take, DbType = DbType.Int32}
                };
                var results = await repository.Get(Constants.StoreProcedure.FilesList, paras);
                return results;
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, methodName, ex, ex.Message);
                return null;
            }
        }
        public async Task<ResultBase<bool>> Create(List<Files> lstFiles)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    var repository = new Repository<Files>();
                    foreach (var model in lstFiles)
                    {
                        var result = await repository.Insert(model);
                        if (!result)
                        {
                            return new ResultBase<bool>()
                            {
                                Status = false,
                                Message = Constants.Message.SOMETHING_ERROR
                            };
                        }
                    }

                    scope.Complete();
                    return new ResultBase<bool>()
                    {
                        Status = true,
                        Message = Constants.Message.SAVED_SUCCESSFULLY
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Create(Orders model)", ex);
                return new ResultBase<bool>()
                {
                    Status = false,
                    Message = ex.Message
                };
            }

        }

        public async Task<ResultBase<bool>> DeleteFile(int id)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    var repository = new Repository<Files>();

                    var item = await repository.GetOne(x => x.Id == id);
                    if (item != null)
                    {
                        item.Active = false;
                    }
                    var result = await repository.Edit(item);
                    if (!result)
                    {
                        return new ResultBase<bool>()
                        {
                            Status = false,
                            Message = Constants.Message.SOMETHING_ERROR
                        };
                    }
                    scope.Complete();
                    return new ResultBase<bool>()
                    {
                        Status = true,
                        Message = Constants.Message.SAVED_SUCCESSFULLY
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Create(Orders model)", ex);
                return new ResultBase<bool>()
                {
                    Status = false,
                    Message = ex.Message
                };
            }
        }
        public async Task<IEnumerable<Folder>> GetFolder()
        {
            var result = _context.Folders.Where(x => x.IsActive == true).OrderBy(x=>x.CreatedDate);
            return result;
        }
        public async Task<ResultBase<bool>> CreateFolder(Folder model)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    var repository = new Repository<Folder>();
                    model.IsActive = true;
                    model.CreatedDate = DateTime.Now;
                    var result = await repository.InsertAndGetData(model);
                    if (result==null)
                    {
                        return new ResultBase<bool>()
                        {
                            Status = false,
                            Message = Constants.Message.SOMETHING_ERROR
                        };
                    }

                    scope.Complete();
                    return new ResultBase<bool>()
                    {
                        Status = true,
                        IdReturn = result.Id,
                        Message = Constants.Message.SAVED_SUCCESSFULLY
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Create(Folder model)", ex);
                return new ResultBase<bool>()
                {
                    Status = false,
                    Message = ex.Message
                };
            }

        }
        #endregion
    }
}
