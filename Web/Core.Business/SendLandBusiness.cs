﻿using CommonHelper.DataContext;
using CommonHelper.Helper;
using Core.Models;
using Core.Models.EF;
using Core.Models.Share;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Core.Business
{
    public class SendLandBusiness
    {
        #region + Constructor

        private readonly BoopAppContext _context;
        public SendLandBusiness()
        {
            _context = new BoopAppContext();
        }
        #endregion

        #region  Method

        public async Task<IEnumerable<SendLand>> GetData(string name, DataTable filterDataTable, DataTable sortDataTable, int skip, int take)
        {
            string methodName = "OrdersView";
            try
            {
                Repository<SendLand> repository = new Repository<SendLand>();
                SqlParameter[] paras =
                {
                    new SqlParameter { ParameterName = "@name",  Value = (object)name??DBNull.Value, DbType = DbType.AnsiString},
                    new SqlParameter { ParameterName =  "@filter" , Value = filterDataTable , TypeName = "FilterType" , SqlDbType = SqlDbType.Structured},
                    new SqlParameter { ParameterName = "@sort" , Value = sortDataTable , TypeName = "SortType" , SqlDbType = SqlDbType.Structured},
                    new SqlParameter { ParameterName = "@skip", Value = skip , DbType = DbType.Int32},
                    new SqlParameter {ParameterName = "@take", Value = take, DbType = DbType.Int32}
                };
                var results = await repository.Get(Constants.StoreProcedure.ServicesList, paras);
                return results;
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, methodName, ex, ex.Message);
                return null;
            }
        }

        public async Task<ResultBase<bool>> Create(SendLand model)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    var repository = new Repository<SendLand>();
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;
                    var result = await repository.Insert(model);
                   
                    if (!result)
                    {
                        return new ResultBase<bool>()
                        {
                            Status = false,
                            Message = Constants.Message.SOMETHING_ERROR
                        };
                    }
                    scope.Complete();
                    return new ResultBase<bool>()
                    {
                        Status = true,
                        Message = Constants.Message.SAVED_SUCCESSFULLY
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Create(Services model)", ex);
                return new ResultBase<bool>()
                {
                    Status = false,
                    Message = ex.Message
                };
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<ResultBase<bool>> Update(SendLand data)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    #region + Update into 

                    var repository = new Repository<SendLand>();
                    var item = await repository.GetOne(x => x.Id == data.Id);

                    item.Name = data.Name.Trim();
                    item.ModifiedDate = DateTime.Now;

                    var result = await repository.Edit(item);
                    if (!result) return new ResultBase<bool>()
                    {
                        Status = false
                    };

                    #endregion + Update into 

                    scope.Complete();
                    return new ResultBase<bool>()
                    {
                        Status = true,
                        Message = Constants.Message.SAVED_SUCCESSFULLY
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Update(Orders data)", ex);
                return new ResultBase<bool>()
                {
                    Status = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<bool> Deletes(List<Guid> lstId)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {

                    if (lstId.Any())
                    {
                        var itemDeletes = _context.Services.Where(x => lstId.Any(p => p == x.Id));
                        if (itemDeletes.Any())
                        {
                            foreach (var item in itemDeletes)
                            {
                                item.Active = false;
                            };
                            var result = await _context.SaveChangesAsync() > 0;
                            if (!result) return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                    scope.Complete();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Deletes(DeleteMasterDataParameter data)", ex);
                return false;
            }
        }


        #endregion
    }
}
