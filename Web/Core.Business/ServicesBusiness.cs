﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using AutoMapper;
using CommonHelper.DataContext;
using CommonHelper.Helper;
using Core.Models;
using Core.Models.EF;
using Core.Models.Messages;
using Core.Models.Share;

namespace Core.Business
{
    public class ServicesBusiness
    {
        #region + Constructor

        private readonly BoopAppContext _context;
        public ServicesBusiness()
        {
            _context = new BoopAppContext();
        }
        #endregion

        #region  Method
        public List<Services> GetAllData()
        {
            return _context.Services.Where(x => x.Active).OrderByDescending(x => x.ModifiedDate).ToList();
        }
        public Services GetDataById(Guid id)
        {
            return _context.Services.Where(x => x.Id == id).FirstOrDefault();
        }

        public async Task<IEnumerable<ServicesView>> GetData( string name, DataTable filterDataTable, DataTable sortDataTable, int skip, int take)
        {
            string methodName = "OrdersView";
            try
            {
                Repository<ServicesView> repository = new Repository<ServicesView>();
                SqlParameter[] paras =
                {
                    new SqlParameter { ParameterName = "@name",  Value = (object)name??DBNull.Value, DbType = DbType.AnsiString},
                    new SqlParameter { ParameterName =  "@filter" , Value = filterDataTable , TypeName = "FilterType" , SqlDbType = SqlDbType.Structured},
                    new SqlParameter { ParameterName = "@sort" , Value = sortDataTable , TypeName = "SortType" , SqlDbType = SqlDbType.Structured},
                    new SqlParameter { ParameterName = "@skip", Value = skip , DbType = DbType.Int32},
                    new SqlParameter {ParameterName = "@take", Value = take, DbType = DbType.Int32}
                };
                var results = await repository.Get(Constants.StoreProcedure.ServicesList, paras);
                return results;
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, methodName, ex, ex.Message);
                return null;
            }
        }

        public async Task<ResultBase<bool>> Create(ServicesView model)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    var repository = new Repository<Services>();
                    var item = new Services();
                    item.Id = model.Id;
                    item.Name = model.Name;
                    item.NumberView = model.NumberView;
                    item.Orders = model.Orders;
                    item.Image = model.Image;
                    item.Summary = !string.IsNullOrEmpty(model.Summary) ? model.Summary.Trim() : string.Empty;
                    item.Title = !string.IsNullOrEmpty(model.Title) ? model.Title.Trim() : string.Empty;
                    item.NumberView = model.NumberView;
                    item.CreatedDate = DateTime.Now;
                    item.ModifiedDate = DateTime.Now;
                    item.Active = true;
                    var result = await repository.Insert(item);
                    scope.Complete();
                    if (!result)
                    {
                        return new ResultBase<bool>()
                        {
                            Status = false,
                            Message = Constants.Message.SOMETHING_ERROR
                        };
                    }
                    return new ResultBase<bool>()
                    {
                        Status = true,
                        Message = Constants.Message.SAVED_SUCCESSFULLY
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Create(Services model)", ex);
                return new ResultBase<bool>()
                {
                    Status = false,
                    Message = ex.Message
                };
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<ResultBase<bool>> Update(Services data)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    #region + Update into 

                    var repository = new Repository<Services>();
                    var item = await repository.GetOne(x => x.Id == data.Id);

                    item.Name = data.Name.Trim();
                    item.Image = data.Image.Trim();
                    item.ModifiedDate = DateTime.Now;
                    item.Orders = data.Orders;
                    item.Summary = !string.IsNullOrEmpty(data.Summary) ? data.Summary.Trim() : string.Empty;
                    item.Title = !string.IsNullOrEmpty(data.Title) ? data.Title.Trim() : string.Empty;
                    //item.Active = data.Active;

                    var result = await repository.Edit(item);
                    if (!result) return new ResultBase<bool>()
                    {
                        Status = false
                    };

                    #endregion + Update into 

                    scope.Complete();
                    return new ResultBase<bool>()
                    {
                        Status = true,
                        Message = Constants.Message.SAVED_SUCCESSFULLY
                    };
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Update(Orders data)", ex);
                return new ResultBase<bool>()
                {
                    Status = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<bool> Deletes(List<Guid> lstId)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {

                    if (lstId.Any())
                    {
                        var itemDeletes = _context.Services.Where(x => lstId.Any(p => p == x.Id));
                        if (itemDeletes.Any())
                        {
                            foreach (var item in itemDeletes)
                            {
                                item.Active = false;
                            };
                            var result = await _context.SaveChangesAsync() > 0;
                            if (!result) return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                    scope.Complete();
                    return true;
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Deletes(DeleteMasterDataParameter data)", ex);
                return false;
            }
        }


        #endregion
    }
}
