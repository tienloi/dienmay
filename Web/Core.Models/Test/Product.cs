namespace Core.Models.Test
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Product
    {
        public Guid Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public int TypeTransaction { get; set; }

        public int TypeProperty { get; set; }

        public int City { get; set; }

        public int District { get; set; }

        public int? Ward { get; set; }

        public int? Street { get; set; }

        public double? Area { get; set; }

        public double? Price { get; set; }

        public int? Unit { get; set; }

        [StringLength(200)]
        public string Address { get; set; }

        public int? HouseDirection { get; set; }

        public int? BalconyDirection { get; set; }

        public int? NumOfFloor { get; set; }

        public int? NumOfBedroom { get; set; }

        public int? NumOfWcs { get; set; }

        [StringLength(500)]
        public string Summary { get; set; }

        public DateTime? CreatedDate { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        [StringLength(50)]
        public string ModifiedBy { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? DeletedDate { get; set; }
    }
}
