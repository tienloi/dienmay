﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Enums
    {
        public enum OrderStatus
        {
            Images = 1,
            Files = 2
        }
        public enum FileType
        {
            Images = 1,
            Files = 2
        }
        public enum StatusCode
        {
            Ok = 201,
            Accepted = 202,
            Unauthorized = 401,
            Forbidden = 403,
            Error = 500,
            GatewayTimeout = 503,
            TokenInValid = 190,
            BadRequest = 400,
            NotAcceptable = 406,
            InvalidData = 422
        };
    }
    public static class Constants
    {
        public static class API_Url
        {
            public const string Get_ListProducts = "/products/search";
        }
        public static class CacheConstant
        {
            public const string ConstantTable = "ConstantTable";
            public const string User = "User";
            public const string Menu1 = "Menu1";
            public const string Menu2 = "Menu2";
            public const string Menu3 = "Menu3";
            public const string VIP = "VIP";
            public const int CacheTimeDefault = 1;//60*60*4=14400; //second;
        }
        public static class StoreProcedure
        {
            public const string ProductsList = "AZ_Products_GetList";
            public const string ConstantList = "AZ_Constant_GetList";
            public const string GetLocation = "AZ_GetLocation";
            public const string FilesList = "AZ_Files_GetList";
            public const string ServicesList = "AZ_Services_GetList";
        }

        public static class Message
        {
            public const string ACCESS_IS_DENIED = "Access is denied";
            public const string EXITS_NAME = "Exits name";
            public const string GET_DATA_SUCCESSFULLY = "Get Data Successfully";

            public const string SAVED_SUCCESSFULLY = "Saved Data Successfully";
            public const string SOMETHING_ERROR = "Something Error";
        }
        public static class Config
        {
            /// <summary>
            /// Number of item in page
            /// </summary>
            public const int ItemsPerPage = 20;
            /// <summary>
            /// Image default base 64
            /// </summary>
            public const string IMAGE_DEFAULT_BASE64 = "iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAWmSURBVHhe7ZzlbiQxDIDv/d+mzKiSyowqMzNDTt+etrrrLUwySezVxlJ/dWcm4y92HMeeXw0NDSb9yengV1K+nPLRfQIg7AESgARA1gVIu+BkAckCkgWkMFTQCpILElR+CkOFlZ8AJAD1vQDXpAX09fWZgYGB7z/pOD7v89Uvwl1dXWZtbc1cXV2ZcvLx8WGOjo7MxMSEaWlpqamoTi2AwcFBc3BwUFbp5f7x+flptre3TVtbW02AUAeAGby7u2ut+J8XvL29mcXFRdPU1KQahCoA/f395v7+Prfy/77BxcWFamtQA2BoaMjgy0PIw8OD6e7uVmkJKgDg79/f30Po/vuer6+vprOzUx0EcQA9PT0G5cSQ29tb09zcrAqCKIDGxsaK4WUIKISreWN3n9eLAiBKkZCpqSk1EMQA4I9D+/1ycJ+fn9Vs2MQAsFmSlNXVVRVWIAKAXSo7VknB+lpbW8UhiABYWFiQ1P33sxmHzwXV5V4iAC4vL1UAuL6+rj8AuJ+vry8VABiE9OYsugWMjY2pUT4DkQ5JowMg+tAkW1tbom4oOoD9/X1N+jdkS10WT1/XRAdwfn6uCgCbMl/KdLlPdABEHpqE/YiL4nxdEx0AGUltIpkhjQ6g0uG6FJi6AnB2dial57LP9eVOXO4T3QL29vZUAeAwyEVxvq6JDmBlZUUVAFyiL2W63Cc6gNHRUVUAsEgXxfm6JjoAbbmg+fn5+gLAzNGSDcUUqTX1NZtd7hPdAhik1FnwT9/3+Pgoqnyx6mgNJ2LA0HAsKWIBkJcOR0lBdHR01KcFAICXl6qKYPZTAOzis31fI2YBvIjUnoDKaQ2zX2wNKM4iStGfnp6i7wtmZmZUzH5xAAyAMDBUVXQpspxH+HYjee4n6oKKA6e1KIZQpt7e3p4AlJoxoWuFOPmSroAo9d4qLOBvSwjhjthwpQaNjE3RNGv4XJhp9NNQglhunVBlAcVBckLFLjWPNby8vIjX/GRZnFUCKA4cn72xsWHw31mFM+fp6Wn13ZHFd4wKgEa82dnZQh/vzs6OWVpaMrQoZZkpuKb19fXCdWRTqa4o/pHWWF5ets5sTk5OFu5H1wwFWoyN52QZj6/fBAdApzvuBJdQTgASs8OdZGCls2msiIxtjGbvYACGh4crvuRPGMTofAPC18wqdx9meFaXRsIOCwkZvnoHwKxxLT/khXFLISDQMY8lujSGFEGEyB95BUCOhURXXjk9PfW6YyXd4aMgjOwtZ9o+J4gXALSbYqo+hXKRvEmzYjjrMuvLvQu9DXNzc94geAEQsuHu7u6u8Bkam49u4LOJirL6epeJMz4+7gVCbgDE3DEE8ydcZPYRzrJgEznhXkZGRgohJFZI2iGGMB4fB/q5ALDgxvrMQAyl2j6DyC1v+JwLQEjXY6sMqd/nPdh3BkB2UVOznRSAvP3GzgBICyT5o4E8UZETAMLOSqmFegNDbsp1b+AEQFuBrTRwXLFr3sgJgLZWU2kAPN+139gJAKmCJP9qwLXf2AlAyB1mrYJ1/e6ENQA2Hkn+1wDhqMtCbA2Ab3smKa0Bl3S1NQCSUElKa4Acla0VWAPQ0lyhcRKQEAwOYHNzU+O7qxgT2YHgAEgJJymtAZeOS2sXpO1jG5omw8nJSXgLqOf8fzXYLk3fVhaQ9gCVEXBAE3QN6O3trTYJ6vr/LpsxKwvg7DVJZQ3YFA9YtyjFOoCvZci2HThWFkCpR5LKGsBN26wDVgDSJqz69LNNR1gBODw8rD6COv+FbcGWFQCN33vTxtu2nNIKgM/eLW2K8zUekpXB1gCfRa6+XljbfWwLtTJbAKf+SaprwPZsODMAClGTVNcAzSlBXFDaBVdXPr84Pj4OAyDtgrMBsP0ae2YXlHbB2QDc3NyEsYC0C84GwPZDgJktIO2CswGgSdFmEf4NLpx2U15ikSsAAAAASUVORK5CYII=";

        }
    }
}
