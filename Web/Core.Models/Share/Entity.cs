﻿using System;

namespace Core.Models.Share
{
    public abstract class Entity
    {
        public virtual int Id { get; set; }

        public virtual int Orders { get; set; }

        public virtual int UsedState { get; set; }

        public virtual DateTime? CreatedDate { get; set; }

        public virtual int? CreatedBy { get; set; }

        public virtual DateTime? ModifiedDate { get; set; }

        public virtual int? ModifiedBy { get; set; }
    }
}
