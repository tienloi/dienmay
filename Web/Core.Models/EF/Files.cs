using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace Core.Models.EF
{
    [Table("AZ_Files")]
    [Serializable]
    public partial class Files
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column(Order = 1)]
        [StringLength(500)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Link { get; set; }
        public bool Active { get; set; }
        public int? FolderId { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public int Type { get; set; }
    }

}

