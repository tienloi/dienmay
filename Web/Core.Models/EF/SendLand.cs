﻿using Core.Models.Share;
using System;
using System.ComponentModel.DataAnnotations.Schema;


namespace Core.Models.EF
{
    [Table("AZ_SendLand")]
    [Serializable]
    public class SendLand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool IsRead { get; set; }
        public string ContentSent { get; set; }
        public bool IsConfirm { get; set; }
        public string ContentConfirm { get; set; }
        public int ConfirmBy { get; set; }

        public string Time { get; set; }
        public string Address { get; set; }
        public string Area { get; set; }
        public string Floor { get; set; }
        public string Facade { get; set; }
        public string Price { get; set; }
        public string BirthYear { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
