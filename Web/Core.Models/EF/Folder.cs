﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models.EF
{
    [Table("AZ_Folder")]
    [Serializable]
    public partial class Folder
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(500)]
        public string Name { get; set; }
        public int? ParentId { get; set; }

        public bool IsActive { get; set; }
        public int Orders { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
