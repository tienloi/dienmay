using System.Collections.Generic;
using System.Reflection;
using Core.Models.EF;

namespace Core.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class BoopAppContext : DbContext
    {
        public BoopAppContext()
            : base("name=BoopAppContext")
        {
        }
        static BoopAppContext()
        {
            Database.SetInitializer<BoopAppContext>(null);
        }

        public BoopAppContext(string nameOfConnectionString)
            : base(nameOfConnectionString)
        {
        }
        private static IReadOnlyDictionary<Type, IReadOnlyCollection<PropertyInfo>> _ignoredProperties;
        public static IReadOnlyDictionary<Type, IReadOnlyCollection<PropertyInfo>> IgnoredProperties
        {
            get { return _ignoredProperties; }
        }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<SendLand> SendLands { get; set; }
        public virtual DbSet<Folder> Folders { get; set; }
        public virtual DbSet<Contents> Contents { get; set; }
        public virtual DbSet<Services> Services { get; set; }
        public virtual DbSet<Files> Files { get; set; }
        public virtual DbSet<Constant> Constant { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
