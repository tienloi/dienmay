﻿using Core.Models.Share;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models.Messages
{
    public class ProductView
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public int TypeTransaction { get; set; }
        public string TypeTransactionName { get; set; }

        public int TypeProperty { get; set; }
        public string TypePropertyName { get; set; }

        public int City { get; set; }
        public string CityName { get; set; }

        public int District { get; set; }
        public string DistrictName { get; set; }

        public int? Ward { get; set; }
        public string WardName { get; set; }

        public int? Street { get; set; }
        public string StreetName { get; set; }

        public double? Area { get; set; }

        public double? Price { get; set; } = 0;

        public int? Unit { get; set; }
        public string UnitName { get; set; }

        public string Address { get; set; }

        public int? HouseDirection { get; set; }
        public string HouseDirectionName { get; set; }

        public int? BalconyDirection { get; set; }
        public string BalconyDirectionName { get; set; }

        public int? NumOfFloor { get; set; }

        public int? NumOfBedroom { get; set; }

        public int? NumOfWcs { get; set; }

        public string Summary { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string ModifiedBy { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsDelete { get; set; }

        public DateTime? DeletedDate { get; set; }

        public string ImageMain { get; set; }
        public string ImageList { get; set; }
        public bool? IsVIP { get; set; } = false;
        public List<KeyValueItem> _imgs;
        public List<KeyValueItem> Imgs
        {
            set { _imgs = value; }
            get
            {
                var result = new List<KeyValueItem>();
                try
                {
                    result = JsonConvert.DeserializeObject<List<KeyValueItem>>(ImageList);
                }
                catch (Exception ex)
                {
                }

                return result;
            }
        }
        public int Total { get; set; }
    }
}
