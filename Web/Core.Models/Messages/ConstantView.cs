using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Models.Messages
{
    public class ConstantView
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [StringLength(20)]
        public string Type { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(100)]
        public string ValueString { get; set; }
        public int? ValueInt { get; set; }
        public int? ParentId { get; set; }
        public string ParentName { get; set; }
        public int Total { get; set; }

    }

}

