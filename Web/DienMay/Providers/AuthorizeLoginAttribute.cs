﻿using System;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace BanNhaAZ.Providers
{
    public class AuthorizeLoginAttribute : System.Web.Http.AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                base.HandleUnauthorizedRequest(actionContext);
            }
            else
            {
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.Forbidden);
            }
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class CustomAuthorizeAttribute : AuthorizationFilterAttribute
    {
        public override Task OnAuthorizationAsync(HttpActionContext actionContext, System.Threading.CancellationToken cancellationToken)
        {
            var principal = actionContext.RequestContext.Principal as ClaimsPrincipal;

            if (!principal.Identity.IsAuthenticated)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "Not allowed to access");
                return Task.FromResult<object>(null);
            }

            var userName = principal.FindFirst(ClaimTypes.Name).Value;
            //if (!CheckIpContains(ipAddress))
            //{
            //    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "Not allowed IP to access");
            //    return Task.FromResult<object>(null);
            //}
            //User is Authorized, complete execution
            return Task.FromResult<object>(null);
        }

    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class AuthorizeCheckRoleAttribute : AuthorizationFilterAttribute
    {
        public string RouterUrl { get; set; }
        public int View { get; set; }
        public int Add { get; set; }
        public int Edit { get; set; }
        public int Delete { get; set; }

        public override Task OnAuthorizationAsync(HttpActionContext actionContext, System.Threading.CancellationToken cancellationToken)
        {
            //            var service = actionContext.Request.GetDependencyScope().GetService(typeof(ISysAuthorizeService)) as ISysAuthorizeService;
            //            var webSercurityUser = actionContext.Request.GetDependencyScope().GetService(typeof(WebSercurityUser)) as WebSercurityUser;
            //            string controllerName =
            //            actionContext.ControllerContext.ControllerDescriptor.ControllerName;
            //
            //            bool check = false;
            //            var currentUser = webSercurityUser.GetCurentUser();
            //            if (!currentUser.Host)
            //            {
            //                if (View != 0)
            //                {
            //                    check = service.CheckRoleRequest(currentUser.Id, RouterUrl, View, controllerName);
            //                    if (!check)
            //                    {
            //                        actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.MethodNotAllowed, "Not ROLE to access");
            //                        return Task.FromResult<object>(null);
            //                    }
            //                }
            //                if (Add != 0)
            //                {
            //                    check = service.CheckRoleRequest(currentUser.Id, RouterUrl, Add, controllerName);
            //                    if (!check)
            //                    {
            //                        actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.MethodNotAllowed, "Not ROLE to access");
            //                        return Task.FromResult<object>(null);
            //                    }
            //                }
            //                if (Edit != 0)
            //                {
            //                    check = service.CheckRoleRequest(currentUser.Id, RouterUrl, Edit, controllerName);
            //                    if (!check)
            //                    {
            //                        actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.MethodNotAllowed, "Not ROLE to access");
            //                        return Task.FromResult<object>(null);
            //                    }
            //                }
            //                if (Delete != 0)
            //                {
            //                    check = service.CheckRoleRequest(currentUser.Id, RouterUrl, Delete, controllerName);
            //                    if (!check)
            //                    {
            //                        actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.MethodNotAllowed, "Not ROLE to access");
            //                        return Task.FromResult<object>(null);
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                check = true;
            //            }
            //
            //            if (!check)
            //            {
            //                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.MethodNotAllowed, "Not ROLE to access");
            //                return Task.FromResult<object>(null);
            //            }

            return Task.FromResult<object>(null);
        }
    }
}