﻿app.controller("LogoutController",
    function ($scope, $window, URL_LOGIN) {
        $scope.logout = function () {
            localStorage.removeItem("_token");
            $window.location.href = "http://" + $window.location.host + URL_LOGIN;
        }
    })