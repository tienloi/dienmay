app.controller("GioiThieuController",
    function ($scope, URL_HOME, BASE_URL_API, GioiThieuService) {
        var vm = $scope;
        vm.options = {
            language: 'vi',
            height: 300,
            allowedContent: true,
            entities: false,
        };
        vm.item = {
            id: 1,
            name: 'GioiThieu',
            content: ''
        }

        GioiThieuService.get().then(function (response) {
            if (response.data.result !== null && response.data.result !== undefined) {
                if (response.data.result.length > 0);
                vm.item = response.data.result[0];
            } else {
                swal("Poof! Something was wrong!", {
                    icon: "error"
                });
            }
        }, function (error) {
            swal("Poof! Something was wrong!", {
                icon: "error"
            });
        });
        vm.onSaveChange = function () {
            GioiThieuService.update(vm.item).then(function (response) {
                if (response.data.statusCode == 201) {
                    swal("Great! Your item has been updated!", {
                        icon: "success",
                        timer: 2000,
                        buttons: false
                    });
                } else {
                    swal("Poof! Something was wrong!", {
                        icon: "error"
                    });
                }
            }, function (error) {
                swal("Poof! Something was wrong!", {
                    icon: "error"
                });
            });
        }
    })