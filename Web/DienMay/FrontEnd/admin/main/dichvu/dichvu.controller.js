﻿app.controller("DichVuController",
    function ($scope, URL_HOME, BASE_URL_API, DichVuService, $interval, $window) {
        var vm = $scope;
        vm.search = {
            type: '',
            name: "",
            skip: 0,
            take: 10,
            filter: {},
            sort: {}
        };
        vm.item = {
            type: 'add'
        };
        vm.searchLink = {
            type: '',
            name: "",
            skip: 0,
            take: 10,
            filter: {},
            sort: {}
        };
        vm.tota = 0;
        vm.selectedRow = [];
        vm.pageSizeInit = 10;
        vm.options = {
            language: 'vi',
            allowedContent: true,
            entities: false,
        };

        vm.searchData = function (keyEvent) {
            if (keyEvent === undefined || keyEvent.which === 13) {
                vm.allDatasource.read();
            }
        };

        vm.allDatasource = new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    vm.search.filter = [];
                    vm.search.sort = [];
                    vm.search.skip = 0;
                    vm.search.take = vm.pageSizeInit;
                    if (options.data.filter !== null && options.data.filter !== undefined) {
                        _.each(options.data.filter.filters, function (m) {
                            var filter = {
                                field: m.field,
                                valueString: m.value,
                                isActive: true,
                            };

                            vm.search.filter.push(filter);
                        });
                    }
                    if (options.data.sort !== null && options.data.sort !== undefined) {
                        _.each(options.data.sort, function (o) {
                            var sort = {
                                field: o.field,
                                asc: o.dir === 'asc',
                                isActive: true
                            };

                            vm.search.sort.push(sort);
                        });
                    }

                    $scope.search.take = options.data.take ? options.data.take : 10000;
                    $scope.search.skip = (options.data.page - 1) * options.data.pageSize;
                    DichVuService.getList(vm.search).then(function (response) {
                        if (response.data.result !== null && response.data.result !== undefined) {
                            var rawData = response.data.result;
                            vm.total = rawData.length > 0 ? rawData[0].total : 0;

                            _.each(rawData, function (item, i) {
                                item.statusModel = {
                                    id: item.status,
                                    name: item.statusString
                                };
                            });
                            options.success(rawData);
                        } else {
                            options.success([]);
                        }

                    }, function (error) {

                        options.error([]);

                    });
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        image: {
                            type: 'string',
                            editable: false
                        },
                        title: {
                            type: 'string',
                            editable: false
                        },
                        createdDate: {
                            type: 'date',
                            editable: false
                        },
                        modifiedDate: {
                            type: 'date',
                            editable: false
                        }
                    }
                },
                total: function (response) {
                    return response === null || response === undefined || response.length === 0 ? 0 : response[0].total;
                }
            },
            pageSize: vm.pageSizeInit,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        });

        vm.mainGridOptions = {
            dataSource: vm.allDatasource,
            sortable: true,
            persistSelection: true,
            noRecords: true,
            messages: {
                noRecords: 'There is no data on current page'
            },
            pageable: {
                refresh: true,
                pageSizes: [10, 20, 50],
                buttonCount: 5
            },
            filterable: {
                extra: true,
                operators: {
                    date: {
                        gte: "Start Date",
                        lte: "End Date"
                    },
                    string: {
                        operator: "contains"
                    },
                    number: {
                        operator: "eq"
                    }
                }
            },
            change: function (e) {
                vm.selectedRow = _.map(this.select(),
                    function (row) {
                        return e.sender.dataItem(row).id;
                    });
                //$scope.$apply();
                $scope.$evalAsync();
            },
            filterMenuInit: function (e) {
                ultis.filterMenuInit(e);
            },
            //editable: "inline",
            columns: [
                {
                    selectable: true,
                    width: "50px"
                }, {
                    template: function (dataItem) {
                        return "<div><img data-ng-click='editItem(dataItem)'  style='height: 80px;cursor:pointer' src='" + dataItem.image + "'/></div>";
                    },
                    field: "file",
                    title: "Hình ảnh",
                    filterable: false,
                    width: 150
                }, {
                    field: "name",
                    title: "Tên",
                    filterable: {
                        extra: false
                    }
                }, {
                    field: "title",
                    title: "Tiêu đề",
                    template: function (dataItem) {
                        if (dataItem.title != null && dataItem.title !== "" && dataItem.title !== undefined) {
                            return dataItem.title.substring(0, 100) + ...";
                        }
                        return "(Blanks)";
                    },
                    filterable: {
                        extra: false
                    }
                }, {
                    field: "orders",
                    title: "Thứ tự",
                    width: 100,
                    filterable: {
                        extra: false
                    }
                }, {
                    field: "modifiedDate",
                    title: "Ngày sửa",
                    filterable: false,
                    //width: 300,
                    template: function (dataItem) {
                        if (!dataItem.modifiedDate) {
                            return "";
                        }
                        var date = kendo.toString(dataItem.modifiedDate, 'dd/MM/yyyy');
                        var time = kendo.toString(dataItem.modifiedDate, 'hh:mm tt');
                        return "<span class='task-name'>" + date + "</span></br><p class='d-summary-text-content' style='color:#999999;font-size:12px'>" + time + '</p';
                    }
                },
                {
                    command: {
                        text: "Edit", template: function (dataItem) {
                            return '<div style="text-align: center;">' +
                                '   <i title="Edit item" data-ng-click="editItem(dataItem)" style="font-size: 18px;cursor: pointer;" class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp' +
                                '   <i title="Delete item" data-ng-click="deleteItem(dataItem)" style="font-size: 18px;cursor: pointer;" class="fa fa-trash" aria-hidden="true"></i>&nbsp' +
                                '</div>';
                        }
                    }, title: "&nbsp;", width: 80
                }
            ]
        };
        //================= On Action command ========================
        vm.editItem = function (dataItem, tabNumber) {
            vm.type = 'edit';
            vm.tabNumber = tabNumber;
            vm.dialog.open();
            vm.item = dataItem;
        };
        vm.deleteItem = function (dataItem) {
            var lstData = [];
            lstData.push(dataItem.id);
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this item!",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willDelete) => {
                if (willDelete) {
                    DichVuService.deletes(lstData).then(
                        function (response) {
                            swal("Great! Your item has been deleted!", {
                                icon: "success",
                                timer: 2000,
                                buttons: false
                            });
                            vm.searchData();
                        },
                        function (error) {
                            swal("Poof! Something was wrong!", {
                                icon: "error"
                            });
                        }
                    );
                }
            });
        };

        //================= On Action buton ========================
        vm.onAddNew = function () {
            vm.dialog.open();
            vm.type = 'add';
            vm.item = {
                id: ultis.newGuid()
            };

        };
        vm.onDeletes = function () {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this item!",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willDelete) => {
                if (willDelete) {
                    DichVuService.deletes(vm.selectedRow).then(
                        function (response) {
                            vm.selectedRow = [];
                            swal("Great! Your item selected has been deleted!", {
                                icon: "success",
                                timer: 2000,
                                buttons: false
                            });
                            vm.searchData();
                        },
                        function (error) {
                            swal("Poof! Something was wrong!", {
                                icon: "error"
                            });
                        }
                    );
                }
            });
        };
        vm.onSaveChange = function () {
            if (!vm.validator.validate()) {
                return
            }
            var myLoop = new myLoading("btnSave", "");
            myLoop.start();
            if (vm.type === 'add') {
                DichVuService.addNew(vm.item).then(
                    function (response) {
                        vm.dialog.close();
                        myLoop.stop();
                        swal("Great! Your item has been created!", {
                            icon: "success",
                            timer: 2000,
                            buttons: false
                        });
                        vm.searchData();
                    },
                    function (error) {
                        swal("Poof! Something was wrong!", {
                            icon: "error"
                        });
                        myLoop.stop();
                    }
                );
            } else {
                DichVuService.update(vm.item).then(
                    function (response) {
                        vm.dialog.close();
                        myLoop.stop();
                        swal("Great! Your item has been updated!", {
                            icon: "success",
                            timer: 2000,
                            buttons: false
                        });
                        vm.searchData();
                    },
                    function (error) {
                        swal("Poof! Something was wrong!", {
                            icon: "error"
                        });
                        myLoop.stop();
                    }
                );
            }

        };
    });