﻿app.controller("ProductsController",
    function ($scope, URL_HOME, BASE_URL_API, ProductsService, ConstantService, $interval, $window) {
        var vm = $scope;

        vm.search = {
            type: '',
            name: "",
            skip: 0,
            take: 10,
            filter: {},
            sort: {}
        };
        vm.item = {
        };
        vm.searchLink = {
            type: '',
            name: "",
            skip: 0,
            take: 10,
            filter: {},
            sort: {}
        };
        vm.totalFile = 0;
        vm.totalLink = 0;
        vm.selectedRow = [];
        vm.pageSizeInit = 10;
        vm.searchData = function (keyEvent) {
            //  vm.typePropertySearchOptions.dataSource.read();
            if (keyEvent === undefined || keyEvent.which === 13) {
                vm.allDatasource.read();
            }
        };

        vm.allDatasource = new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    vm.search.filter = [];
                    vm.search.sort = [];
                    vm.search.skip = 0;
                    vm.search.take = vm.pageSizeInit;
                    if (options.data.filter !== null && options.data.filter !== undefined) {
                        _.each(options.data.filter.filters, function (m) {
                            var filter = {};
                            if (m.field === "statusString") {
                                filter = {
                                    field: "status",
                                    valueString: _.chain(vm.lstStatus).filter(function (x) { return x.name === m.value }).first().value().id,
                                    isActive: true,
                                };
                            } else {
                                filter = {
                                    field: m.field,
                                    valueString: m.value,
                                    isActive: true,
                                };

                            }
                            vm.search.filter.push(filter);
                        });
                    }
                    if (options.data.sort !== null && options.data.sort !== undefined) {
                        _.each(options.data.sort, function (o) {
                            var sort = {
                                field: o.field,
                                asc: o.dir === 'asc',
                                isActive: true
                            };

                            vm.search.sort.push(sort);
                        });
                    }

                    $scope.search.take = options.data.take ? options.data.take : 10000;
                    $scope.search.skip = (options.data.page - 1) * options.data.pageSize;
                    ProductsService.getList(vm.search).then(function (response) {
                        if (response.data.result !== null && response.data.result !== undefined) {
                            var rawData = response.data.result;
                            vm.totalFile = rawData.length > 0 ? rawData[0].total : 0;

                            _.each(rawData, function (item, i) {
                                item.statusModel = {
                                    id: item.status,
                                    name: item.statusString
                                };
                            });
                            options.success(rawData);
                        } else {
                            options.success([]);
                        }

                    }, function (error) {

                        options.error([]);

                    });
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        file: {
                            type: 'string',
                            editable: false
                        },
                        no: {
                            type: 'string',
                            editable: false
                        },
                        name: {
                            type: 'string',
                            editable: true
                        },
                        phone: {
                            type: 'string',
                            editable: true
                        },
                        email: {
                            type: 'string',
                            editable: true
                        },
                        address: {
                            type: 'string',
                            editable: true
                        },
                        isActive: {
                            type: 'bool',
                            editable: true
                        },
                        createdDate: {
                            type: 'date',
                            editable: false
                        }
                    }
                },
                total: function (response) {
                    return response === null || response === undefined || response.length === 0 ? 0 : response[0].total;
                }
            },
            pageSize: vm.pageSizeInit,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        });

        vm.mainGridOptions = {
            dataSource: vm.allDatasource,
            sortable: true,
            persistSelection: true,
            noRecords: true,
            messages: {
                noRecords: 'There is no data on current page'
            },
            pageable: {
                refresh: true,
                pageSizes: [10, 20, 50],
                buttonCount: 5
            },
            filterable: {
                extra: false,
                operators: {
                    date: {
                        gte: "Start Date",
                        lte: "End Date"
                    },
                    string: {
                        operator: "contains"
                    },
                    number: {
                        operator: "eq"
                    },
                    bool: {
                        operator: "eq"
                    },
                }
            },
            change: function (e) {
                var lsSelected = _.map(this.select(),
                    function (row) {
                        return e.sender.dataItem(row).id;
                    });
                //$scope.$apply();
                vm.selectedRow = _.uniq(lsSelected, itm => itm);
                $scope.$evalAsync();
            },
            filterMenuInit: function (e) {
                // ultis.filterMenuInit(e);
            },
            //editable: "inline",
            scrollable: true,
            columns: [
                {
                    selectable: true,
                    width: "50px",
                    locked: true,
                }, {
                    template: function (dataItem) {
                        if (dataItem.imageMain) {
                            return "<div class='t-img' style='cursor: pointer;' data-ng-click='editItem(dataItem)'><img  title='Click to edit'  style='height: 68px;' src='" + dataItem.imageMain + "'/></div>";
                        }
                        return "<div class='t-img'><img style='height: 68px;' src='/Images/not-found.png'/></div>";
                    },
                    field: "file",
                    title: "#",
                    locked: true,
                    filterable: false,
                    width: 150
                }, {
                    field: "name",
                    title: "Name",
                    locked: true,
                    template: function (dataItem) {
                        return `<div style='cursor: pointer;' data-ng-click='editItem(dataItem)'>
                                    <span style="font-size: 12px;color: blue;font-style: italic;">
                                    `+ dataItem.typeTransactionName + ` / ` + dataItem.typePropertyName + `
                                    </span>
                                    <br>`+ dataItem.name + `
                                </div>`
                    },
                    filterable: {
                        extra: false
                    },
                    width: 350
                }, {
                    field: "address",
                    title: "Address",
                    filterable: {
                        extra: false
                    },
                    template: function (item) {
                        return item.address + " - " + item.streetName +" - " + item.wardName + " - " + item.districtName + " - " + item.cityName;
                    },
                    width: 320
                }, {
                    field: "area",
                    title: "Area",
                    width: 100,
                    template: function (dataItem) {
                        return dataItem.area + " (m²)";
                    },
                    filterable: {
                        extra: false
                    }
                }, {
                    field: "price",
                    title: "Price",
                    width: 120,
                    filterable: {
                        extra: false
                    },
                    template: function (dataItem) {
                        return (dataItem.price ? dataItem.price : 0) + "<br> " + (dataItem.unitName ? dataItem.unitName : "");
                    }
                }, {
                    field: "numOfFloor",
                    title: "Floor",
                    width: 100,
                    filterable: {
                        extra: false
                    }
                }, {
                    field: "numOfBedroom",
                    title: "Bedroom",
                    width: 100,
                    filterable: {
                        extra: false
                    }
                }, {
                    field: "numOfWcs",
                    title: "Wc",
                    width: 100,
                    filterable: {
                        extra: false
                    }
                }, {
                    field: "createdDate",
                    title: "Created Date",
                    filterable: false,
                    width: 100,
                    template: function (dataItem) {
                        if (!dataItem.createdDate) {
                            return "";
                        }
                        var date = kendo.toString(dataItem.createdDate, 'dd/MM/yyyy');
                        var time = kendo.toString(dataItem.createdDate, 'hh:mm tt');
                        return "<span class='task-name'>" + date + "</span></br><p class='d-summary-text-content' style='color:#999999;font-size:12px'>" + time + '</p';
                    }
                }, {
                    field: "isActive",
                    title: "Status",
                    width: 100,
                    template: function (dataItem) {
                        var checked = dataItem.isActive ? "checked-span" : "";
                        var title = dataItem.isActive ? "Active" : "Not Active";
                        var checkedVIP = dataItem.isVIP ? "checked-vip" : "";
                        var titleVIP = dataItem.isVIP ? "VIP" : "Not VIP";
                        return `<div style="text-align: center;">
                                        Status
                                     <label class="switch" title="`+ title + `"  data-ng-click='activeItem(dataItem)'>
                                       <span class="slider round `+ checked + `"></span></label>
                                       <br>
                                       VIP
                                       <label class="switch" title="`+ titleVIP + `"  data-ng-click='vipItem(dataItem)'>
                                       <span class="slider round `+ checkedVIP + `"></span></label>
                                   </div>`;
                    },
                    // filterable: {
                    //     ui: cityFilter
                    // }
                    filterable: {
                        multi: true,
                        search: true,
                        dataSource: new kendo.data.DataSource({
                            transport: {
                                read: function (options) {
                                    var data = [
                                        { id: 1, name: "Active" }, { id: 0, name: "Not Active" }];
                                    options.success(data);
                                }
                            },
                        }),
                        itemTemplate: function (e) {
                            if (e.field === "all") {
                                return "<li class=\"select_all\"><label><input type='checkbox' /> <span class=\"filter-form\">#= all#</span></label></li>";
                            } else {
                                return "<li><label><input type='checkbox' name='" + e.field + "' value='#=data.id#'/> <span class=\"filter-form\">#= data.name #</span></label></li>";
                            }
                        }
                    },
                },
                {
                    command: {
                        text: "Edit", template: function (dataItem) {
                            return '<div style="text-align: center;">' +
                                '   <i title="Edit item" data-ng-click="editItem(dataItem)" style="font-size: 18px;cursor: pointer;" class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp' +
                                '   <i title="Delete item" data-ng-click="deleteItem(dataItem)" style="font-size: 18px;cursor: pointer;" class="fa fa-trash" aria-hidden="true"></i>&nbsp' +
                                '</div>';
                        }
                    }, title: "&nbsp;", width: 80
                }
            ]
        };

        //================= On Action command ========================
        vm.activeItem = function (dataItem) {
            var lstData = [];
            lstData.push(dataItem.id);
            var status = dataItem.isActive ? "Deactived" : "Actived";
            swal({
                title: "Are you sure?",
                text: "This item will set " + status + "!",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willDelete) => {
                if (willDelete) {
                    ProductsService.setStatus(lstData).then(
                        function (response) {
                            swal("Great! Your item has been " + status + "!", {
                                icon: "success",
                                timer: 2000,
                                buttons: false
                            });
                            vm.searchData();
                        },
                        function (error) {
                            swal("Error!", {
                                icon: "error"
                            });
                        }
                    );
                }
            });
        }
        vm.vipItem = function (dataItem) {
            var lstData = [];
            lstData.push(dataItem.id);
            var status = dataItem.isVIP ? "Remove VIP" : "Set VIP";
            swal({
                title: "Are you sure?",
                text: "This item will " + status + "!",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willDelete) => {
                if (willDelete) {
                    ProductsService.setVIP(lstData).then(
                        function (response) {
                            swal("Great! Your item has been " + status + "!", {
                                icon: "success",
                                timer: 2000,
                                buttons: false
                            });
                            vm.searchData();
                        },
                        function (error) {
                            swal("Error!", {
                                icon: "error"
                            });
                        }
                    );
                }
            });
        }
        vm.editItem = function (dataItem) {

            vm.type = 'edit';
            vm.dialog.open();
            vm.item = dataItem;
            if (dataItem.imgs.length == 0) vm.item.imgs = [{ id: 0, name: "" }];
            vm.onChangeTypeTransaction(dataItem.typeTransaction);
            vm.onChangeCity(dataItem.city);
            vm.onChangeDistrict(dataItem.district);
        };
        vm.deleteItem = function (dataItem) {
            var lstData = [];
            lstData.push(dataItem.id);
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this item!",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willDelete) => {
                if (willDelete) {
                    ProductsService.deletes(lstData).then(
                        function (response) {
                            swal("Great! Your item has been deleted!", {
                                icon: "success",
                                timer: 2000,
                                buttons: false
                            });
                            vm.searchData();
                        },
                        function (error) {
                            swal("Error!", {
                                icon: "error"
                            });
                        }
                    );
                }
            });
        };

        //================= On Action buton ========================
        vm.onAddNew = function () {
            vm.type = 'add';
            vm.dialog.open();
            vm.item = {
                imageMain: ''
            };
            vm.item.imgs = [{ id: 0, name: "" }];
            vm.directionOptions.dataSource.read();
            // vm.idForm.$setPristine();
        };
        vm.pushImage = function () {
            vm.item.imgs.push({ id: _.max(vm.item.imgs.map((rec) => { return rec.id + 1 })), name: "" });
        }
        vm.deleteImage = function (id) {
            vm.item.imgs = _.without(vm.item.imgs, _.findWhere(vm.item.imgs, {
                id: id
            }));
        }
        vm.onDeletes = function () {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this item!",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willDelete) => {
                if (willDelete) {
                    ProductsService.deletes(vm.selectedRow).then(
                        function (response) {
                            vm.selectedRow = [];
                            swal("Great! Your item selected has been deleted!", {
                                icon: "success",
                                timer: 2000,
                                buttons: false
                            });
                            vm.searchData();
                        },
                        function (error) {
                            swal("Poof! Something was wrong!", {
                                icon: "error"
                            });
                        }
                    );
                }
            });
        };
        vm.onSaveChange = function () {
            var myLoop = new myLoading("btnSave", "");
            myLoop.start();
            console.log(vm.item);
            vm.item._imgs = vm.item.imgs;
            if (vm.type == 'edit') {
                ProductsService.update(vm.item).then(
                    function (response) {
                        vm.dialog.close();
                        myLoop.stop();
                        swal("Great! Your item has been updated!", {
                            icon: "success",
                            timer: 2000,
                            buttons: false
                        });
                        vm.searchData();
                    },
                    function (error) {
                        swal("Error!", {
                            icon: "error"
                        });
                        myLoop.stop();
                    }
                );
            } else {
                ProductsService.create(vm.item).then(
                    function (response) {
                        vm.dialog.close();
                        myLoop.stop();
                        swal("Great! Your item has been created!", {
                            icon: "success",
                            timer: 2000,
                            buttons: false
                        });
                        vm.searchData();
                    },
                    function (error) {
                        swal("Error!", {
                            icon: "error"
                        });
                        myLoop.stop();
                    }
                );
            }
        };

        //================= On Init function ========================//
        init();
        function init() {
            ConstantService.getConstants("TypeTransaction", null).then(
                function (response) {
                    vm.lstTransaction = response.data.result;
                },
                function (error) {
                    swal("Error!", {
                        icon: "error"
                    });
                })
            ConstantService.getLocation("VN_City", null).then(
                function (response) {
                    vm.lstCity = response.data.result;
                },
                function (error) {
                    swal("Error!", {
                        icon: "error"
                    });
                })

            vm.directionOptions = {
                placeholder: "Select ...",
                dataTextField: "name",
                dataValueField: "id",
                valuePrimitive: true,
                autoBind: true,
                dataSource: new kendo.data.DataSource({
                    transport: {
                        read: function (options) {
                            ConstantService.getConstants("Direction", null).then(
                                function (response) {
                                    vm.item.balconyDirection = response.data.result[0].id;
                                    vm.item.houseDirection = response.data.result[0].id;
                                    options.success(response.data.result);
                                },
                                function (error) {
                                    swal("Error!", {
                                        icon: "error"
                                    });
                                })
                        }
                    }
                })
            }
        }
        vm.onSubmitValidate = function () {
            if (vm.validator.validate()) {
                if (vm.item.typeTransaction != undefined
                    && vm.item.typeProperty != undefined
                    && vm.item.city != undefined
                    && vm.item.district != undefined)
                    vm.onSaveChange();
            } else {
            }
        }
        // vm.validatorFormOptions= {
        //     validate: function (e) {
        //         if ($scope.idForm.$pristine) {
        //             // utils.clearValid();
        //         }
        //     }
        // };

        //================= On Change function ========================//
        vm.onChangeCity = function (id) {
            ConstantService.getLocation("VN_Districts", id).then(
                function (response) {
                    vm.lstDistrict = response.data.result;
                },
                function (error) {
                    swal("Error!", {
                        icon: "error"
                    });
                })
        }
        vm.onChangeDistrict = function (id) {
            ConstantService.getLocation("VN_Streets", id).then(
                function (response) {
                    vm.lstStreet = response.data.result;
                },
                function (error) {
                    swal("Error!", {
                        icon: "error"
                    });
                })
            ConstantService.getLocation("VN_Wards", id).then(
                function (response) {
                    vm.lstWard = response.data.result;
                },
                function (error) {
                    swal("Error!", {
                        icon: "error"
                    });
                })
        }
        vm.onChangeTypeTransaction = function (id) {
            ConstantService.getConstants("TypeProperty", id).then(
                function (response) {
                    vm.lstProperty = response.data.result;
                },
                function (error) {
                    swal("Error!", {
                        icon: "error"
                    });
                })
            ConstantService.getConstants("Unit", id).then(
                function (response) {
                    vm.lstUnit = response.data.result;
                },
                function (error) {
                    swal("Error!", {
                        icon: "error"
                    });
                })
        }
        vm.onChangeTypeTransactionSearch = function (item) {
            ConstantService.getConstants("TypeProperty", item).then(
                function (response) {
                    vm.lstProperty = response.data.result;
                },
                function (error) {
                    swal("Error!", {
                        icon: "error"
                    });
                })
        }
    })
