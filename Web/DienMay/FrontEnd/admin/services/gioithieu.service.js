angular.module('app')
    .service('GioiThieuService',
        [
            '$http',
            'BASE_URL_API',
            function ($http, BASE_URL_API) {
                this.get = function () {
                    var url = BASE_URL_API + '/constant/getContents';
                    return $http.get(url);
                };
                // update
                this.update = function (data) {
                    var url = BASE_URL_API + '/constant/updateContents';
                    return $http.post(url, data);
                };
            }]);