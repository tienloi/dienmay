angular.module('app')
    .service('HinhAnhService',
        [
            '$http',
            'BASE_URL_API',
            function ($http, BASE_URL_API) {
                // get area list
                this.getList = function (data) {
                    var url = BASE_URL_API + '/files/search';
                    return $http.post(url, data);
                };
                // add new
                this.addNew = function (data) {
                    var url = BASE_URL_API + '/files/create';
                    return $http.post(url, data);
                };
                // update
                this.update = function (data) {
                    var url = BASE_URL_API + '/files/update';
                    return $http.post(url, data);
                };
                //delete
                this.deleteFile = function (id) {
                    var url = BASE_URL_API + '/files/deleteFile?id=' + id;
                    return $http.get(url);
                };
                // get area list
                this.getFolder = function () {
                    var url = BASE_URL_API + '/files/getFolder';
                    return $http.get(url);
                };
                // add new Folder 
                this.createFolder = function (data) {
                    var url = BASE_URL_API + '/files/createFolder';
                    return $http.post(url, data);
                };
            }
        ])