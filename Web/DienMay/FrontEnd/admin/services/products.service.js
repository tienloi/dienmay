angular.module('app')
    .service('ProductsService',
        [
            '$http',
            'BASE_URL_API',
            function ($http, BASE_URL_API) {
                var router = "/products";
                // get area list
                this.getList = function (data) {
                    var url = BASE_URL_API + router+'/search';
                    return $http.post(url, data);
                };
                this.getListSubOrder = function (data) {
                    var url = BASE_URL_API + router+'/searchSubOrder';
                    return $http.post(url, data);
                };
                // get area list
                this.getAllStatus = function () {
                    var url = BASE_URL_API + router+'/getAllStatus';
                    return $http.get(url);
                };
                 // create
                 this.create = function (data) {
                    var url = BASE_URL_API + router+'/create';
                    return $http.post(url, data);
                };
                // update
                this.update = function (data) {
                    var url = BASE_URL_API + router+'/update';
                    return $http.post(url, data);
                };
                // deletes
                this.deletes = function (data) {
                    var url = BASE_URL_API + router+'/deletes';
                    return $http.post(url, data);
                };
                 // active-deactive
                 this.setStatus = function (data) {
                    var url = BASE_URL_API + router+'/setStatus';
                    return $http.post(url, data);
                };
                 // active-deactive
                 this.setVIP = function (data) {
                    var url = BASE_URL_API + router+'/setVIP';
                    return $http.post(url, data);
                };
            }
        ])