//Load showroom
$(".loadShowroom").click(function(){
  if($(".list-showroom-top").text().trim()=='')
    $(".list-showroom-top").load("/page/he-thong-showroom?type=ajax");
  
  $(".list-showroom-top").toggle();
  $(this).toggleClass("active");
});



$('body').click(function(e){
  var targ = $("#list-showroom-top");
  if (!targ.is(e.target) && targ.has(e.target).length === 0){
    $(".list-showroom-top").hide();
    $(".loadShowroom").removeClass("active");
  }
  
});

$(function(){
  $("img.lazy").lazyload({
    effect : "fadeIn"
  }); 
  $(".img-lazy img").lazyload({
    effect : "fadeIn"
  }); 
  
  //Fixed category:
  var topfix = $("#main-menu ul").offset().top + $("#main-menu ul").height();
  $(window).load(function(){
  	$("#category-fixed").css("top",($(window).height() - $("#category-fixed").height())/2)
  });
  $(window).scroll(function(){
    if($(window).scrollTop() > topfix) $("#category-fixed").addClass("slideInLeft").fadeIn();
    else $("#category-fixed").removeClass("slideInLeft").fadeOut();
    
  });
  
  //show banner fix bottom
  var lastScrollTop = $(window).scrollTop();
  $(window).scroll(function(event){
     var st = $(this).scrollTop();
     if (st > lastScrollTop && st > 500){
         $("#banner-promotion-footer").fadeIn();
     } else {
        
     }
     lastScrollTop = st;
  });
  
  //Close banner footer:
  $("#banner-promotion-footer .close").click(function(){
    $("#banner-promotion-footer").remove();
  });
  
  //nav fixed
  $(window).scroll(function(){
    if($(window).scrollTop() > 500){
    	$("#nav,#header").addClass("fixed");
    }
    else $("#nav,#header").removeClass("fixed");
  });
  
  //toggle support
  $("#support-header .title-support-top").click(function(){
  	 $(".support_content").toggle();
     $(this).toggleClass("active");
  });
  $('body').click(function(e){
    var targ = jQuery("#support-header");
    if (!targ.is(e.target) && targ.has(e.target).length === 0){
      $(".support_content").hide();
      $(".title-support-top").removeClass("active");
    }
  	
  });
  
  //nd
  $(".nd p").each(function(){
  	if($(this).text().trim()=='' && $(this).find("img").length == 0 && $(this).find("iframe").length == 0) $(this).remove();
  });
});

//gototop
$("#gotoTop").click(function(){
  	console.log("top");
	$('document,body').animate({scrollTop:0},800);
});
$(window).scroll(function(){
	if($(window).scrollTop() > 0) $("#gotoTop").fadeIn();
  	else $("#gotoTop").fadeOut();
});


//show popup
function showPopup(a){
  if(a == 'popup-youtube'){
    var url = $("#url-video-popup").val();
  	$("#content-popup-youtube").html('<iframe width="495" height="315" src='+url+' frameborder="0" allowfullscreen></iframe>');
  }
  $("#bg-opacity").fadeIn();
  $("#"+a).fadeIn();
}

//close popup:
function closePopup(){
  	if($("#content-popup-youtube iframe").length > 0) $("#content-popup-youtube").empty();
	$(".popup-common").fadeOut();
  	$("#bg-opacity").fadeOut();
}
function subscribe_newsletter(){
  var email = document.getElementById('email_newsletter').value;
  if(email.length > 3){
    $.post("/ajax/register_for_newsletter.php", {email:email}, function(data) {
      if(data=='success') {alert("Quý khách đã đăng ký thành công "); $("#email_newsletter").val("Nhập Email nhận bản tin");}
      else if(data=='exist'){ alert("Email này đã tồn tại");}
        else { alert('Lỗi xảy ra, vui lòng thử lại '); }
      
    });
  }else{alert('Vui lòng nhập địa chỉ email');}  
}
function check_form_contact(){
  var error = "";
  var check_name = document.getElementById('contact_name').value;
  var check_email = document.getElementById('contact_email').value;
  var check_tel = document.getElementById('contact_tel').value;
  var check_message = document.getElementById('contact_message').value;
  //var check_captcha = document.getElementById('captcha').value;
  
  if(check_name.length < 4) error += "- Bạn chưa nhập tên\n";
  if(check_email.length < 4) error += "- Bạn chưa nhập email\n";
  if(check_message.length < 4) error += "- Bạn chưa nhập nội dung\n";
  //if(check_captcha.length < 4) error += "- Bạn chưa nhập Mã bảo vệ\n";
  
  if(error == ""){
    $.post("/ajax/customer_contact.php", {from: 'ajax',contact_name:check_name, contact_email:check_email, contact_tel:check_tel, contact_message:check_message}
           ,function(data){
             alert("Bạn đã gửi liên hệ thành công");
             closePopup();
             return true;
           });  
  }
  else alert(error);
  return false;
}


function change_captcha(holder){
  var unixtime_ms = new Date().getTime();
  $("#"+holder).attr("src","/includes/captcha/captcha.php?v="+unixtime_ms);
}

function check_user_captcha_contact(captcha,id){
  $('#check_captcha_contact').html("... đang kiểm tra");
  $.post("/ajax/check_user.php", {action : 'check-captcha', captcha : captcha}, function(data){ $('#'+id).html(data); });
} 

$(document).ready(function(){
  $("#text_search").keyup(function(b){
    
    if (b.keyCode != 38 && b.keyCode != 40) {
      inputString = $(this).val();
      if(inputString.trim() !=''){
        $(".autocomplete-suggestions").show();
        $(".autocomplete-suggestions").load("/ajax/get_product_list.php?q="+encodeURIComponent(inputString));
      }else  {
        $(".autocomplete-suggestions").hide();
        count_select=0;
      }
    }
  });
  
  $('body').click(function(){
    $(".autocomplete-suggestions").hide();
  });
});



$.get("/ajax/get_json.php?action=count-deal", function(data, status){
  	$("#count-total-deal").text(data);
});


(function($){$.fn.viewportChecker=function(useroptions){var options={classToAdd:'visible',offset:100,callbackFunction:function(elem){}};$.extend(options,useroptions);var $elem=this,windowHeight=$(window).height();this.checkElements=function(){var scrollElem=((navigator.userAgent.toLowerCase().indexOf('webkit')!=-1)?'body':'html'),viewportTop=$(scrollElem).scrollTop(),viewportBottom=(viewportTop+windowHeight);$elem.each(function(){var $obj=$(this);if($obj.hasClass(options.classToAdd)){return}var elemTop=Math.round($obj.offset().top)+options.offset,elemBottom=elemTop+($obj.height());if((elemTop<viewportBottom)&&(elemBottom>viewportTop)){$obj.addClass(options.classToAdd);options.callbackFunction($obj)}})};$(window).scroll(this.checkElements);this.checkElements();$(window).resize(function(e){windowHeight=e.currentTarget.innerHeight})}})(jQuery);
/*
$('#home-section3 .box-pro-home').viewportChecker({
  classToAdd: 'animated slideInUp',
  offset:0
});
*/

//popup
function confirmimage(o){$(".popup").show(),$(".overlay").show(),urlLogin=o}function closePop(){$(".popup").hide(),$(".overlay").hide()}function confirmLogin(o){$(".popup_login").show(),$(".overlay").show(),urlLogin=o}function forwardLogin(){window.location="http://aha.vn"}function loadPopup(){0==popupStatus&&($("#backgroundPopup").css({opacity:"0.7"}),$("#backgroundPopup").fadeIn("slow"),$("#popupContact").fadeIn("slow"),popupStatus=1)}function disablePopup(){1==popupStatus&&($("#backgroundPopup").fadeOut("slow"),$("#popupContact").fadeOut("slow"),popupStatus=0)}function centerPopup(){var o=document.documentElement.clientWidth,p=document.documentElement.clientHeight,n=$("#popupContact").height(),t=$("#popupContact").width();$("#popupContact").css({position:"absolute",top:p/2-n/2,left:o/2-t/2}),$("#backgroundPopup").css({height:p})}var urlLogin="";$(document).keypress(function(o){27==escape(o.keyCode)&&(closePop(),windown.close())});var popupStatus=0;$(window).load(function(){centerPopup(),loadPopup(),$("#button").click(function(){centerPopup(),loadPopup()}),$("#popupContactClose").click(function(){disablePopup()}),$("#backgroundPopup").click(function(){disablePopup()}),$(document).keypress(function(o){27==o.keyCode&&1==popupStatus&&disablePopup()})});

//session
!function(e){e.session={_id:null,_cookieCache:void 0,_init:function(){window.name||(window.name=Math.random()),this._id=window.name,this._initCache();var e=new RegExp(this._generatePrefix()+"=([^;]+);").exec(document.cookie);if(e&&document.location.protocol!==e[1]){this._clearSession();for(var t in this._cookieCache)try{window.sessionStorage.setItem(t,this._cookieCache[t])}catch(e){}}document.cookie=this._generatePrefix()+"="+document.location.protocol+";path=/;expires="+new Date((new Date).getTime()+12e4).toUTCString()},_generatePrefix:function(){return"__session:"+this._id+":"},_initCache:function(){var e=document.cookie.split(";");this._cookieCache={};for(var t in e){var i=e[t].split("=");new RegExp(this._generatePrefix()+".+").test(i[0])&&i[1]&&(this._cookieCache[i[0].split(":",3)[2]]=i[1])}},_setFallback:function(e,t,i){var o=this._generatePrefix()+e+"="+t+"; path=/";return i&&(o+="; expires="+new Date(Date.now()+12e4).toUTCString()),document.cookie=o,this._cookieCache[e]=t,this},_getFallback:function(e){return this._cookieCache||this._initCache(),this._cookieCache[e]},_clearFallback:function(){for(var e in this._cookieCache)document.cookie=this._generatePrefix()+e+"=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;";this._cookieCache={}},_deleteFallback:function(e){document.cookie=this._generatePrefix()+e+"=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;",delete this._cookieCache[e]},get:function(e){return window.sessionStorage.getItem(e)||this._getFallback(e)},set:function(e,t,i){try{window.sessionStorage.setItem(e,t)}catch(e){}return this._setFallback(e,t,i||!1),this},delete:function(e){return this.remove(e)},remove:function(e){try{window.sessionStorage.removeItem(e)}catch(e){}return this._deleteFallback(e),this},_clearSession:function(){try{window.sessionStorage.clear()}catch(t){for(var e in window.sessionStorage)window.sessionStorage.removeItem(e)}},clear:function(){return this._clearSession(),this._clearFallback(),this}},e.session._init()}(jQuery);