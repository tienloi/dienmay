﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using BanNhaAZ.Models;
using CommonHelper.Helper;
using Core.Business;
using Core.Models;
using Core.Models.EF;

namespace BanNhaAZ.Areas.Admin.Controllers
{
    [RoutePrefix("api/files")]
    public class FilesController : ApiController
    {
        #region + Constructor
        private readonly FilesBusiness _business = new FilesBusiness();
        #endregion

        #region + Function
        /// <summary>
        /// Function search 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [Route("Search")]
        [HttpPost]
        public async Task<IHttpActionResult> Search([FromBody]ListFilter filter)
        {
            try
            {

                if (filter == null)
                {
                    filter = new ListFilter();
                }
                if (filter.Filter == null || filter.Filter.Count == 0)
                {
                    filter.Filter = new List<FilterTypeModel>();
                }
                if (filter.Sort == null || filter.Sort.Count == 0)
                {
                    filter.Sort = new List<SortTypeModel>();
                }
                var filterDataTable = Utilities.ListToDataTable(filter.Filter);
                var sortDataTable = Utilities.ListToDataTable(filter.Sort);
                var skip = filter.Skip ?? 0;
                var take = filter.Take ?? Constants.Config.ItemsPerPage;
                var results = await _business.GetData(filter.Name != null ? filter.Name.Trim() : null, filter.Id, filterDataTable, sortDataTable, skip, take);

                return Ok(new ObjectResult
                {
                    StatusCode = Enums.StatusCode.Ok,
                    Result = results,
                    Message = Constants.Message.GET_DATA_SUCCESSFULLY
                });
            }
            catch (Exception ex)
            {
                var result = new ObjectResult();
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = ex.Message;
                result.Result = false;
                Logger.CreateLog(Logger.Levels.ERROR, this, "Files/Search()", string.Concat("filter : ", filter), ex.ToString());
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
        }

        [HttpPost]
        [HttpOptions]
        [Route("uploadFile")]
        public async Task<IHttpActionResult> UploadFile()
        {
            try
            {
                var lstFiles = new List<Files>();
                if (Request.Content.IsMimeMultipartContent())
                {
                    var httpRequest = HttpContext.Current.Request;
                    var idFolder = httpRequest.Form["id"] == null ? 0 : Convert.ToInt32(httpRequest.Form["id"].ToString());

                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        string fileNameNew = Ultity.GenerateRandomNumberDate() + "_" + postedFile.FileName;
                        var filePath = HttpContext.Current.Server.MapPath("~/Files/" + fileNameNew);
                        postedFile.SaveAs(filePath);


                        var item = new Files();
                        item.CreatedDate = DateTime.Now;
                        item.FolderId = idFolder;
                        item.Type = (int)Enums.FileType.Images;
                        item.Name = postedFile.FileName.Trim();
                        item.Link = System.IO.Path.Combine("/Files/", fileNameNew).Trim();
                        item.Active = true;

                        lstFiles.Add(item);
                    }
                }
                //    string StoragePath = AppDomain.CurrentDomain.BaseDirectory + "Files/";
                //    var streamProvider = new MultipartFormDataStreamProvider(StoragePath);
                //    await Request.Content.ReadAsMultipartAsync(streamProvider);
                //    foreach (MultipartFileData fileData in streamProvider.FileData)
                //    {
                //        if (string.IsNullOrEmpty(fileData.Headers.ContentDisposition.FileName))
                //        {
                //            //return Request.CreateResponse(HttpStatusCode.NotAcceptable, "This request is not properly formatted");
                //        }
                //        string fileName = fileData.Headers.ContentDisposition.FileName;
                //        if (fileName.StartsWith("\"") && fileName.EndsWith("\""))
                //        {
                //            fileName = fileName.Trim('"');
                //        }
                //        if (fileName.Contains(@"/") || fileName.Contains(@"\"))
                //        {
                //            fileName = System.IO.Path.GetFileName(fileName);
                //        }

                //        string fileNameNew = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(fileName);
                //        System.IO.File.Move(fileData.LocalFileName, System.IO.Path.Combine(StoragePath, fileNameNew));

                //        var item = new Files();
                //        item.CreatedDate = DateTime.Now;
                //        item.Type = (int)Enums.FileType.Images;
                //        item.Name = fileName.Trim();
                //        item.Link = System.IO.Path.Combine("/Files/", fileNameNew).Trim();
                //        item.Active = true;

                //        lstFiles.Add(item);
                //    }
                //}
                var result = new ObjectResult();
                var returnVal = await _business.Create(lstFiles);
                if (returnVal.Status)
                {
                    result.StatusCode = Enums.StatusCode.Ok;
                    result.Message = returnVal.Message;
                    result.Result = returnVal.Status;

                    return Ok(result);
                }
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = returnVal.Message;
                result.Result = false;
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
            catch (Exception ex)
            {
                var result = new ObjectResult();
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = ex.Message;
                result.Result = false;
                Logger.CreateLog(Logger.Levels.ERROR, this, "UploadFile()", string.Empty, ex.ToString());
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }

            //return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Route("deleteFile")]
        [HttpGet]
        public async Task<IHttpActionResult> DeleteFile(int id)
        {
            var results = await _business.DeleteFile(id);

            return Ok(new ObjectResult
            {
                StatusCode = Enums.StatusCode.Ok,
                Result = results,
                Message = Constants.Message.GET_DATA_SUCCESSFULLY
            });
        }

        [Route("GetFolder")]
        public async Task<IHttpActionResult> GetFolder()
        {
            var results = await _business.GetFolder();

            return Ok(new ObjectResult
            {
                StatusCode = Enums.StatusCode.Ok,
                Result = results,
                Message = Constants.Message.GET_DATA_SUCCESSFULLY
            });
        }

        [Route("createFolder")]
        [HttpPost]
        public async Task<IHttpActionResult> CreateFolder([FromBody] Folder data)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = new ObjectResult();
                var returnVal = await _business.CreateFolder(data);
                if (returnVal.Status)
                {
                    result.StatusCode = Enums.StatusCode.Ok;
                    result.Message = returnVal.Message;
                    result.Result = returnVal.Status;
                    result.IdReturn = returnVal.IdReturn;

                    return Ok(result);
                }

                result.StatusCode = Enums.StatusCode.Error;
                result.Message = returnVal.Message;
                result.Result = false;
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
            catch (Exception ex)
            {
                var result = new ObjectResult();
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = ex.Message;
                result.Result = false;
                Logger.CreateLog(Logger.Levels.ERROR, this, "Create()", string.Concat("data : ", data), ex.ToString());
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
        }
        #endregion
    }
}
