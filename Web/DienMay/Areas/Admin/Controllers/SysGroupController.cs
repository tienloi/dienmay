﻿using System.Security.Claims;
using System.Web.Http;

namespace BanNhaAZ.Areas.Admin.Controllers
{
    [RoutePrefix("api/sysgroup")]
    public class SysGroupController : ApiController
    {
        [Authorize]
        [Route("get")]
        public IHttpActionResult Get()
        {
            var identity = (ClaimsIdentity)User.Identity;
            return Ok("Hello " + identity.Name);
        }

        [Route("getNo")]
        public IHttpActionResult GetNoResult()
        {
            var identity = (ClaimsIdentity)User.Identity;
            return Ok("GetNoResult " + identity.Name);
        }
    }
}