﻿using AutoMapper;
using Core.Models.EF;
using Core.Models.Messages;

namespace BanNhaAZ.Mapping
{
    public class MappingProfile : Profile
    {
        public static void RegisterProfiles()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<MappingProfile>();
                // Auto Map Ignore field in Object 
                x.CreateMap<ProductView, Product>();
            });
        }
    }
}