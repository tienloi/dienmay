﻿using System.Threading.Tasks;
using System.Web.Mvc;
using BanNhaAZ.Models;
using CommonHelper.Helper;
using Core.Models;

namespace BanNhaAZ.Controllers
{
    public class LoginController : Controller
    {
        private readonly CacheProvider _cacheProvider = new CacheProvider();
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Index(UserLogin model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            if (!model.UserName.Equals("admin") || !model.Password.Equals("12345aA@!"))
            {
                return View();
            }

            _cacheProvider.Set(Constants.CacheConstant.User, model, 2); //seconds
            return RedirectToAction("Index", "Home", new
            {
                area = "Admin"
            });

            //string apiUrl = "http://" + Request.Url.Host +
            //                (Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port) + "/token";
            //using (var client = new HttpClient())
            //{
            //    var values = new Dictionary<string, string>
            //    {
            //        { "username", "admin" },
            //        { "password", "12345aA@" },
            //        { "grant_type", "password" },
            //        //{ "username", model.UserName },
            //        //{ "password", model.Password },
            //        //{ "grant_type", "password" },
            //    };

            //    var content = new FormUrlEncodedContent(values);
            //    var response = await client.PostAsync(apiUrl, content);
            //    if (response.IsSuccessStatusCode)
            //    {
            //        var respStr = await response.Content.ReadAsStringAsync();
            //        var fb = JsonConvert.DeserializeObject<UserLoginInformation>(respStr);
            //        _cacheProvider.Set(Constants.CacheConstant.User, fb, 2); //seconds
            //        return RedirectToAction("Index", "Home", new
            //        {
            //            area = "Admin"
            //        });
            //    }
            //    return View();
            //}
        }

        [HttpGet]
        public async Task<ActionResult> LogOut()
        {
            _cacheProvider.Set(Constants.CacheConstant.User, null, 2); //seconds
            return RedirectToAction("Index");
        }
        [HttpGet]
        public async Task<ActionResult> Register()
        {
            return View();
        }
        [HttpGet]
        public async Task<ActionResult> ForgotPassword()
        {
            return View();
        }
    }
}