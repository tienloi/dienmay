﻿using System.Web.Http;
using log4net.Config;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;

namespace BanNhaAZ
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //log4net
            XmlConfigurator.Configure();
            // Web API configuration and services

            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Web API configuration return json
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}
