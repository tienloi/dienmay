﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace BanNhaAZ.Models
{
    public class FileModel
    {
        [Required(ErrorMessage = "Vui lòng chọn file upload")]
        public HttpPostedFileBase Files { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập Họ tên")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập Số điện thoại")]
        public string Phone { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập Số điện thoại")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail không hợp lệ")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập Địa chỉ")]
        public string Address { get; set; }
        [StringLength(500,ErrorMessage = "Độ dài tối đa 500")]
        public string Summary { get; set; }
    }
}