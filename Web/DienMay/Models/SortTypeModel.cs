﻿namespace BanNhaAZ.Models
{
    /// <summary>
    /// SortTypeModel Object
    /// </summary>
    public class SortTypeModel
    {
        /// <summary>
        /// Field Name
        /// </summary>
        public string Field { get; set; }
        /// <summary>
        /// Order by ascending
        /// </summary>
        public bool? Asc { get; set; }
        /// <summary>
        /// Is Active
        /// </summary>
        public bool IsActive { get; set; }
    }
}