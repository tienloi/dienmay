﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BanNhaAZ.Models
{
    public class PagingModel
    {
        public int TotalPage { get; set; }
        public int FromPage { get; set; }
        public int ToPage { get; set; }
        public int CurrentPage { get; set; }
        public bool IsActivePre { get; set; }
        public bool IsActiveNext { get; set; }
    }
}