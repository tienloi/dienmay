﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Net;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Web.Mvc;
using CommonHelper.Helper;
using Newtonsoft.Json;

namespace BanNhaAZ.Models
{
    public static class Utilities
    {
        public static string ObjectResultToJson(ObjectResult objectResult)
        {
            return JsonConvert.SerializeObject(objectResult);
        }
        public static MediaTypeHeaderValue MediaTypeHeaderValueJson = new MediaTypeHeaderValue("application/json");

        public static MediaTypeFormatter MediaTypeFormatterJson = new JsonMediaTypeFormatter();
        public static string GetAppSetting(string appSettingKey)
        {
            return ConfigurationManager.AppSettings[appSettingKey];
        }
        public static DataTable ListToDataTable<T>(IList<T> data)
        {
            if (data == null)
            {
                return null;
            }

            DataTable table = new DataTable();

            //special handling for value types and string
            if (typeof(T).IsValueType || typeof(T).Equals(typeof(string)))
            {

                DataColumn dc = new DataColumn("Value");
                table.Columns.Add(dc);
                foreach (T item in data)
                {
                    DataRow dr = table.NewRow();
                    dr[0] = item;
                    table.Rows.Add(dr);
                }
            }
            else
            {
                var properties = TypeDescriptor.GetProperties(typeof(T));
                foreach (PropertyDescriptor prop in properties)
                {
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                }
                foreach (T item in data)
                {
                    DataRow row = table.NewRow();
                    foreach (PropertyDescriptor prop in properties)
                    {
                        try
                        {
                            row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                        }
                        catch
                        {
                            row[prop.Name] = DBNull.Value;
                        }
                    }
                    table.Rows.Add(row);
                }
            }
            return table;
        }

        public static void SendEmail(string toEmail, string toNameEmail, string bodyHtml, string fullPath)
        {
            try
            {
                var fromAddress = new MailAddress(ConfigurationManager.AppSettings["host"], ConfigurationManager.AppSettings["fullName"]);
                var toAddress = new MailAddress(toEmail, toNameEmail);
                string fromPassword = ConfigurationManager.AppSettings["pass"].ToString();
                string subject = ConfigurationManager.AppSettings["subject"].ToString();
                string body = bodyHtml;
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                //string fullPath = Server.MapPath("/Files/TbLogistics.xlsx");
                if (string.IsNullOrEmpty(fullPath))
                {
                    using (var message = new MailMessage(fromAddress, toAddress)
                    {
                        Subject = subject,
                        Body = body,
                        IsBodyHtml = true
                    })
                    {
                        smtp.Send(message);
                    }
                }
                else
                {
                    var attachment = new Attachment(fullPath);
                    using (var message = new MailMessage(fromAddress, toAddress)
                    {
                        Subject = subject,
                        Body = body,
                        IsBodyHtml = true,
                        Attachments = { attachment }
                    })
                    {
                        smtp.Send(message);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, null, "SendMail()", string.Empty, ex.ToString());
            }
        }

        public static string IsCurrentMenu(this HtmlHelper html, string control, string action)
        {
            var routeData = html.ViewContext.RouteData;

            var routeAction = (string)routeData.Values["action"];
            var routeControl = (string)routeData.Values["controller"];

            // both must match
            var returnActive = control == routeControl &&
                               action == routeAction;

            return returnActive ? "active" : "";
        }
        public static string ConvertImageToThumbail(string url)
        {
            //----------        Getting the Image File
            System.Drawing.Image img = System.Drawing.Image.FromFile(System.Web.HttpContext.Current.Server.MapPath("~"+url));

            //----------        Getting Size of Original Image
            double imgHeight = img.Size.Height;
            double imgWidth = img.Size.Width;

            //----------        Getting Decreased Size
            double x = imgWidth / 200;
            int newWidth = Convert.ToInt32(imgWidth / x);
            int newHeight = Convert.ToInt32(imgHeight / x);

            //----------        Creating Small Image
            System.Drawing.Image.GetThumbnailImageAbort myCallback = new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback);
            System.Drawing.Image myThumbnail = img.GetThumbnailImage(newWidth, newHeight, myCallback, IntPtr.Zero);

            //----------        Saving Image
            string fileUrl = url;
            if (url.Contains(".jpg"))
                fileUrl = url.Replace(".jpg", "_thumbail.jpg");
            if (url.Contains(".png"))
                fileUrl = url.Replace(".png", "_thumbail.png");
            myThumbnail.Save(System.Web.HttpContext.Current.Server.MapPath("~"+ fileUrl));
            return fileUrl;
        }
        public static bool ThumbnailCallback()
        {
            return false;
        }
    }
}