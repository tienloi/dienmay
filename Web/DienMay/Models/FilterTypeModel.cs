﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BanNhaAZ.Models
{
    /// <summary>
    /// Filter Model
    /// </summary>
    public class FilterTypeModel
    {
        /// <summary>
        /// Field Name
        /// </summary>
        public string Field { get; set; }
        /// <summary>
        /// Value String
        /// </summary>
        public string ValueString { get; set; }
        /// <summary>
        /// Value DateTime From
        /// </summary>
        public DateTime? ValueDateTimeFrom { get; set; }
        /// <summary>
        /// Value DateTime To
        /// </summary>
        public DateTime? ValueDateTimeTo { get; set; }
        /// <summary>
        /// Value Decimal From
        /// </summary>
        [DataType("decimal(18,0)")]
        public decimal? ValueDecimalFrom { get; set; }
        /// <summary>
        /// Value Decimal To
        /// </summary>
        [DataType("decimal(18,0)")]
        public decimal? ValueDecimalTo { get; set; }
        /// <summary>
        /// Value bool
        /// </summary>
        public bool? ValueBit { get; set; }
        /// <summary>
        /// Is Active
        /// </summary>
        public bool IsActive { get; set; }
    }
}