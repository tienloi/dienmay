﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BanNhaAZ.Models
{
    public class ListFilter
    {
        public IList<FilterTypeModel> Filter { get; set; }

        /// <summary>
        /// List Of Sort
        /// </summary>
        public IList<SortTypeModel> Sort { get; set; }

        public int? Skip { get; set; }

        public int? Take { get; set; }
        [StringLength(100)]
        public string Name { get; set; }

        public int Id { get; set; }
        public Guid IdGuid { get; set; }
        public int? TypeTransaction { get; set; }
        public int? TypeProperty { get; set; }
        public bool IsAdmin { get; set; } = false;
        public string Type { get; set; }
    }
}
