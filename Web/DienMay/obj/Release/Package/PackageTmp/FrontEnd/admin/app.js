﻿angular
    .module('sweetalert', [])
    .factory('swal', SweetAlert);

function SweetAlert() {
    return window.swal;
};

var app = angular.module('app',
    [
        "ngRoute",
        "kendo.directives",
        "sweetalert",
        "ckeditor",
        'ui.select',
               'ngSanitize',
        //        'ui.bootstrap',
        'pascalprecht.translate'
    ]);
//Developing scalable apps in Java by google
//--- hoc online
//Udacity--follow university
//coursera --advence
//edx

// app.constant('BASE_URL', 'http://elearningApi.com/');
// app.constant('BASE_URL_API', 'http://elearningApi.com/api/');

app.constant('BASE_URL', 'http://localhost:2111/');
app.constant('BASE_URL_API', window.location.origin + '/api');

//app.constant('URL_LOGIN', '/System/Login');
app.constant('URL_LOGIN', '/login.html');
//app.constant('URL_HOME', '/System/Home');
app.constant('URL_HOME', '/app/view/layout/_layoutAdmin.html');


/////////////////////////////////////////////////////////////////////////////////////////////
//Factory
app.factory('Auth', function () {
    var user;
    return {
        setUser: function (aUser) {
            (localStorage.getItem("_token") !== null) ? localStorage.setItem("_token", angular.toJson(aUser)) : null;;
        },
        isLoggedIn: function () {
            return (localStorage.getItem("_token") !== null) ? true : false;
        }
    }
});
app.factory('tokenAuthen', function () {
    var fac = {};
    fac.CurrentToken = null;
    fac.SetCurrentToken = function (token) {
        fac.CurrentToken = token;
        localStorage.setItem("_token", angular.toJson(token));
    }
    fac.GetCurrentToken = function () {
        fac.CurrentToken = angular.fromJson(localStorage.getItem("_token"));
        return fac.CurrentToken;
    }
    return fac;
});

/////////////////////////////////////////////////////////////////////////////////////////////
//Config

app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push(function (tokenAuthen, $q, URL_LOGIN, $window) {
        return {
            // optional method
            request: function (config) {
                var currentToken = tokenAuthen.GetCurrentToken();
                if (currentToken !== null) {
                    config.headers['Authorization'] = 'bearer ' + currentToken.access_token;
                }
                return config;
            },

            // optional method
            responseError: function (rejection) {
                if (rejection.status === 401) {
                    alert("Not login 401 !!!!!!!");
                    // $window.location.href = "http://" + $window.location.host + URL_LOGIN;
                    return $q.reject(rejection);
                }
                if (rejection.status === 403) {
                    alert("Not login 403");
                    return $q.reject(rejection);
                }
                return $q.reject(rejection);
            }

            //// optional method
            //'response': function (response) {
            //    return response || $q.when(response);
            //},

            //// optional method
            //'responseError': function (rejection) {
            //    return $q.reject(rejection);
            //}
        }
    });
}]);
app.config(function () {
    angular.lowercase = angular.$$lowercase;
});

//config multi language
app.config(function ($translateProvider) {
    // i18n fun goes here
    $translateProvider.useStaticFilesLoader({
        prefix: '/locale-',
        suffix: '.json'
    });
    $translateProvider.preferredLanguage('vi');
});

//////////////////////////////////////////////////////////////////////////////////////////////
//Functio
function getTotalPage(rowInPage, totalRows) {
    var lst = [1];
    var total = (totalRows % rowInPage > 0) ? parseInt(totalRows / rowInPage) + 1 : totalRows / rowInPage;
    for (var i = 2; i <= total; i++) {
        lst.push(i);
    }
    return lst;
}
function isEmpty(value) {
    return angular.isUndefined(value) || value === '' || value === null || value !== value;
}

///////////////////////////////////////////////////////////////////////////////////////////////
//Directive
app.directive("pagingNvt", function () {
    return {
        templateUrl: function (elem, attr) {
            return "/app/view/layout/paging-nvt.html";
        }
    }
});

app.directive('ngMin', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attr, ctrl) {
            scope.$watch(attr.ngMin, function () {
                ctrl.$setViewValue(ctrl.$viewValue);
            });
            var minValidator = function (value) {
                var min = scope.$eval(attr.ngMin) || 0;
                if (!isEmpty(value) && value < min) {
                    ctrl.$setValidity('ngMin', false);
                    return undefined;
                } else {
                    ctrl.$setValidity('ngMin', true);
                    return value;
                }
            };

            ctrl.$parsers.push(minValidator);
            ctrl.$formatters.push(minValidator);
        }
    };
});

app.directive('ngMax', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attr, ctrl) {
            scope.$watch(attr.ngMax, function () {
                ctrl.$setViewValue(ctrl.$viewValue);
            });
            var maxValidator = function (value) {
                var max = scope.$eval(attr.ngMax) || Infinity;
                if (!isEmpty(value) && value > max) {
                    ctrl.$setValidity('ngMax', false);
                    return undefined;
                } else {
                    ctrl.$setValidity('ngMax', true);
                    return value;
                }
            };

            ctrl.$parsers.push(maxValidator);
            ctrl.$formatters.push(maxValidator);
        }
    };
});