var ultis = {
    newGuid: function uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    },
    gridInit: function (e) {
        var lst = jQuery(".k-pager-sm");
        if (lst.length > 0) {
            jQuery(lst[0]).removeClass("k-pager-sm");
        }
    },
    filterMenuInit: function (e) {
        //hide dropdown Contain 
        if (e.container.find(".k-dropdown").length > 0) {
            e.container.find(".k-dropdown").remove();
        }
        //hide title 
        if (e.container.find(".k-filter-help-text").length > 0) {
            e.container.attr("title", "");
            e.container.find(".k-filter-help-text").remove();
        }
        if (e.sender.dataSource.options.schema.model.fields[e.field] !== null && e.sender.dataSource.options.schema.model.fields[e.field] !== undefined && e.sender.dataSource.options.schema.model.fields[e.field].type === "date") {
            var beginOperator = e.container.find("[data-role=dropdownlist]:eq(0)").data("kendoDropDownList");
            beginOperator.value("gte");
            beginOperator.trigger("change");
            beginOperator.readonly();

            var logicOperator = e.container.find("[data-role=dropdownlist]:eq(1)").data("kendoDropDownList");
            if (logicOperator) {
                logicOperator.readonly();
            }

            var endOperator = e.container.find("[data-role=dropdownlist]:eq(2)").data("kendoDropDownList");
            if (endOperator) {
                endOperator.value("lte");
                endOperator.trigger("change");
                endOperator.readonly();
            }

            if (e.container.find(".k-header").length > 0 && e.container.find(".k-header").find(".k-input[data-role=datepicker]").length > 0) {
                e.container.find(".k-dropdown.k-header").find(".k-input").hide();
                var datePicker = e.container.find("[data-role=datepicker]").data("kendoDatePicker");
                datePicker.setOptions({
                    format: "dd/MM/yyyy",
                    parseFormats: ["dd/MM/yyyy"]
                });
            }
        }
        else {
            if (e.container.find(".k-header").length > 0 && e.container.find(".k-header").find(".k-input").length > 0) {
                e.container.find(".k-header").find(".k-input").hide();
            }
        }
    }
}