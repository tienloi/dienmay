app.controller("ConstantController",
    function ($scope, URL_HOME, BASE_URL_API, ConstantService, $interval, $window) {
        var vm = $scope;
        vm.total = 0;
        vm.pageSizeInit = 20;
        vm.search = {
            name: "",
            skip: 0,
            take: 10,
            filter: {},
            sort: {}
        }
        vm.searchData = function (keyEvent) {
            if (keyEvent === undefined || keyEvent.which === 13) {
                vm.allDatasource.read();
            }
        };

        vm.allDatasource = new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    vm.search.filter = [];
                    vm.search.sort = [];
                    vm.search.skip = 0;
                    vm.search.take = vm.pageSizeInit;
                    if (options.data.filter !== null && options.data.filter !== undefined) {
                        _.each(options.data.filter.filters, function (m) {
                            var filter = {
                                field: m.field,
                                valueString: m.value,
                                isActive: true,
                            };

                            vm.search.filter.push(filter);
                        });
                    }
                    if (options.data.sort !== null && options.data.sort !== undefined) {
                        _.each(options.data.sort, function (o) {
                            var sort = {
                                field: o.field,
                                asc: o.dir === 'asc',
                                isActive: true
                            };

                            vm.search.sort.push(sort);
                        });
                    }
                    $scope.search.take = options.data.take ? options.data.take : 10000;
                    $scope.search.skip = (options.data.page - 1) * options.data.pageSize;
                    ConstantService.getList(vm.search).then(function (response) {
                        if (response.data.result !== null && response.data.result !== undefined) {
                            var rawData = response.data.result;
                            vm.total = response.data.result.length > 0 ? response.data.result[0].total : 0;
                            options.success(rawData);
                        } else {
                            options.success([]);
                        }

                    }, function (error) {
                        options.error([]);
                    });
                }
                ,
                update: function (options) {
                    ConstantService.update(options.data).then(function (response) {
                        vm.allDatasource.read();
                    }, function (error) {
                        options.error([]);
                    });
                },
                // destroy: {
                //     url: "/Products/Destroy",
                //     dataType: "jsonp"
                // },
                create: function (options) {
                    options.data.id = 0;
                    ConstantService.addNew(options.data).then(function (response) {
                        vm.allDatasource.read();
                    }, function (error) {
                        options.error([]);
                    });
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, nullable: true, defaultValue: 0 },
                        name: { editable: isEditable, validation: { required: true } },
                        value: { validation: { required: true } },
                    }
                },
                total: function (response) {
                    return response === null || response === undefined || response.length === 0 ? 0 : response[0].total;
                }
            },
            pageSize: vm.pageSizeInit,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        })
        function isEditable(e) {
            // If the id(ProductID) is null, then it is editable.
            return e.name === null || e.name === "";
        }
        vm.mainGridOptions = {
            dataSource: vm.allDatasource,
            sortable: true,
            persistSelection: true,
            noRecords: true,
            messages: {
                noRecords: 'There is no data on current page'
            },
            pageable: {
                refresh: true,
                pageSizes: [10, 20, 50],
                buttonCount: 5
            },
            filterable: {
                extra: true,
                operators: {
                    date: {
                        gte: "Start Date",
                        lte: "End Date"
                    },
                    string: {
                        operator: "contains"
                    },
                    number: {
                        operator: "eq"
                    }
                }
            },
            filterMenuInit: function (e) {
                ultis.filterMenuInit(e);
            },
            columns: [
                {
                    selectable: true,
                    width: "50px"
                }, {
                    field: "id",
                    title: "Id",
                    editable: isEditable,
                    filterable: {
                        extra: false
                    },
                    width: "100px"
                }, {
                    field: "type",
                    title: "Type",
                    // editable: isEditable,
                    filterable: {
                        extra: false
                    },
                    width: "250px"
                }, {
                    field: "name",
                    title: "Name",
                    // editable: isEditable,
                    filterable: {
                        extra: false
                    },
                    width: "250px"
                }, {
                    field: "valueInt",
                    title: "Value Int",
                    filterable: {
                        extra: false
                    },
                }, {
                    field: "valueString",
                    title: "Value String",
                    filterable: {
                        extra: false
                    }
                }, {
                    field: "parentId",
                    title: "ParentId",
                    width: "150px",
                    filterable: {
                        extra: false
                    }
                }, {
                    field: "parentName",
                    title: "Parent Name",
                    editable: isEditable,
                    filterable: {
                        extra: false
                    }
                }, {
                    command: ["edit"],
                    title: "&nbsp;",
                    width: "120px"
                }
            ],
            editable: "inline"
        };
        vm.addRow = function () {
            var grid = $("#grid").data("kendoGrid");
            grid.addRow();
        }
    })