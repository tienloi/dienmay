angular.module('app')
    .service('FilesService',
        [
            '$http',
            'BASE_URL_API',
            function ($http, BASE_URL_API) {
                // get area list
                this.getList = function (data) {
                    var url = BASE_URL_API + '/files/search';
                    return $http.post(url, data);
                };
                // add new
                this.addNew = function (data) {
                    var url = BASE_URL_API + '/files/create';
                    return $http.post(url, data);
                };
                // update
                this.update = function (data) {
                    var url = BASE_URL_API + '/files/uploadFile';
                    return $http.post(url, data);
                };
                //delete
                this.deleteFile = function (id) {
                    var url = BASE_URL_API + '/files/deleteFile?id='+id;
                    return $http.get(url);
                };
            }
        ])