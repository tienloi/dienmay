angular.module('app')
    .service('ConstantService',
        [
            '$http',
            'BASE_URL_API',
            function ($http, BASE_URL_API) {
                // get area list
                this.getList = function (data) {
                    var url = BASE_URL_API + '/constant/search';
                    return $http.post(url, data);
                }
                // add new
                this.addNew = function (data) {
                    var url = BASE_URL_API + '/constant/create';
                    return $http.post(url, data);
                }
                // update
                this.update = function (data) {
                    var url = BASE_URL_API + '/constant/update';
                    return $http.post(url, data);
                }
                // get constant
                this.getConstants = function (type, id) {
                    var url = BASE_URL_API + '/constant/getConstants?' + 'type=' + type + '&id=' + id;
                    return $http.get(url);
                };
                // get location
                this.getLocation = function (type, id) {
                    var url = BASE_URL_API + '/constant/getLocation?' + 'type=' + type + '&id=' + id;
                    return $http.get(url);
                };
            }
        ])