angular.module('app')
    .service('OrderService',
        [
            '$http',
            'BASE_URL_API',
            function ($http, BASE_URL_API) {
                // get area list
                this.getList = function (data) {
                    var url = BASE_URL_API + '/orders/search';
                    return $http.post(url, data);
                };
                this.getListSubOrder = function (data) {
                    var url = BASE_URL_API + '/orders/searchSubOrder';
                    return $http.post(url, data);
                };
                // get area list
                this.getAllStatus = function () {
                    var url = BASE_URL_API + '/orders/getAllStatus';
                    return $http.get(url);
                };
                // update
                this.update = function (data) {
                    var url = BASE_URL_API + '/orders/update';
                    return $http.post(url, data);
                };
                // deletes
                this.deletes = function (data) {
                    var url = BASE_URL_API + '/orders/deletes';
                    return $http.post(url, data);
                };
            }
        ])