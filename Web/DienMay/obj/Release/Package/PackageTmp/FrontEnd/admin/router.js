﻿app.config(
    function ($routeProvider, $locationProvider) {

        //  $locationProvider.html5Mode(true).hashPrefix('');;
        $routeProvider.when('/Home', {
            templateUrl: '/app/view/layout/calendar.html',
            controller: 'HomeController'
        }).when('/SysGroup', {
            templateUrl: '/app/view/admin/group/group.html',
            controller: 'SysGroupController'
        }).when('/SysMenu', {
            templateUrl: '/app/view/admin/menu/menu.html',
            controller: 'SysMenuController'
        }).when('/SysControl', {
            templateUrl: '/app/view/admin/control/control.html',
            controller: 'SysControlController'
        }).when('/SysGroupMenu', {
            templateUrl: '/app/view/admin/group-menu/group-menu.html',
            controller: 'SysGroupMenuController'
        }).when('/SysPermissionMenu', {
            templateUrl: '/app/view/admin/group-user/group-user.html',
            controller: 'SysPermissionMenuController'
        }).when('/SysUser', {
            templateUrl: '/app/view/admin/user/user.html',
            controller: 'SysUserController'
        }).when('/noi-dung', {
            templateUrl: '/FrontEnd/admin/main/gioithieu/gioithieu.html',
            controller: 'GioiThieuController'
        }).when('/products', {
            templateUrl: '/FrontEnd/admin/main/products/products.html',
            controller: 'ProductsController'
        }).when('/don-hang', {
            templateUrl: '/FrontEnd/admin/main/donhang/donhang.html',
            controller: 'DonhangController'
        }).when('/constant', {
            templateUrl: '/FrontEnd/admin/main/constant/constant.html',
            controller: 'ConstantController'
        }).when('/hinh-anh', {
            templateUrl: '/FrontEnd/admin/main/hinhanh/hinhanh.html',
            controller: 'HinhAnhController'
        }).when('/dich-vu', {
            templateUrl: '/FrontEnd/admin/main/dichvu/dichvu.html',
            controller: 'DichVuController'
        }).otherwise({
                redirectTo: '/don-hang'
            });

    });