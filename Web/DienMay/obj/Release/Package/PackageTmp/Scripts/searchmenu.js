﻿var url = window.location.origin + '/api/'

$('#typeTransaction').on('change', '', function (e) {
    loadTypeProperty(this.value, 0);
    loadPrice(this.value, 0);
});
$('#city').on('change', '', function (e) {
    loadLocation('VN_Districts', this.value, 0);
    loadLocation('VN_Wards', this.value, 0);
});

$('#btnSearch').on('click', '', function (e) {
    var origin = window.location.origin + '?keySearch=' + $("#txtSearch").val();
    var typeTransaction = $("#typeTransaction").find(':selected').val();
    var typeProperty = $("#typeProperty").find(':selected').val();
    var area = $("#area").find(':selected').val();
    var price = $("#price").find(':selected').val();
    var direction = $("#direction").find(':selected').val();
    var city = $("#city").find(':selected').val();
    var district = $("#district").find(':selected').val();
    var ward = $("#ward").find(':selected').val();

    if (typeTransaction != '0') {
        origin += '&typeTransaction=' + typeTransaction;
    }
    if (typeProperty != '0') {
        origin += '&typeProperty=' + typeProperty;
    }
    if (area != '0') {
        origin += '&area=' + area;
    }
    if (price != '0') {
        origin += '&price=' + price;
    }
    if (direction != '0') {
        origin += '&direction=' + direction;
    }
    if (city != '0') {
        origin += '&city=' + city;
    }
    if (district != '0') {
        origin += '&district=' + district;
    }
    if (ward != '0') {
        origin += '&ward=' + ward;
    }
    window.location.href = origin;
})

//getUrlParameter
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

function loadTypeTransaction(valueSelected) {
    $.ajax({
        type: "GET",
        url: url + 'constant/getConstants',
        data: {
            type: 'TypeTransaction',
            id: null
        },
        success: function (res) {
            var sel = $('#typeTransaction');
            sel.html('');
            sel.append('<option value="0">Loại giao dịch</option>');
            $.each(res.result, function (key, item) {
                if (valueSelected == item.id) {
                    sel.append('<option value="' + item.id + '" selected>' + item.name + '</option>');
                } else {
                    sel.append('<option value="' + item.id + '">' + item.name + '</option>');
                }

            });
        },
        error: function (xhr) {

        }
    });
}
function loadDirection(valueSelected) {
    $.ajax({
        type: "GET",
        url: url + 'constant/getConstants',
        data: {
            type: 'Direction',
            id: null
        },
        success: function (res) {
            var sel = $('#direction');
            $.each(res.result, function (key, item) {
                if (valueSelected == item.id) {
                    sel.append('<option value="' + item.id + '" selected>' + item.name + '</option>');
                } else {
                    sel.append('<option value="' + item.id + '">' + item.name + '</option>');
                }
            });
        },
        error: function (xhr) {

        }
    });
}
function loadTypeProperty(idTransaction, valueSelected) {
    $.ajax({
        type: "GET",
        url: url + 'constant/getConstants',
        data: {
            type: 'TypeProperty',
            id: idTransaction
        },
        success: function (res) {
            var sel = $('#typeProperty');
            sel.html('');
            sel.append('<option value="0">Loại bất động sản</option>');
            $.each(res.result, function (key, item) {
                if (valueSelected == item.id) {
                    sel.append('<option value="' + item.id + '" selected>' + item.name + '</option>');
                } else {
                    sel.append('<option value="' + item.id + '">' + item.name + '</option>');
                }
            });
        },
        error: function (xhr) {

        }
    });
}
function loadPrice(idTransaction, valueSelected) {
    //Gias
    $.ajax({
        type: "GET",
        url: url + 'constant/getConstants',
        data: {
            type: 'PriceTypeTransaction',
            id: idTransaction
        },
        success: function (res) {
            var sel = $('#price');
            sel.html('');
            sel.append('<option value="0">Giá</option>');
            $.each(res.result, function (key, item) {
                if (valueSelected == item.id) {
                    sel.append('<option value="' + item.id + '" selected>' + item.name + '</option>');
                } else {
                    sel.append('<option value="' + item.id + '">' + item.name + '</option>');
                }
            });
        },
        error: function (xhr) {

        }
    });
}

//Location
function loadLocation(type, id, valueSelected) {
    $.ajax({
        type: "GET",
        url: url + 'constant/getLocation',
        data: {
            type: type,
            id: id
        },
        success: function (res) {
            var sel;
            if (type == 'VN_City') {
                sel = $('#city');
                sel.html('');
                sel.append('<option value="0">Tỉnh/Thành phố</option>');
            }
            if (type == 'VN_Districts') {
                sel = $('#district');
                sel.html('');
                sel.append('<option value="0">Quận/Huyện</option>');
            }
            if (type == 'VN_Wards') {
                sel = $('#ward');
                sel.html('');
                sel.append('<option value="0">Đường/Phố</option>');
            }
            $.each(res.result, function (key, item) {
                if (valueSelected == item.id) {
                    sel.append('<option value="' + item.id + '" selected>' + item.name + '</option>');
                } else {
                    sel.append('<option value="' + item.id + '">' + item.name + '</option>');
                }
            });
        },
        error: function (xhr) {

        }
    });
}
//end Location

function init() {
    var keySearch = getUrlParameter('keySearch');
    var typeTransaction = getUrlParameter('typeTransaction');
    var typeProperty = getUrlParameter('typeProperty');
    var area = getUrlParameter('area');
    var price = getUrlParameter('price');
    var direction = getUrlParameter('direction');
    var city = getUrlParameter('city');
    var district = getUrlParameter('district');
    var ward = getUrlParameter('ward');


    if (keySearch != '') {
        $("#txtSearch").val(keySearch);
    }
    if (typeTransaction != undefined) {
        loadTypeTransaction(parseInt(typeTransaction));
    } else {
        loadTypeTransaction(0);
    }

    if (typeProperty != undefined) {
        loadTypeProperty(parseInt(typeTransaction), parseInt(typeProperty));
    } else {
        loadTypeProperty(parseInt(typeTransaction), 0);
    }

    if (area != undefined) {
        $("#area").val(parseInt(area))
    }
    if (price != undefined) {
        loadPrice(parseInt(typeTransaction), parseInt(price));
    } else {
        loadPrice(parseInt(typeTransaction), 0);
    }

    if (direction != undefined) {
        loadDirection(parseInt(direction));
    } else {
        loadDirection(0);
    }
    //Location
    if (city != undefined) {
        loadLocation('VN_City', null, parseInt(city));
    } else {
        loadLocation('VN_City', null, 0);
    }
    if (district != undefined) {
        loadLocation('VN_Districts', parseInt(city), parseInt(district));
    } else {
        loadLocation('VN_Districts', parseInt(city), 0);
    }
    if (ward != undefined) {
        loadLocation('VN_Wards', parseInt(city), parseInt(ward));
    } else {
        loadLocation('VN_Wards', parseInt(city), 0);
    }
    //end Location
}

init();

