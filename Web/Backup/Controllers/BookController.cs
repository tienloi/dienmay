﻿using System.Security.Claims;
using System.Web.Http;

namespace BanNhaAZ.Controllers
{
    public class BookController : ApiController
    {
        [Authorize]
        public IHttpActionResult Get()
        {
            var identity = (ClaimsIdentity)User.Identity;
            return Ok("Hello " + identity.Name);
        }
    }
}