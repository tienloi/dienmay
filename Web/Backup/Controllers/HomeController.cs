﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;
using BanNhaAZ.Models;
using CommonHelper.Helper;
using Core.Business;
using Core.Models;
using Core.Models.EF;
using Core.Models.Share;
using Newtonsoft.Json.Linq;

namespace BanNhaAZ.Controllers
{
    public class HomeController : Controller
    {
        #region + Constructor
        private readonly ProductsBusiness _productsBusiness = new ProductsBusiness();
        private readonly ConstantBusiness _constantBusiness = new ConstantBusiness();
        private readonly ServicesBusiness _servicesBusiness = new ServicesBusiness();
        private readonly SendLandBusiness _sendLandBusiness = new SendLandBusiness();
        private readonly CacheProvider _cacheProvider = new CacheProvider();

        #endregion

        public async Task<ActionResult> Index(int? page, int? type, int? prop, string key, int? area, int? price, int? direction, string keySearch, int? city, int? district, int? ward, int? street, int? bedroom)
        {
            #region + Get Data for search
            var filter = new ListFilter();
            filter.Filter = new List<FilterTypeModel>();
            filter.Sort = new List<SortTypeModel>();

            #region + Filter
            filter.Filter.Add(new FilterTypeModel { Field = "IsActive", ValueString = "1", IsActive = true });
            if (bedroom.HasValue)
                filter.Filter.Add(new FilterTypeModel { Field = "Bedroom", ValueString = bedroom.Value.ToString(), IsActive = true });
            if (city.HasValue)
                filter.Filter.Add(new FilterTypeModel { Field = "City", ValueString = city.Value.ToString(), IsActive = true });
            if (district.HasValue)
                filter.Filter.Add(new FilterTypeModel { Field = "District", ValueString = district.Value.ToString(), IsActive = true });
            if (ward.HasValue)
                filter.Filter.Add(new FilterTypeModel { Field = "Ward", ValueString = ward.Value.ToString(), IsActive = true });
            if (street.HasValue)
                filter.Filter.Add(new FilterTypeModel { Field = "Street", ValueString = street.Value.ToString(), IsActive = true });
            if (direction.HasValue)
                filter.Filter.Add(new FilterTypeModel { Field = "HouseDirection", ValueString = direction.Value.ToString(), IsActive = true });
            if (price.HasValue)
            {
                switch (price.Value)
                {
                    case 44:
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMin", ValueString = "0", IsActive = true });
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMax", ValueString = "2", IsActive = true });
                        break;
                    case 45:
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMin", ValueString = "2", IsActive = true });
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMax", ValueString = "3", IsActive = true });
                        break;
                    case 46:
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMin", ValueString = "3", IsActive = true });
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMax", ValueString = "5", IsActive = true });
                        break;
                    case 47:
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMin", ValueString = "5", IsActive = true });
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMax", ValueString = "7", IsActive = true });
                        break;
                    case 48:
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMin", ValueString = "7", IsActive = true });
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMax", ValueString = "10", IsActive = true });
                        break;
                    case 49:
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMin", ValueString = "10", IsActive = true });
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMax", ValueString = "100", IsActive = true });
                        break;
                    case 50:
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMin", ValueString = "0", IsActive = true });
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMax", ValueString = "3", IsActive = true });
                        break;
                    case 51:
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMin", ValueString = "3", IsActive = true });
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMax", ValueString = "5", IsActive = true });
                        break;
                    case 52:
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMin", ValueString = "5", IsActive = true });
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMax", ValueString = "7", IsActive = true });
                        break;
                    case 53:
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMin", ValueString = "7", IsActive = true });
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMax", ValueString = "10", IsActive = true });
                        break;
                    case 54:
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMin", ValueString = "10", IsActive = true });
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMax", ValueString = "15", IsActive = true });
                        break;
                    case 55:
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMin", ValueString = "15", IsActive = true });
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMax", ValueString = "30", IsActive = true });
                        break;
                    case 56:
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMin", ValueString = "30", IsActive = true });
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMax", ValueString = "50", IsActive = true });
                        break;
                    case 57:
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMin", ValueString = "50", IsActive = true });
                        filter.Filter.Add(new FilterTypeModel { Field = "PriceRangeMax", ValueString = "70", IsActive = true });
                        break;
                }

            }
            if (area.HasValue)
            {
                switch (area.Value)
                {
                    case 1:
                        filter.Filter.Add(new FilterTypeModel { Field = "AreaMin", ValueString = "0", IsActive = true });
                        filter.Filter.Add(new FilterTypeModel { Field = "AreaMax", ValueString = "30", IsActive = true });
                        break;
                    case 2:
                        filter.Filter.Add(new FilterTypeModel { Field = "AreaMin", ValueString = "30", IsActive = true });
                        filter.Filter.Add(new FilterTypeModel { Field = "AreaMax", ValueString = "50", IsActive = true });
                        break;
                    case 3:
                        filter.Filter.Add(new FilterTypeModel { Field = "AreaMin", ValueString = "50", IsActive = true });
                        filter.Filter.Add(new FilterTypeModel { Field = "AreaMax", ValueString = "80", IsActive = true });
                        break;
                    case 4:
                        filter.Filter.Add(new FilterTypeModel { Field = "AreaMin", ValueString = "80", IsActive = true });
                        filter.Filter.Add(new FilterTypeModel { Field = "AreaMax", ValueString = "100", IsActive = true });
                        break;
                    case 5:
                        filter.Filter.Add(new FilterTypeModel { Field = "AreaMin", ValueString = "100", IsActive = true });
                        filter.Filter.Add(new FilterTypeModel { Field = "AreaMax", ValueString = "150", IsActive = true });
                        break;
                    case 6:
                        filter.Filter.Add(new FilterTypeModel { Field = "AreaMin", ValueString = "150", IsActive = true });
                        filter.Filter.Add(new FilterTypeModel { Field = "AreaMax", ValueString = "300", IsActive = true });
                        break;
                    case 7:
                        filter.Filter.Add(new FilterTypeModel { Field = "AreaMin", ValueString = "300", IsActive = true });
                        filter.Filter.Add(new FilterTypeModel { Field = "AreaMax", ValueString = "500", IsActive = true });
                        break;
                    case 8:
                        filter.Filter.Add(new FilterTypeModel { Field = "AreaMin", ValueString = "500", IsActive = true });
                        filter.Filter.Add(new FilterTypeModel { Field = "AreaMax", ValueString = "5000", IsActive = true });
                        break;
                }

            }
            #endregion

            string name = !string.IsNullOrEmpty(keySearch) ? keySearch : key;

            int take = 10;
            int skipFilter = page.HasValue ? (page.Value > 0 ? (page.Value - 1) * take : 0) : 0;
            var result = (await _productsBusiness.GetData(name, Utilities.ListToDataTable(filter.Filter), Utilities.ListToDataTable(filter.Sort), skipFilter, take, type.HasValue ? type.Value : 0, prop.HasValue ? prop.Value : 0)).ToList();
            #endregion

            #region + TOP 6 VIP

            filter = new ListFilter();
            filter.Filter = new List<FilterTypeModel>();
            filter.Filter.Add(new FilterTypeModel { Field = "IsVIP", ValueString = "1", IsActive = true });
            filter.Filter.Add(new FilterTypeModel { Field = "IsActive", ValueString = "1", IsActive = true });
            ViewBag.lstVip = await _productsBusiness.GetData(string.Empty, Utilities.ListToDataTable(filter.Filter), null, 0, 6, 0, 0);
            #endregion

            #region + News

            filter = new ListFilter();
            filter.Filter = new List<FilterTypeModel>();
            filter.Filter.Add(new FilterTypeModel { Field = "IsActive", ValueString = "1", IsActive = true });

            filter.Sort = new List<SortTypeModel>();
            filter.Sort.Add(new SortTypeModel { Field = "Orders", Asc = false, IsActive = true });

            ViewBag.News = await _servicesBusiness.GetData(string.Empty, Utilities.ListToDataTable(filter.Filter), null, 0, 6);
            #endregion

            #region + Paging
            var total = result.Count() > 0 ? result[0].Total : 0;
            var pagingModel = new PagingModel();
            pagingModel.CurrentPage = page.HasValue ? (page.Value > 0 ? page.Value : 1) : 1;
            pagingModel.TotalPage = (total % take > 0) ? (1 + total / take) : total / take;
            pagingModel.IsActivePre = (pagingModel.CurrentPage > 1);
            pagingModel.IsActiveNext = (pagingModel.CurrentPage < pagingModel.TotalPage);
            pagingModel.FromPage = (pagingModel.CurrentPage <= 3) ? 1 : (pagingModel.CurrentPage - 2);

            pagingModel.ToPage = (pagingModel.FromPage + 4 < pagingModel.TotalPage) ? pagingModel.FromPage + 4 : pagingModel.TotalPage;
            if (pagingModel.TotalPage - pagingModel.ToPage < 2)
            {
                if (pagingModel.ToPage - pagingModel.FromPage != 5)
                {
                    pagingModel.FromPage = pagingModel.ToPage - 4 > 0 ? pagingModel.ToPage - 4 : 1;
                }
            }
            ViewBag.PagingModel = pagingModel;
            #endregion

            var contents = await _constantBusiness.GetContents();
            ViewBag.Introduce = contents.Count() > 0 ? contents.ToList()[0] : null;

            return View(result);
        }
        [ChildActionOnly]
        public async Task<ActionResult> PartialHeader()
        {
            try
            {
                //var getMenu1 = _cacheProvider.Get(Constants.CacheConstant.Menu1) as IEnumerable<Constant>;
                //if (getMenu1 == null)
                //{
                ViewBag.MenuTypeProperty1 = (await _constantBusiness.GetConstants("TypeProperty", 5)).ToList();
                //    _cacheProvider.Set(Constants.CacheConstant.Menu1, ViewBag.MenuTypeProperty1, Constants.CacheConstant.CacheTimeDefault);
                //}

                //var getMenu2 = _cacheProvider.Get(Constants.CacheConstant.Menu1) as IEnumerable<Constant>;
                //if (getMenu2 == null)
                //{
                ViewBag.MenuTypeProperty2 = (await _constantBusiness.GetConstants("TypeProperty", 6)).ToList();
                //    _cacheProvider.Set(Constants.CacheConstant.Menu2, ViewBag.MenuTypeProperty1, Constants.CacheConstant.CacheTimeDefault);
                //}
                //var getMenu3 = _cacheProvider.Get(Constants.CacheConstant.Menu3) as IEnumerable<Constant>;
                //if (getMenu3 == null)
                //{
                ViewBag.MenuTypeProperty3 = (await _constantBusiness.GetConstants("TypeProperty", 58)).ToList();
                //    _cacheProvider.Set(Constants.CacheConstant.Menu3, ViewBag.MenuTypeProperty1, Constants.CacheConstant.CacheTimeDefault);
                //}

                ViewBag.Key = Request.QueryString["key"].ToString();
            }
            catch (Exception ex)
            {
                ViewBag.Key = string.Empty;
            }

            return PartialView();
        }
        public async Task<ActionResult> PartialFooter()
        {
            try
            {
                //var getMenu1 = _cacheProvider.Get(Constants.CacheConstant.Menu1) as IEnumerable<Constant>;
                //if (getMenu1 == null)
                //{
                ViewBag.MenuTypeProperty1 = (await _constantBusiness.GetConstants("TypeProperty", 5)).ToList();
                //    _cacheProvider.Set(Constants.CacheConstant.Menu1, ViewBag.MenuTypeProperty1, Constants.CacheConstant.CacheTimeDefault);
                //}

                //var getMenu2 = _cacheProvider.Get(Constants.CacheConstant.Menu1) as IEnumerable<Constant>;
                //if (getMenu2 == null)
                //{
                ViewBag.MenuTypeProperty2 = (await _constantBusiness.GetConstants("TypeProperty", 6)).ToList();
                //    _cacheProvider.Set(Constants.CacheConstant.Menu2, ViewBag.MenuTypeProperty1, Constants.CacheConstant.CacheTimeDefault);
                //}
                //var getMenu3 = _cacheProvider.Get(Constants.CacheConstant.Menu3) as IEnumerable<Constant>;
                //if (getMenu3 == null)
                //{
                ViewBag.MenuTypeProperty3 = (await _constantBusiness.GetConstants("TypeProperty", 58)).ToList();
                //    _cacheProvider.Set(Constants.CacheConstant.Menu3, ViewBag.MenuTypeProperty1, Constants.CacheConstant.CacheTimeDefault);
                //}
                ViewBag.Contact = (await _constantBusiness.GetConstants("Contact")).ToList().ToDictionary(m => m.Name, m => m.ValueString, StringComparer.OrdinalIgnoreCase);
                ViewBag.Key = Request.QueryString["key"].ToString();
            }
            catch (Exception ex)
            {
                ViewBag.Key = string.Empty;
            }

            return PartialView();
        }
        public async Task<ActionResult> Details(string id, string name)
        {
            var filter = new ListFilter();
            filter.Filter = new List<FilterTypeModel>();
            filter.Filter.Add(new FilterTypeModel { Field = "Id", ValueString = id, IsActive = true });
            var data = await _productsBusiness.GetData(string.Empty, Utilities.ListToDataTable(filter.Filter), null, 0, 1, 0, 0);
            if (data.Count() > 0)
            {
                ViewBag.Item = data.FirstOrDefault();
            }

            filter = new ListFilter();
            filter.Filter = new List<FilterTypeModel>();
            filter.Filter.Add(new FilterTypeModel { Field = "IsVIP", ValueString = "1", IsActive = true });
            filter.Filter.Add(new FilterTypeModel { Field = "IsActive", ValueString = "1", IsActive = true });
            ViewBag.lstVip = await _productsBusiness.GetData(string.Empty, Utilities.ListToDataTable(filter.Filter), null, 0, 12, 0, 0);
            return View();
        }
        public async Task<ActionResult> TinTuc()
        {
            ViewBag.lstNews = _servicesBusiness.GetAllData();
            ViewBag.Id = 0;

            var filter = new ListFilter();
            filter.Filter = new List<FilterTypeModel>();
            filter.Filter.Add(new FilterTypeModel { Field = "IsVIP", ValueString = "1", IsActive = true });
            filter.Filter.Add(new FilterTypeModel { Field = "IsActive", ValueString = "1", IsActive = true });
            ViewBag.lstVip = await _productsBusiness.GetData(string.Empty, Utilities.ListToDataTable(filter.Filter), null, 0, 12, 0, 0);
            return View();
        }
        public async Task<ActionResult> TinTucDetail(string id, string name)
        {
            if (!string.IsNullOrEmpty(id))
            {
                ViewBag.New = _servicesBusiness.GetDataById(Guid.Parse(id));
                ViewBag.Id = 1;
            }
            else
            {
                ViewBag.lstNews = _servicesBusiness.GetAllData();
                ViewBag.Id = 0;
            }

            return View();
        }
        public async Task<ActionResult> Kygui()
        {
            var filter = new ListFilter();
            filter.Filter = new List<FilterTypeModel>();
            filter.Filter.Add(new FilterTypeModel { Field = "IsVIP", ValueString = "1", IsActive = true });
            filter.Filter.Add(new FilterTypeModel { Field = "IsActive", ValueString = "1", IsActive = true });
            ViewBag.lstVip = await _productsBusiness.GetData(string.Empty, Utilities.ListToDataTable(filter.Filter), null, 0, 12, 0, 0);
            return View(new SendLand());
        }
        [HttpPost]
        public async Task<ActionResult> Kygui(SendLand model)
        {
            #region VIP
            var filter = new ListFilter();
            filter.Filter = new List<FilterTypeModel>();
            filter.Filter.Add(new FilterTypeModel { Field = "IsVIP", ValueString = "1", IsActive = true });
            filter.Filter.Add(new FilterTypeModel { Field = "IsActive", ValueString = "1", IsActive = true });
            ViewBag.lstVip = await _productsBusiness.GetData(string.Empty, Utilities.ListToDataTable(filter.Filter), null, 0, 12, 0, 0);
            #endregion

            #region Check Captcha

            var response = Request["g-recaptcha-response"];
            string secretKey = Utilities.GetAppSetting("captcha");
            var client = new WebClient();
            var result = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
            var obj = JObject.Parse(result);
            var status = (bool)obj.SelectToken("success");

            #endregion Check Captcha

            bool check = true;

            #region Validate form

            if (!status)
            {
                ModelState.AddModelError("Captcha", "Mã captcha không đúng.");
                check = false;
            }
            if (string.IsNullOrEmpty(model.Name))
            {
                ModelState.AddModelError("Username", "Không được để trống Tên.");
                check = false;
            }
            if (string.IsNullOrEmpty(model.Address))
            {
                ModelState.AddModelError("Email", "Không được để trống Địa chỉ.");
                check = false;
            }
            //if (string.IsNullOrEmpty(model.Area))
            //{
            //    ModelState.AddModelError("ContentSent", "Không được để trống Diện tích.");
            //    check = false;
            //}
            //if (string.IsNullOrEmpty(model.Floor))
            //{
            //    ModelState.AddModelError("ContentSent", "Không được để trống Số tầng.");
            //    check = false;
            //}
            //if (string.IsNullOrEmpty(model.Facade))
            //{
            //    ModelState.AddModelError("ContentSent", "Không được để trống Mặt tiền.");
            //    check = false;
            //}
            //if (string.IsNullOrEmpty(model.Price))
            //{
            //    ModelState.AddModelError("ContentSent", "Không được để trống Giá bán.");
            //    check = false;
            //}
            if (string.IsNullOrEmpty(model.Phone))
            {
                ModelState.AddModelError("ContentSent", "Không được để trống Số điện thoại.");
                check = false;
            }
            //if (string.IsNullOrEmpty(model.BirthYear))
            //{
            //    ModelState.AddModelError("ContentSent", "Không được để trống Năm sinh.");
            //    check = false;
            //}
            //if (!string.IsNullOrEmpty(model.ContentSent) && model.ContentSent.Length < 20)
            //{
            //    ModelState.AddModelError("ContentSent", "Nội dung phải lớn hơn 20 ký tự.");
            //    check = false;
            //}

            #endregion Validate form

            if (check)
            {
                if (ModelState.IsValid)
                {
                    var objSendLand = new SendLand();
                    objSendLand.IsRead = false;
                    objSendLand.Name = model.Name;
                    objSendLand.Email = model.Email;
                    objSendLand.ContentSent = model.ContentSent;
                    objSendLand.CreatedDate = DateTime.Now;
                    objSendLand.ModifiedDate = DateTime.Now;
                    objSendLand.Phone = model.Phone;

                    objSendLand.Time = model.Time;
                    objSendLand.Address = model.Address;
                    objSendLand.Area = model.Area;
                    objSendLand.Floor = model.Floor;
                    objSendLand.Facade = model.Facade;
                    objSendLand.Price = model.Price;
                    objSendLand.BirthYear = model.BirthYear;
                    try
                    {
                        var res = await _sendLandBusiness.Create(objSendLand);
                        if (!res.Status)
                        {
                            ModelState.AddModelError("ContentSent", res.Message);
                        }
                        else
                        {
                            TempData["Success"] = "Ký gửi mua bán nhà đất thành công.";
                        }
                    }
                    catch (Exception ex)
                    {
                        ModelState.AddModelError("ContentSent", ex.Message);
                    }
                }
            }

            return View(model);
        }

        public async Task<ActionResult> Product(string vip, int? page)
        {
            var filter = new ListFilter();
            filter.Filter = new List<FilterTypeModel>();
            filter.Filter.Add(new FilterTypeModel { Field = "IsVIP", ValueString = !string.IsNullOrEmpty(vip) ? "1" : "0", IsActive = true });
            filter.Filter.Add(new FilterTypeModel { Field = "IsActive", ValueString = "1", IsActive = true });

            int take = 10;
            int skipFilter = page.HasValue ? (page.Value > 0 ? (page.Value - 1) * take : 0) : 0;
            var result = await _productsBusiness.GetData(string.Empty, Utilities.ListToDataTable(filter.Filter), null, skipFilter, take, 0, 0);

            var total = _productsBusiness.GetTotal(!string.IsNullOrEmpty(vip));
            var pagingModel = new PagingModel();
            pagingModel.CurrentPage = page.HasValue ? (page.Value > 0 ? page.Value : 1) : 1;
            pagingModel.TotalPage = (total % take > 0) ? (1 + total / take) : total / take;
            pagingModel.IsActivePre = (pagingModel.CurrentPage > 1);
            pagingModel.IsActiveNext = (pagingModel.CurrentPage < pagingModel.TotalPage);
            pagingModel.FromPage = (pagingModel.CurrentPage <= 3) ? 1 : (pagingModel.CurrentPage - 2);

            pagingModel.ToPage = (pagingModel.FromPage + 4 < pagingModel.TotalPage) ? pagingModel.FromPage + 4 : pagingModel.TotalPage;
            if (pagingModel.TotalPage - pagingModel.ToPage < 2)
            {
                if (pagingModel.ToPage - pagingModel.FromPage != 5)
                {
                    pagingModel.FromPage = pagingModel.ToPage - 4 > 0 ? pagingModel.ToPage - 4 : 1;
                }
            }


            ViewBag.News = _servicesBusiness.GetAllData();
            ViewBag.VIP = vip;
            ViewBag.PagingModel = pagingModel;
            return View(result);
        }
        //public ActionResult GetThumbail(string url)
        //{
        //    Utilities.ConvertImageToThumbail("/Files/0502010257_ff29e1538815714b2804.jpg");
        //    return View();
        //}
    }
}
