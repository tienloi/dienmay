﻿using Core.Models;

namespace BanNhaAZ.Models
{
    public class ObjectResult
    {
        /// <summary>
        /// Status Code
        /// </summary>
        public Enums.StatusCode StatusCode { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Result Object
        /// </summary>
        public object Result { get; set; }

        public bool Status { get; set; }

        public bool? IsFatigue { get; set; }
        public int IdReturn { get; set; }
    }
}