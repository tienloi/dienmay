﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BanNhaAZ.Models
{
    public class UploadImages
    {
        public IEnumerable<object> Files { get; set; }
        public int Id { get; set; }
    }
}