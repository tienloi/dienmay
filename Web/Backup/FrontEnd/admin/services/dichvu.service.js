angular.module('app')
    .service('DichVuService',
        [
            '$http',
            'BASE_URL_API',
            function ($http, BASE_URL_API) {
                var router = "/services";
                // get area list
                this.getList = function (data) {
                    var url = BASE_URL_API + router + '/search';
                    return $http.post(url, data);
                };
                // add new
                this.addNew = function (data) {
                    var url = BASE_URL_API + router + '/create';
                    return $http.post(url, data);
                };
                // update
                this.update = function (data) {
                    var url = BASE_URL_API + router + '/update';
                    return $http.post(url, data);
                };
                // deletes
                this.deletes = function (data) {
                    var url = BASE_URL_API + router + '/deletes';
                    return $http.post(url, data);
                };
            }
        ])