﻿app.controller("DonhangController",
    function ($scope, URL_HOME, BASE_URL_API, OrderService, $interval, $window) {
        var vm = $scope;
        vm.search = {
            type: '',
            name: "",
            skip: 0,
            take: 10,
            filter: {},
            sort: {}
        };
        vm.item = {
        };
        vm.searchLink = {
            type: '',
            name: "",
            skip: 0,
            take: 10,
            filter: {},
            sort: {}
        };
        vm.totalFile = 0;
        vm.totalLink = 0;
        vm.selectedRow = [];
        vm.pageSizeInit = 10;

        vm.searchData = function (keyEvent) {
            if (keyEvent === undefined || keyEvent.which === 13) {
                vm.allDatasource.read();
                vm.allDatasource2.read();
            }
        };

        vm.allDatasource = new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    vm.search.filter = [];
                    vm.search.type = 'file';
                    vm.search.sort = [];
                    vm.search.skip = 0;
                    vm.search.take = vm.pageSizeInit;
                    if (options.data.filter !== null && options.data.filter !== undefined) {
                        _.each(options.data.filter.filters, function (m) {
                            var filter = {};
                            if (m.field === "statusString") {
                                filter = {
                                    field: "status",
                                    valueString: _.chain(vm.lstStatus).filter(function (x) { return x.name === m.value }).first().value().id,
                                    isActive: true,
                                };
                            } else {
                                filter = {
                                    field: m.field,
                                    valueString: m.value,
                                    isActive: true,
                                };
                               
                            }
                            vm.search.filter.push(filter);
                        });
                    }
                    if (options.data.sort !== null && options.data.sort !== undefined) {
                        _.each(options.data.sort, function (o) {
                            var sort = {
                                field: o.field,
                                asc: o.dir === 'asc',
                                isActive: true
                            };

                            vm.search.sort.push(sort);
                        });
                    }

                    $scope.search.take = options.data.take ? options.data.take : 10000;
                    $scope.search.skip = (options.data.page - 1) * options.data.pageSize;
                    OrderService.getList(vm.search).then(function (response) {
                        if (response.data.result !== null && response.data.result !== undefined) {
                            var rawData = response.data.result;
                            vm.totalFile = rawData.length > 0 ? rawData[0].total : 0;

                            _.each(rawData, function (item, i) {
                                item.statusModel = {
                                    id: item.status,
                                    name: item.statusString
                                };
                            });
                            options.success(rawData);
                        } else {
                            options.success([]);
                        }

                    }, function (error) {

                        options.error([]);

                    });
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        file: {
                            type: 'string',
                            editable: false
                        },
                        no: {
                            type: 'string',
                            editable: false
                        },
                        name: {
                            type: 'string',
                            editable: true
                        },
                        phone: {
                            type: 'string',
                            editable: true
                        },
                        email: {
                            type: 'string',
                            editable: true
                        },
                        address: {
                            type: 'string',
                            editable: true
                        },
                        statusString: {
                            type: 'string',
                            editable: true
                        },
                        createdDate: {
                            type: 'date',
                            editable: false
                        }
                    }
                },
                total: function (response) {
                    return response === null || response === undefined || response.length === 0 ? 0 : response[0].total;
                }
            },
            pageSize: vm.pageSizeInit,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        });

        vm.mainGridOptions = {
            dataSource: vm.allDatasource,
            sortable: true,
            persistSelection: true,
            noRecords: true,
            messages: {
                noRecords: 'There is no data on current page'
            },
            pageable: {
                refresh: true,
                pageSizes: [10, 20, 50],
                buttonCount: 5
            },
            filterable: {
                extra: true,
                operators: {
                    date: {
                        gte: "Start Date",
                        lte: "End Date"
                    },
                    string: {
                        operator: "contains"
                    },
                    number: {
                        operator: "eq"
                    }
                }
            },
            change: function (e) {
                vm.selectedRow = _.map(this.select(),
                    function (row) {
                        return e.sender.dataItem(row).id;
                    });
                //$scope.$apply();
                $scope.$evalAsync();
            },
            filterMenuInit: function (e) {
                ultis.filterMenuInit(e);
            },
            //editable: "inline",
            columns: [
                {
                    selectable: true,
                    width: "50px"
                }, {
                    template: function (dataItem) {
                        return "<div class=''>" +
                            "<a href='" + dataItem.link + "' target='_blank'>Download</a></div>";
                        // "style='background-image: url(/Content/admin/Assets/dist/img/user2-160x160.jpg);'></div>"
                    },
                    field: "file",
                    title: "File",
                    filterable: false,
                    width: 80
                }, {
                    field: "no",
                    title: "Mã đơn hàng",
                    filterable: {
                        extra: false
                    }
                }, {
                    field: "name",
                    title: "Họ tên",
                    filterable: {
                        extra: false
                    }
                }, {
                    field: "phone",
                    title: "Phone",
                    filterable: {
                        extra: false
                    }
                }, {
                    field: "email",
                    title: "Email",
                    filterable: {
                        extra: false
                    }
                }, {
                    field: "address",
                    title: "Địa chỉ",
                    filterable: {
                        extra: false
                    }
                }, {
                    field: "createdDate",
                    title: "Ngày tạo",
                    filterable: false,
                    width: 100,
                    template: function (dataItem) {
                        if (!dataItem.createdDate) {
                            return "";
                        }
                        var date = kendo.toString(dataItem.createdDate, 'dd/MM/yyyy');
                        var time = kendo.toString(dataItem.createdDate, 'hh:mm tt');
                        return "<span class='task-name'>" + date + "</span></br><p class='d-summary-text-content' style='color:#999999;font-size:12px'>" + time + '</p';
                    }
                }, {
                    field: "statusString",
                    title: "Trạng thái",
                    width: 120,
                    //editor: statusDropDownEditor,
                    /../Content/home/template: "#=statusModel.name#",
                    filterable: {
                        multi: true,
                        search: true,
                        dataSource: new kendo.data.DataSource({
                            transport: {
                                read: function (options) {
                                    OrderService.getAllStatus().then(
                                        function (response) {
                                            var data = _.map(response.data.result,
                                                function (item) {
                                                    return {
                                                        statusString: item.name
                                                    };
                                                });
                                            options.success(data);
                                        },
                                        function (error) { }
                                    );
                                }
                            },
                        })
                    },
                },
                {
                    command: {
                        text: "Edit", template: function (dataItem) {
                            return '<div style="text-align: center;">' +
                                '   <i title="Edit item" data-ng-click="editItem(dataItem)" style="font-size: 18px;cursor: pointer;" class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp' +
                                '   <i title="Delete item" data-ng-click="deleteItem(dataItem)" style="font-size: 18px;cursor: pointer;" class="fa fa-trash" aria-hidden="true"></i>&nbsp' +
                                '</div>';
                        }
                    }, title: "&nbsp;", width: 80
                }
                //{ command: ["edit"], title: "&nbsp;", width: "120px" }
            ]
        };

        //================= On Action command ========================
        vm.editItem = function (dataItem, tabNumber) {
            vm.type = 'edit';
            vm.tabNumber = tabNumber;
            vm.dialog.open();
            vm.item.id = dataItem.id;
            vm.item.name = dataItem.name;
            vm.item.no = dataItem.no;
            vm.item.note = dataItem.note;
            vm.item.phone = dataItem.phone;
            vm.item.email = dataItem.email;
            vm.item.phone = dataItem.phone;
            vm.item.address = dataItem.address;
            vm.item.summary = dataItem.summary;
            vm.item.statusSelected = dataItem.statusModel;
        };
        vm.deleteItem = function (dataItem) {
            var lstData = [];
            lstData.push(dataItem.id);
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this item!",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willDelete) => {
                if (willDelete) {
                    OrderService.deletes(lstData).then(
                        function (response) {
                            swal("Poof! Your item has been deleted!", {
                                icon: "success",
                                timer: 2000,
                                buttons: false
                            });
                            vm.searchData();
                        },
                        function (error) {
                            swal("Error!", {
                                icon: "error"
                            });
                        }
                    );
                }
            });
        };

        //================= On Action buton ========================
        vm.onAddNew = function () {
            vm.dialog.open();
            vm.item = {
                name: 'vvvvvvvvvvvvv',
                statusSelected: vm.lstStatus[0]
            };

        };
        vm.onDeletes = function () {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this item!",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willDelete) => {
                if (willDelete) {
                    OrderService.deletes(vm.selectedRow).then(
                        function (response) {
                            vm.selectedRow = [];
                            swal("Great! Your item selected has been deleted!", {
                                icon: "success",
                                timer: 2000,
                                buttons: false
                            });
                            vm.searchData();
                        },
                        function (error) {
                            swal("Poof! Something was wrong!", {
                                icon: "error"
                            });
                        }
                    );
                }
            });
        };
        vm.onSaveChange = function () {
            var myLoop = new myLoading("btnSave", "");
            vm.item.status = vm.item.statusSelected.id;
            myLoop.start();
            OrderService.update(vm.item).then(
                function (response) {
                    vm.dialog.close();
                    myLoop.stop();
                    swal("Poof! Your item has been updated!", {
                        icon: "success",
                        timer: 2000,
                        buttons: false
                    });
                    vm.searchData();
                },
                function (error) {
                    swal("Error!", {
                        icon: "error"
                    });
                    myLoop.stop();
                }
            );
        };

        vm.allDatasource2 = new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    vm.searchLink.name = vm.search.name;
                    vm.searchLink.filter = [];
                    vm.searchLink.type = 'link';
                    vm.searchLink.sort = [];
                    vm.searchLink.skip = 0;
                    vm.searchLink.take = vm.pageSizeInit;
                    if (options.data.filter !== null && options.data.filter !== undefined) {
                        _.each(options.data.filter.filters, function (m) {
                            var filter = {};
                            if (m.field === "statusString") {
                                filter = {
                                    field: "status",
                                    valueString: _.chain(vm.lstStatus).filter(function (x) { return x.name === m.value }).first().value().id,
                                    isActive: true,
                                };
                            } else {
                                filter = {
                                    field: m.field,
                                    valueString: m.value,
                                    isActive: true,
                                };

                            }
                            vm.searchLink.filter.push(filter);
                        });
                    }
                    if (options.data.sort !== null && options.data.sort !== undefined) {
                        _.each(options.data.sort, function (o) {
                            var sort = {
                                field: o.field,
                                asc: o.dir === 'asc',
                                isActive: true
                            };

                            vm.searchLink.sort.push(sort);
                        });
                    }

                    $scope.searchLink.take = options.data.take ? options.data.take : 10000;
                    $scope.searchLink.skip = (options.data.page - 1) * options.data.pageSize;

                    OrderService.getList(vm.searchLink).then(function (response) {
                        if (response.data.result !== null && response.data.result !== undefined) {
                            var rawData = response.data.result;
                            vm.totalLink = rawData.length > 0 ? rawData[0].total : 0;

                            _.each(rawData, function (item, i) {
                                item.statusModel = {
                                    id: item.status,
                                    name: item.statusString
                                };
                            });

                            options.success(rawData);
                        } else {
                            options.success([]);
                        }

                    }, function (error) {

                        options.error([]);

                    });
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        no: {
                            type: 'string'
                        },
                        name: {
                            type: 'string'
                        },
                        phone: {
                            type: 'string'
                        },
                        email: {
                            type: 'string'
                        },
                        address: {
                            type: 'string'
                        },
                        statusString: {
                            type: 'string'
                        },
                        createdDate: {
                            type: 'date'
                        }
                    }
                },
                total: function (response) {
                    return response === null || response === undefined || response.length === 0 ? 0 : response[0].total;
                }
            },
            pageSize: vm.pageSizeInit,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        });
        vm.mainGridOptions2 = {
            dataSource: vm.allDatasource2,
            sortable: true,
            persistSelection: true,
            noRecords: true,
            messages: {
                noRecords: 'There is no data on current page'
            },
            dataBound: function (e) {
                ultis.gridInit(e);
            },
            pageable: {
                refresh: true,
                pageSizes: [10, 20, 50],
                buttonCount: 5
            },
            filterable: {
                extra: true,
                operators: {
                    date: {
                        gte: "Start Date",
                        lte: "End Date"
                    },
                    string: {
                        operator: "contains"
                    },
                    number: {
                        operator: "eq"
                    }
                }
            },
            filterMenuInit: function (e) {
                ultis.filterMenuInit(e);
            },
            change: function (e) {
                vm.selectedRow = _.map(this.select(),
                    function (row) {
                        return e.sender.dataItem(row).id;
                    });
                $scope.$evalAsync();
            },
            columns: [
                {
                    selectable: true,
                    width: "50px"
                }, {
                    field: "no",
                    title: "Mã đơn hàng",
                    filterable: {
                        extra: false
                    },
                    width: 160
                }, {
                    field: "name",
                    title: "Họ tên",
                    filterable: {
                        extra: false
                    }
                }, {
                    field: "phone",
                    title: "Phone",
                    filterable: {
                        extra: false
                    }
                }, {
                    field: "email",
                    title: "Email",
                    filterable: {
                        extra: false
                    }
                }, {
                    field: "address",
                    title: "Địa chỉ",
                    filterable: {
                        extra: false
                    }
                }, {
                    field: "createdDate",
                    title: "Ngày tạo",
                    filterable: false,
                    width: 100,
                    template: function (dataItem) {
                        if (!dataItem.createdDate) {
                            return "";
                        }
                        var date = kendo.toString(dataItem.createdDate, 'dd/MM/yyyy');
                        var time = kendo.toString(dataItem.createdDate, 'hh:mm tt');
                        return "<span class='task-name'>" + date + "</span></br><p class='d-summary-text-content' style='color:#999999;font-size:12px'>" + time + '</p';
                    }
                }, {
                    field: "statusString",
                    title: "Trạng thái",
                    width: 120,
                    //editor: statusDropDownEditor,
                    /../Content/home/template: "#=statusModel.name#",
                    filterable: {
                        multi: true,
                        search: true,
                        dataSource: new kendo.data.DataSource({
                            transport: {
                                read: function (options) {
                                    OrderService.getAllStatus().then(
                                        function (response) {
                                            var data = _.map(response.data.result,
                                                function (item) {
                                                    return {
                                                        statusString: item.name
                                                    };
                                                });
                                            options.success(data);
                                        },
                                        function (error) { }
                                    );
                                }
                            },
                        })
                    },
                },
                {
                    command: {
                        text: "Edit", template: function (dataItem) {
                            return '<div style="text-align: center;">' +
                                '   <i title="Edit item" data-ng-click="editItem(dataItem)" style="font-size: 18px;cursor: pointer;" class="fa fa-pencil-square-o" aria-hidden="true"></i> &nbsp' +
                                '   <i title="Delete item" data-ng-click="deleteItem(dataItem)" style="font-size: 18px;cursor: pointer;" class="fa fa-trash" aria-hidden="true"></i>&nbsp' +
                                '</div>';
                        }
                    }, title: "&nbsp;", width: 80
                }
            ]
        };

        vm.detailGridOptions = function (dataItem) {
            return {
                dataSource: new kendo.data.DataSource({
                    transport: {
                        read: function (options) {
                            vm.searchDetail = {};
                            vm.searchDetail.idGuid = dataItem.id;
                            vm.searchDetail.filter = [];
                            vm.searchDetail.sort = [];
                            vm.searchDetail.skip = 0;
                            vm.searchDetail.take = 20;

                            OrderService.getListSubOrder(vm.searchDetail).then(function (response) {
                                if (response.data.result !== null && response.data.result !== undefined) {
                                    var rawData = response.data.result;
                                    options.success(rawData);
                                } else {
                                    options.success([]);
                                }
                            }, function (error) {
                                options.error([]);
                            });
                        }
                    }
                }),
                noRecords: true,
                messages: {
                    noRecords: 'There is no data on current page'
                },
                scrollable: false,
                sortable: false,
                pageable: false,
                columns: [
                    { field: "link", title: "Link sản phẩm", width: 200 },
                    { field: "color", title: "Màu", width: 100 },
                    { field: "size", title: "Size", width: 100 },
                    { field: "quantity", title: "Số lượng", width: 100 },
                    { field: "price", title: "Giá", width: 100 },
                    { field: "imageLink", title: "Link ảnh sản phẩm", width: 200 },
                    { field: "note", title: "Ghi chú" }
                ]
            };
        };

        function statusDropDownEditor(container, options) {
            $('<input required name="' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    autoBind: true,
                    dataTextField: "name",
                    dataValueField: "value",
                    dataSource: new kendo.data.DataSource({
                        transport: {
                            read: function (options) {
                                OrderService.getAllStatus().then(
                                    function (response) {
                                        //var data = _.map(response.data.result,
                                        //    function (item) {
                                        //        return {
                                        //            statusString: item.name
                                        //        };
                                        //    });
                                        options.success(response.data.result);
                                    },
                                    function (error) { }
                                );
                            }
                        },
                    })
                });
        }
        OrderService.getAllStatus().then(
            function (response) {
                vm.lstStatus = response.data.result;
                vm.item.statusSelected = vm.lstStatus[0];
            },
            function (error) {
                vm.lstStatus = [];
            }
        );

    })