app.controller("HinhAnhController",
    function ($scope, URL_HOME, BASE_URL_API, HinhAnhService, $interval, $window) {
        var vm = $scope;
        vm.search = {
            name: "",
            skip: 0,
            take: 10,
            filter: {},
            sort: {}
        }
        vm.list = [];

        $scope.click = function (dataItem) {
            alert(dataItem.text);
        };
        function getSubFolder(lst, id) {
            var result = [];
            _.each(lst, function (item) {
                if (item.parentId == id) {
                    result.push({ id: item.id, text: item.name,parentId:item.parentId, spriteCssClass: "rootfolder", items: getSubFolder(lst, item.id) })
                }
            })
            return result;
        }
        function makeItem(name) {
            return { text: name, spriteCssClass: "rootfolder" };
        };

        $scope.addAfter = function (item) {
            var array = item.parent();
            var index = array.indexOf(item);
            swal({
                text: "Add below folder " + item.text + ":",
                content: "input",
                button: {
                    text: "Submit!",
                    closeModal: false,
                },
            })
                .then(name => {
                    if (!name) throw null;
                    var obj = {
                        name: name,
                        parentId:item.parentId,
                        orders: index + 1
                    }
                    HinhAnhService.createFolder(obj).then(
                        function (res) { 
                            var newItem = makeItem(name);
                            newItem.id=res.data.idReturn;
                            newItem.parentId=item.parentId;
                            array.splice(index + 1, 0, newItem);
                            swal.close()
                        },
                        function (error) {
                            swal("Poof! Something was wrong!", {
                                icon: "error"
                            });
                            myLoop.stop();
                        })
                    // var newItem = makeItem(name);
                    // array.splice(index + 1, 0, newItem);
                    // swal.close()
                    return;
                })
            // array.splice(index + 1, 0, newItem);
        };

        $scope.addBelow = function (item) {
            // can't get this to work by just modifying the data source
            // therefore we're using tree.append instead.
          

            swal({
                text: "Add Child folder " + item.text + ":",
                content: "input",
                button: {
                    text: "Submit!",
                    closeModal: false,
                },
            })
                .then(name => {
                    if (!name) throw null;
                    var obj = {
                        name: name,
                        parentId:item.id,
                    }
                    HinhAnhService.createFolder(obj).then(
                        function (res) { 
                            // loadFolder();
                            var newItem = makeItem(name);
                            newItem.id=res.data.idReturn;
                            newItem.parentId=item.id;
                            $scope.tree.append(newItem, $scope.tree.select());
                            swal.close()
                        },
                        function (error) {
                            swal("Poof! Something was wrong!", {
                                icon: "error"
                            });
                            myLoop.stop();
                        })
                    // var newItem = makeItem(name);
                    // array.splice(index + 1, 0, newItem);
                    // swal.close()
                    return;
                })
        };

        $scope.remove = function (item) {
            var array = item.parent();
            var index = array.indexOf(item);
            array.splice(index, 1);

            $scope.selectedItem = undefined;
        }

        vm.setLightGallery = function () {
            console.log("setLightGallery")
            $('#lightgallery').lightGallery();
            $('.t-block').click(false);
        };
        vm.onRemove = function (obj) {

            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this item!",
                icon: "warning",
                buttons: true,
                dangerMode: true
            }).then((willDelete) => {
                if (willDelete) {
                    HinhAnhService.deleteFile(obj.item.id).then(function (response) {
                        if (response.data.result !== null && response.data.result !== undefined) {
                            swal("Delete item successfully !", {
                                icon: "success",
                                timer: 5000,
                                buttons: false
                            });
                            loadImage();
                        } else {
                            swal("Poof! Something was wrong!", {
                                icon: "error"
                            });
                        }
                    }, function (error) {
                        swal("Poof! Something was wrong!", {
                            icon: "error"
                        });
                    });
                }
            });
        };

        vm.onCopy = function (obj) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val(obj.item.link).select();
            document.execCommand("copy");
            $temp.remove();

            swal("Copied :" + obj.item.link, {
                icon: "success",
                timer: 5000,
                buttons: false
            });
        };
        vm.selectedFolder = function (data) {
            vm.selectedItem = data;
            loadImage(data.id);
        }

        init();
        function loadImage(id) {
            vm.search.id = id;
            HinhAnhService.getList(vm.search).then(function (response) {
                if (response.data.result !== null && response.data.result !== undefined) {
                    vm.list = response.data.result;
                    _.map(vm.list, function (item) {
                        item.link = item.link.trim();
                    });
                } else {
                    vm.list = [];
                }
            }, function (error) {
                options.error([]);
            });
        }
        function loadFolder() {
            HinhAnhService.getFolder().then(function (response) {
                if (response.data.result !== null && response.data.result !== undefined) {
                    var data = [];
                    var lst = _.where(response.data.result, { parentId: null })
                    if (lst.length > 0) {
                        _.each(lst, function (item) {
                            data.push({ id: item.id, text: item.name,parentId:item.parentId, spriteCssClass: "rootfolder", items: getSubFolder(response.data.result, item.id) })
                        })
                    }
                    console.log(data)
                    vm.data = data;
                    $scope.treeData = new kendo.data.HierarchicalDataSource({
                        data: vm.data
                    });
                }
            });
        }
        function init() {
            loadImage(vm.selectedItem == undefined ? 0 : vm.selectedItem.id);
            loadFolder();
            $("#files").kendoUpload({
                async: {
                    saveUrl: BASE_URL_API + "/files/uploadFile",
                    removeUrl: "remove",
                    autoUpload: true
                },
                validation: {
                    allowedExtensions: [".jpg", ".jpeg", ".png", ".bmp", ".gif"]
                },
                upload: function (e) {
                    e.data = { id: vm.selectedItem == undefined ? 0 : vm.selectedItem.id }
                },
                success: onSuccess,
                showFileList: true,
                dropZone: ".dropZoneElement"
            });
            function onSuccess(e) {
                if (e.operation === "upload") {
                    swal("Great! Upload image successed!", {
                        icon: "success",
                        timer: 2000,
                        buttons: false
                    });
                    loadImage(vm.selectedItem == undefined ? 0 : vm.selectedItem.id);
                    // for (var i = 0; i < e.files.length; i++) {
                    //     var file = e.files[i].rawFile;

                    //     if (file) {
                    //         var reader = new FileReader();

                    //         reader.onloadend = function () {
                    //             $("<div class='product'><img src=" + this.result + " /></div>").appendTo($("#products"));
                    //         };

                    //         reader.readAsDataURL(file);
                    //     }
                    // }
                }
            }
        }
    })