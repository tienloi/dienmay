﻿using BanNhaAZ.Models;
using CommonHelper.Helper;
using Core.Business;
using Core.Models;
using Core.Models.EF;
using Core.Models.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace BanNhaAZ.Areas.Admin.Controllers
{
    [RoutePrefix("api/products")]
    public class ProductsController : ApiController
    {
        #region + Constructor
        private readonly ProductsBusiness _business = new ProductsBusiness();
        #endregion

        #region + Function
        /// <summary>
        /// Function search 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [Route("Search")]
        [HttpPost]
        public async Task<IHttpActionResult> Search([FromBody]ListFilter filter)
        {
            try
            {
                if (filter == null)
                {
                    filter = new ListFilter();
                }
                if (filter.Filter == null || filter.Filter.Count == 0)
                {
                    filter.Filter = new List<FilterTypeModel>();
                }
                if (filter.Sort == null || filter.Sort.Count == 0)
                {
                    filter.Sort = new List<SortTypeModel>();
                }

                var filterDataTable = Utilities.ListToDataTable(filter.Filter);
                var sortDataTable = Utilities.ListToDataTable(filter.Sort);
                var skip = filter.Skip ?? 0;
                var take = filter.Take ?? Constants.Config.ItemsPerPage;
                var results = await _business.GetData(filter.Name != null ? filter.Name.Trim() : null, filterDataTable, sortDataTable, skip, take, filter.TypeTransaction, filter.TypeProperty);

                return Ok(new ObjectResult
                {
                    StatusCode = Enums.StatusCode.Ok,
                    Result = results,
                    Message = Constants.Message.GET_DATA_SUCCESSFULLY
                });
            }
            catch (Exception ex)
            {
                var result = new ObjectResult();
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = ex.Message;
                result.Result = false;
                Logger.CreateLog(Logger.Levels.ERROR, this, "Orders/Search()", string.Concat("filter : ", filter), ex.ToString());
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
        }

        [Route("create")]
        [HttpPost]
        public async Task<IHttpActionResult> Create([FromBody] ProductView data)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = new ObjectResult();
                data.ImageMain = Utilities.ConvertImageToThumbail(data.ImageMain);
                var returnVal = await _business.Create(data);
                if (returnVal.Status)
                {
                    result.StatusCode = Enums.StatusCode.Ok;
                    result.Message = returnVal.Message;
                    result.Result = returnVal.Status;

                    return Ok(result);
                }


                result.StatusCode = Enums.StatusCode.Error;
                result.Message = returnVal.Message;
                result.Result = false;
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
            catch (Exception ex)
            {
                var result = new ObjectResult();
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = ex.Message;
                result.Result = false;
                Logger.CreateLog(Logger.Levels.ERROR, this, "Create()", string.Concat("data : ", data), ex.ToString());
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
        }

        [Route("update")]
        [HttpPost]
        public async Task<IHttpActionResult> Update([FromBody] ProductView data)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = new ObjectResult();
                if (!data.ImageMain.Contains("_thumbail"))
                    data.ImageMain = Utilities.ConvertImageToThumbail(data.ImageMain);
                var returnVal = await _business.Update(data);
                if (returnVal.Status)
                {
                    result.StatusCode = Enums.StatusCode.Ok;
                    result.Message = returnVal.Message;
                    result.Result = returnVal.Status;

                    return Ok(result);
                }
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = returnVal.Message;
                result.Result = false;
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
            catch (Exception ex)
            {
                var result = new ObjectResult();
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = ex.Message;
                result.Result = false;
                Logger.CreateLog(Logger.Levels.ERROR, this, "Create()", string.Concat("data : ", data), ex.ToString());
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
        }

        /// <summary>
        /// Function deletes 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [Route("deletes")]
        [HttpPost]
        public async Task<IHttpActionResult> Deletes(List<Guid> data)
        {
            try
            {
                if (data == null)
                {
                    data = new List<Guid>();
                }

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = new ObjectResult();
                var returnVal = await _business.Deletes(data);
                if (returnVal)
                {
                    result.StatusCode = Enums.StatusCode.Ok;
                    result.Message = Constants.Message.SOMETHING_ERROR;
                    result.Result = returnVal;
                    return Ok(result);
                }

                result.StatusCode = Enums.StatusCode.Error;
                result.Message = Constants.Message.SAVED_SUCCESSFULLY;
                result.Result = false;
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
            catch (Exception ex)
            {
                var result = new ObjectResult();
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = ex.Message;
                result.Result = false;
                Logger.CreateLog(Logger.Levels.ERROR, this, "Deletes()",
                    string.Concat("data : ", data), ex.ToString());
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
        }

        [Route("setStatus")]
        [HttpPost]
        public async Task<IHttpActionResult> SetStatus(List<Guid> data)
        {
            try
            {
                if (data == null)
                {
                    data = new List<Guid>();
                }

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = new ObjectResult();
                var returnVal = await _business.SetStatus(data);
                if (returnVal)
                {
                    result.StatusCode = Enums.StatusCode.Ok;
                    result.Message = Constants.Message.SOMETHING_ERROR;
                    result.Result = returnVal;
                    return Ok(result);
                }

                result.StatusCode = Enums.StatusCode.Error;
                result.Message = Constants.Message.SAVED_SUCCESSFULLY;
                result.Result = false;
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
            catch (Exception ex)
            {
                var result = new ObjectResult();
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = ex.Message;
                result.Result = false;
                Logger.CreateLog(Logger.Levels.ERROR, this, "SetStatus()",
                    string.Concat("data : ", data), ex.ToString());
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
        }

        [Route("setVIP")]
        [HttpPost]
        public async Task<IHttpActionResult> SetVIP(List<Guid> data)
        {
            try
            {
                if (data == null)
                {
                    data = new List<Guid>();
                }

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = new ObjectResult();
                var returnVal = await _business.SetVIP(data);
                if (returnVal)
                {
                    result.StatusCode = Enums.StatusCode.Ok;
                    result.Message = Constants.Message.SOMETHING_ERROR;
                    result.Result = returnVal;
                    return Ok(result);
                }

                result.StatusCode = Enums.StatusCode.Error;
                result.Message = Constants.Message.SAVED_SUCCESSFULLY;
                result.Result = false;
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
            catch (Exception ex)
            {
                var result = new ObjectResult();
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = ex.Message;
                result.Result = false;
                Logger.CreateLog(Logger.Levels.ERROR, this, "SetStatus()",
                    string.Concat("data : ", data), ex.ToString());
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
        }

        #endregion
    }
}
