﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using BanNhaAZ.Models;
using CommonHelper.Helper;
using Core.Business;
using Core.Models;
using Core.Models.EF;
using Core.Models.Messages;

namespace BanNhaAZ.Areas.Admin.Controllers
{
    [RoutePrefix("api/constant")]
    public class ConstantController : ApiController
    {
        #region + Constructor
        private readonly ConstantBusiness _business = new ConstantBusiness();
        #endregion

        #region + Function
        /// <summary>
        /// Function search 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [Route("Search")]
        [HttpPost]
        public async Task<IHttpActionResult> Search([FromBody]ListFilter filter)
        {
            try
            {

                if (filter == null)
                {
                    filter = new ListFilter();
                }
                if (filter.Filter == null || filter.Filter.Count == 0)
                {
                    filter.Filter = new List<FilterTypeModel>();
                }
                if (filter.Sort == null || filter.Sort.Count == 0)
                {
                    filter.Sort = new List<SortTypeModel>();
                }
                var filterDataTable = Utilities.ListToDataTable(filter.Filter);
                var sortDataTable = Utilities.ListToDataTable(filter.Sort);
                var skip = filter.Skip ?? 0;
                var take = filter.Take ?? Constants.Config.ItemsPerPage;
                var results = await _business.GetData(filter.Name != null ? filter.Name.Trim() : null, filterDataTable, sortDataTable, skip, take);

                return Ok(new ObjectResult
                {
                    StatusCode = Enums.StatusCode.Ok,
                    Result = results,
                    Message = Constants.Message.GET_DATA_SUCCESSFULLY
                });
            }
            catch (Exception ex)
            {
                var result = new ObjectResult();
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = ex.Message;
                result.Result = false;
                Logger.CreateLog(Logger.Levels.ERROR, this, "Constant/Search()", string.Concat("filter : ", filter), ex.ToString());
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
        }
        [Route("create")]
        [HttpPost]
        public async Task<IHttpActionResult> Create([FromBody] ConstantView data)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = new ObjectResult();
                var returnVal = await _business.Create(data);
                if (returnVal.Status)
                {
                    result.StatusCode = Enums.StatusCode.Ok;
                    result.Message = returnVal.Message;
                    result.Result = returnVal.Status;

                    return Ok(result);
                }
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = returnVal.Message;
                result.Result = false;
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
            catch (Exception ex)
            {
                var result = new ObjectResult();
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = ex.Message;
                result.Result = false;
                Logger.CreateLog(Logger.Levels.ERROR, this, "Create()", string.Concat("data : ", data), ex.ToString());
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
        }
        [Route("update")]
        [HttpPost]
        public async Task<IHttpActionResult> Update([FromBody] ConstantView data)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = new ObjectResult();
                var returnVal = await _business.Update(data);
                if (returnVal.Status)
                {
                    result.StatusCode = Enums.StatusCode.Ok;
                    result.Message = returnVal.Message;
                    result.Result = returnVal.Status;

                    return Ok(result);
                }
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = returnVal.Message;
                result.Result = false;
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
            catch (Exception ex)
            {
                var result = new ObjectResult();
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = ex.Message;
                result.Result = false;
                Logger.CreateLog(Logger.Levels.ERROR, this, "Update()", string.Concat("data : ", data), ex.ToString());
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
        }

        [Route("getConstants")]
        [HttpGet]
        public async Task<IHttpActionResult> GetConstants(string type, int? id)
        {
            try
            {
                //var result = new ObjectResult();
                if (id.HasValue)
                {
                    var result = await _business.GetConstants(type, id.Value);
                    return Ok(new ObjectResult
                    {
                        StatusCode = Enums.StatusCode.Ok,
                        Result = result,
                        Message = Constants.Message.GET_DATA_SUCCESSFULLY
                    });
                }
                var results = await _business.GetConstants(type);
                return Ok(new ObjectResult
                {
                    StatusCode = Enums.StatusCode.Ok,
                    Result = results,
                    Message = Constants.Message.GET_DATA_SUCCESSFULLY
                });
            }
            catch (Exception ex)
            {
                var result = new ObjectResult();
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = ex.Message;
                result.Result = false;
                Logger.CreateLog(Logger.Levels.ERROR, this, "Constant/GetConstants()", string.Concat("type : ", type), ex.ToString());
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
        }

        [Route("getLocation")]
        [HttpGet]
        public async Task<IHttpActionResult> GetLocation(string type, int? id)
        {
            try
            {
                var result = await _business.GetLocation(type, id);
                return Ok(new ObjectResult
                {
                    StatusCode = Enums.StatusCode.Ok,
                    Result = result,
                    Message = Constants.Message.GET_DATA_SUCCESSFULLY
                });
            }
            catch (Exception ex)
            {
                var result = new ObjectResult();
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = ex.Message;
                result.Result = false;
                Logger.CreateLog(Logger.Levels.ERROR, this, "Constant/GetConstants()", string.Concat("type : ", type), ex.ToString());
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
        }


        [Route("getContents")]
        [HttpGet]
        public async Task<IHttpActionResult> GetContents()
        {
            try
            {
                var result = await _business.GetContents();
                return Ok(new ObjectResult
                {
                    StatusCode = Enums.StatusCode.Ok,
                    Result = result,
                    Message = Constants.Message.GET_DATA_SUCCESSFULLY
                });
            }
            catch (Exception ex)
            {
                var result = new ObjectResult();
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = ex.Message;
                result.Result = false;
                Logger.CreateLog(Logger.Levels.ERROR, this, "Constant/GetContents()", string.Empty, ex.ToString());
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
        }
        [Route("updateContents")]
        [HttpPost]
        public async Task<IHttpActionResult> UpdateContents([FromBody] Contents data)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = new ObjectResult();
                var returnVal = await _business.UpdateContents(data);
                if (returnVal.Status)
                {
                    result.StatusCode = Enums.StatusCode.Ok;
                    result.Message = returnVal.Message;
                    result.Result = returnVal.Status;

                    return Ok(result);
                }
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = returnVal.Message;
                result.Result = false;
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
            catch (Exception ex)
            {
                var result = new ObjectResult();
                result.StatusCode = Enums.StatusCode.Error;
                result.Message = ex.Message;
                result.Result = false;
                Logger.CreateLog(Logger.Levels.ERROR, this, "UpdateContents()", string.Concat("data : ", data), ex.ToString());
                return Content(HttpStatusCode.InternalServerError, result, Utilities.MediaTypeFormatterJson);
            }
        }

    }
    #endregion
}
