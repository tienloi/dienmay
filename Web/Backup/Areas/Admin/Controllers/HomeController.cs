﻿using System.Web.Mvc;
using BanNhaAZ.Models;
using CommonHelper.Helper;
using Core.Models;

namespace BanNhaAZ.Areas.Admin.Controllers
{
    public class HomeController : Controller
    {
        private readonly CacheProvider _cacheProvider = new CacheProvider();
        // GET: Admin/Home
        public ActionResult Index()
        {
            var getCache = _cacheProvider.Get(Constants.CacheConstant.User) as UserLogin;
            if (getCache == null)
            {
                return RedirectToAction("Index", "Login", new { area = "" });
            }
            return View();
        }
    }
}