﻿using AutoMapper;
using BanNhaAZ;
using BanNhaAZ.Mapping;
using Core.Models.EF;
using Core.Models.Messages;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Startup))]

namespace BanNhaAZ
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            ConfigureAuth(app);
            //var config = new AutoMapper.MapperConfiguration(cfg =>
            //{
            //    cfg.CreateMap<OrdersView, Orders>();

            //    cfg.CreateMap<ProductView, Product>();
            //});
        }
    }
}
