﻿using System.Web.Mvc;
using System.Web.Routing;

namespace BanNhaAZ
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Login",
                url: "dang-nhap",
                defaults: new { controller = "Login", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "BanNhaAZ.Controllers" }
            );
            routes.MapRoute(
                name: "Product",
                url: "bat-dong-san",
                defaults: new { controller = "Home", action = "Product", id = UrlParameter.Optional },
                namespaces: new[] { "BanNhaAZ.Controllers" }
            );
            routes.MapRoute(
                name: "SendLand",
                url: "ky-gui-bat-dong-san",
                defaults: new { controller = "Home", action = "KyGui", id = UrlParameter.Optional },
                namespaces: new[] { "BanNhaAZ.Controllers" }
            );
            routes.MapRoute(
               name: "TinTuc2",
               url: "tin-tuc/{id}/{name}",
               defaults: new { controller = "Home", action = "TinTucDetail", id = UrlParameter.Optional },
               namespaces: new[] { "BanNhaAZ.Controllers" }
           );
            routes.MapRoute(
             name: "TinTuc",
             url: "tin-tuc",
             defaults: new { controller = "Home", action = "TinTuc", id = UrlParameter.Optional },
             namespaces: new[] { "BanNhaAZ.Controllers" }
         );
            routes.MapRoute(
               name: "Details",
               url: "chi-tiet/{id}/{name}",
               defaults: new { controller = "Home", action = "Details", id = UrlParameter.Optional },
               namespaces: new[] { "BanNhaAZ.Controllers" }
           );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "BanNhaAZ.Controllers" }
            );
        }
    }
}
