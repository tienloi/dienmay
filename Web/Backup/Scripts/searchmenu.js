﻿var url = window.location.origin + '/api/'

$('#typeTransaction').on('change', '', function (e) {
    loadTypeProperty(this.value, 0);
    loadPrice(this.value, 0);
});
$('#city').on('change', '', function (e) {
    loadLocation('VN_Districts', this.value, 0);
    loadLocation('VN_Streets', -1, 0);
    loadLocation('VN_Wards', -1, 0);
});
$('#district').on('change', '', function (e) {
    loadLocation('VN_Streets', this.value, 0);
    loadLocation('VN_Wards', this.value, 0);
});

$('#btnSearch').on('click', '', function (e) {
    var origin = window.location.origin + '?keySearch=' + $("#txtSearch").val();
    var typeTransaction = $("#typeTransaction").find(':selected').val();
    var typeProperty = $("#typeProperty").find(':selected').val();
    var area = $("#area").find(':selected').val();
    var price = $("#price").find(':selected').val();
    var direction = $("#direction").find(':selected').val();
    var city = $("#city").find(':selected').val();
    var district = $("#district").find(':selected').val();
    var ward = $("#ward").find(':selected').val();
    var street = $("#street").find(':selected').val();
    var bedroom = $("#bedroom").find(':selected').val();

    if (typeTransaction != '0') {
        origin += '&type=' + typeTransaction;
    }
    if (typeProperty != '0') {
        origin += '&prop=' + typeProperty;
    }
    if (area != '0') {
        origin += '&area=' + area;
    }
    if (price != '0') {
        origin += '&price=' + price;
    }
    if (direction != '0') {
        origin += '&direction=' + direction;
    }
    if (city != '0') {
        origin += '&city=' + city;
    }
    if (district != '0') {
        origin += '&district=' + district;
    }
    if (ward != '0') {
        origin += '&ward=' + ward;
    }
    if (street != '0') {
        origin += '&street=' + street;
    }
    if (bedroom != '0') {
        origin += '&bedroom=' + bedroom;
    }
    window.location.href = origin;
})

//getUrlParameter
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

function loadTypeTransaction(valueSelected) {
    $.ajax({
        type: "GET",
        url: url + 'constant/getConstants',
        data: {
            type: 'TypeTransaction',
            id: null
        },
        success: function (res) {
            var sel = $('#typeTransaction');
            sel.html('');
            sel.append('<option value="0">Loại giao dịch</option>');
            $.each(res.result, function (key, item) {
                if (valueSelected == item.id) {
                    sel.append('<option value="' + item.id + '" selected>' + item.name + '</option>');
                } else {
                    sel.append('<option value="' + item.id + '">' + item.name + '</option>');
                }

            });
        },
        error: function (xhr) {

        }
    });
}
function loadDirection(valueSelected) {
    $.ajax({
        type: "GET",
        url: url + 'constant/getConstants',
        data: {
            type: 'Direction',
            id: null
        },
        success: function (res) {
            var sel = $('#direction');
            $.each(res.result, function (key, item) {
                if (valueSelected == item.id) {
                    sel.append('<option value="' + item.id + '" selected>' + item.name + '</option>');
                } else {
                    sel.append('<option value="' + item.id + '">' + item.name + '</option>');
                }
            });
        },
        error: function (xhr) {

        }
    });
}
function loadTypeProperty(idTransaction, valueSelected) {
    $.ajax({
        type: "GET",
        url: url + 'constant/getConstants',
        data: {
            type: 'TypeProperty',
            id: idTransaction
        },
        success: function (res) {
            var sel = $('#typeProperty');
            sel.html('');
            sel.append('<option value="0">Loại bất động sản</option>');
            $.each(res.result, function (key, item) {
                if (valueSelected == item.id) {
                    sel.append('<option value="' + item.id + '" selected>' + item.name + '</option>');
                } else {
                    sel.append('<option value="' + item.id + '">' + item.name + '</option>');
                }
            });
        },
        error: function (xhr) {

        }
    });
}
function loadPrice(idTransaction, valueSelected) {
    //Gias
    $.ajax({
        type: "GET",
        url: url + 'constant/getConstants',
        data: {
            type: 'PriceTypeTransaction',
            id: idTransaction
        },
        success: function (res) {
            var sel = $('#price');
            sel.html('');
            sel.append('<option value="0">Giá</option>');
            $.each(res.result, function (key, item) {
                if (valueSelected == item.id) {
                    sel.append('<option value="' + item.id + '" selected>' + item.name + '</option>');
                } else {
                    sel.append('<option value="' + item.id + '">' + item.name + '</option>');
                }
            });
        },
        error: function (xhr) {

        }
    });
}

//Location
function loadLocation(type, id, valueSelected) {
    $.ajax({
        type: "GET",
        url: url + 'constant/getLocation',
        data: {
            type: type,
            id: id
        },
        success: function (res) {
            var sel;
            if (type == 'VN_City') {
                sel = $('#city');
                sel.html('');
            }
            if (type == 'VN_Districts') {
                sel = $('#district');
                sel.html('');
                sel.append('<option value="0">Quận/Huyện</option>');
            }
            if (type == 'VN_Wards') {
                sel = $('#ward');
                sel.html('');
                sel.append('<option value="0">Phường/Xã</option>');
            }
            if (type == 'VN_Streets') {
                sel = $('#street');
                sel.html('');
                sel.append('<option value="0">Đường/Phố</option>');
            }
            $.each(res.result, function (key, item) {
                if (valueSelected == item.id) {
                    sel.append('<option value="' + item.id + '" selected>' + item.name + '</option>');
                } else {
                    sel.append('<option value="' + item.id + '">' + item.name + '</option>');
                }
            });
        },
        error: function (xhr) {

        }
    });
}
//end Location

function init() {
    var keySearch = getUrlParameter('keySearch');
    var typeTransaction = getUrlParameter('type');
    var typeProperty = getUrlParameter('prop');
    var area = getUrlParameter('area');
    var bedroom = getUrlParameter('bedroom');
    var price = getUrlParameter('price');
    var direction = getUrlParameter('direction');
    var city = getUrlParameter('city');
    var district = getUrlParameter('district');
    var ward = getUrlParameter('ward');
    var street = getUrlParameter('street');


    if (keySearch != '') {
        $("#txtSearch").val(keySearch);
    }
    if (typeTransaction != undefined) {
        loadTypeTransaction(parseInt(typeTransaction));
        loadTypeProperty(parseInt(typeTransaction), 0);
        loadPrice(parseInt(typeTransaction), 0);
    }
    else {
        loadTypeTransaction(0);
    }

    if (typeProperty != undefined) {
        loadTypeProperty(parseInt(typeTransaction), parseInt(typeProperty));
    }
    //else {
    //    loadTypeProperty(parseInt(typeTransaction), 0);
    //}

    if (area != undefined) {
        $("#area").val(parseInt(area))
    }
    if (bedroom != undefined) {
        $("#bedroom").val(parseInt(bedroom))
    }
    if (price != undefined) {
        loadPrice(parseInt(typeTransaction), parseInt(price));
    }
    //else {
    //    loadPrice(parseInt(typeTransaction), 0);
    //}

    if (direction != undefined) {
        loadDirection(parseInt(direction));
    } else {
        loadDirection(0);
    }
    //Location
    if (city != undefined) {
        loadLocation('VN_City', null, parseInt(city));
        loadLocation('VN_Districts', parseInt(city), 0);
    } else {
        loadLocation('VN_City', null, 1);
        loadLocation('VN_Districts', 1, 0);
    }
    if (district != undefined) {
        loadLocation('VN_Districts', parseInt(city), parseInt(district));
        loadLocation('VN_Wards', parseInt(district), 0);
        loadLocation('VN_Streets', parseInt(district),0);
    }
    //} else {
    //    loadLocation('VN_Districts', parseInt(city), 0);
    //}
    if (ward != undefined) {
        loadLocation('VN_Wards', parseInt(district), parseInt(ward));
    }
    if (street != undefined) {
        loadLocation('VN_Streets', parseInt(district), parseInt(street));
    }
    //else {
    //    loadLocation('VN_Wards', parseInt(city), 0);
    //}
    //end Location
}

init();

