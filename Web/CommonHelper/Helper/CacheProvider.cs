﻿using System;
using System.Runtime.Caching;

namespace CommonHelper.Helper
{
    public class CacheProvider
    {
        private ObjectCache Cache { get { return MemoryCache.Default; } }

        public object Get(string key)
        {
            return Cache[key];
        }

        public void Set(string key, object data, int cacheTime)
        {
            CacheItemPolicy policy = new CacheItemPolicy();
            policy.AbsoluteExpiration = DateTime.Now + TimeSpan.FromHours(cacheTime);
            if (data != null) Cache.Add(new CacheItem(key, data), policy);
        }

        public bool IsSet(string key)
        {
            return (Cache[key] != null);
        }

        public void Remove(string key)
        {
            Cache.Remove(key);
        }
    }
}
