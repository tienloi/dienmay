﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace CommonHelper.Helper
{
    public class Ultity
    {
        public static int GenerateRandom(int min, int max)
        {
            var seed = Convert.ToInt32(Regex.Match(Guid.NewGuid().ToString(), @"\d+").Value);
            return new Random(seed).Next(min, max);
        }
        public static string GenerateRandomNumberDate()
        {
            var now = DateTime.Now;
            string result = "";
            result += now.ToString("ddMMhhmmss");
            return result;
        }
        public static class ConvertNameUrl
        {
            public static string CovnertName(string name)
            {
                if (!string.IsNullOrEmpty(name))
                {
                    name = name.ToLower().Trim();
                    name = name.Replace(".", "");
                    Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
                    string temp = name.Normalize(NormalizationForm.FormD);
                    name = regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
                    name = Regex.Replace(name, @"[^0-9a-zA-Z:,]+", "-");
                    name = name.Replace(":", "-");
                    if (name.StartsWith("-"))
                    {
                        name = name.Substring(1);
                    }

                    if (name.EndsWith("-"))
                    {
                        name = name.Substring(0, name.Length - 1);
                    }

                    while (name.Contains("--"))
                    {
                        name = name.Replace("--", "-");
                    }
                }

                return name;
            }
        }
        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

    }
}
