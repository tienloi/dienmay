﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace CommonHelper.Helper
{
    public class ConvertHelper
    {
        public static class ConvertNameUrl
        {
            public static string ConvertName(string name)
            {
                if (!string.IsNullOrEmpty(name))
                {
                    name = name.ToLower().Trim();
                    name = name.Replace(".", "");
                    Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
                    string temp = name.Normalize(NormalizationForm.FormD);
                    name = regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
                    name = Regex.Replace(name, @"[^0-9a-zA-Z:,]+", "-");
                    name = name.Replace(":", "-");
                    if (name.StartsWith("-"))
                    {
                        name = name.Substring(1);
                    }

                    if (name.EndsWith("-"))
                    {
                        name = name.Substring(0, name.Length - 1);
                    }

                    while (name.Contains("--"))
                    {
                        name = name.Replace("--", "-");
                    }
                }

                return name;
            }
        }
    }
}
