﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using CommonHelper.Helper;
using Core.Models;

namespace CommonHelper.DataContext
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly BoopAppContext _dataContext;

        public Repository()
        {
            _dataContext = new BoopAppContext();
        }

        #region IRepository<T> Members

        /// <summary>
        /// Insert entity in DataBase
        /// </summary>
        /// <param name="entity">entity</param>
        /// <returns>Boolean</returns>
        public async Task<bool> Insert(T entity)
        {
            try
            {
                _dataContext.Set<T>().Add(entity);
                return await _dataContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Insert(IList<T> list)", ex);
                return false;
            }
        }

        /// <summary>
        /// Insert entity to Database and return entify to keep track new inserted Id
        /// </summary>
        /// <param name="entity"></param>
        /// <returns>entify to keep track new inserted Id</returns>
        public async Task<T> InsertAndGetData(T entity)
        {
            try
            {
                _dataContext.Set<T>().Add(entity);
                bool result = await _dataContext.SaveChangesAsync() > 0;
                if (result)
                {
                    return entity;
                }
                else return null;
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "InsertAndGetData(T entity)", ex);
                return null;
            }
        }

        /// <summary>
        /// Insert multiple rows to database
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public async Task<bool> InsertRange(IList<T> list)
        {
            try
            {
                _dataContext.Set<T>().AddRange(list);
                return await _dataContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "InsertRange(IList<T> list)", ex);
                return false;
            }
        }

        /// <summary>
        /// Update entity in DataBase
        /// </summary>
        /// <param name="entity">entity</param>
        /// <param name="propertyNamesNotChanged">Property names not change</param>
        /// <returns>Boolean</returns>
        public async Task<bool> Edit(T entity, IList<string> propertyNamesNotChanged = null)
        {
            try
            {
                _dataContext.Set<T>().Attach(entity);
                _dataContext.Entry(entity).State = EntityState.Modified;
                if (propertyNamesNotChanged != null)
                {
                    var ignoredProperties = BoopAppContext.IgnoredProperties;
                    var properties = ignoredProperties != null ? ignoredProperties[typeof(T)] : null;
                    var propertyNames = entity.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(x => ignoredProperties == null || properties == null || !properties.Contains(x));

                    var propertyNotChangedNames = from x in propertyNames
                                                  where propertyNamesNotChanged.Contains(x.Name)
                                                  select x.Name;
                    foreach (var propertyName in propertyNotChangedNames)
                    {
                        _dataContext.Entry(entity).Property(propertyName).IsModified = false;
                    }
                }

                return await _dataContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Edit(T entity, IList<string> notChangedPropertyNames = null)", ex);
                return false;
            }
        }

        /// <summary>
        /// Update entity in DataBase
        /// </summary>
        /// <param name="entitys"> List entity Update</param>
        /// <param name="propertyNamesNotChanged">Property names not change</param>
        /// <param name="checkError">Check Error</param>
        /// <returns></returns>
        public async Task<bool> Edit(IList<T> entitys, IList<string> propertyNamesNotChanged = null)
        {
            try
            {
                foreach (var entity in entitys)
                {
                    _dataContext.Set<T>().Attach(entity);
                    _dataContext.Entry(entity).State = EntityState.Modified;
                    if (propertyNamesNotChanged != null)
                    {
                        var ignoredProperties = BoopAppContext.IgnoredProperties;
                        var properties = ignoredProperties != null ? ignoredProperties[typeof(T)] : null;
                        var propertyNames = entity.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(x => ignoredProperties == null || properties == null || !properties.Contains(x));

                        var propertyNotChangedNames = from x in propertyNames
                                                      where propertyNamesNotChanged.Contains(x.Name)
                                                      select x.Name;
                        foreach (var propertyName in propertyNotChangedNames)
                        {
                            _dataContext.Entry(entity).Property(propertyName).IsModified = false;
                        }
                    }
                }
                return await _dataContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Edit(IList<T> entitys, IList<string> propertyNamesNotChanged = null, Ref<CheckError> checkError = null)", ex);
                return false;
            }
        }

        /// <summary>
        /// Delete entity in DataBase
        /// </summary>
        /// <param name="entity">entity</param>
        /// <returns>Boolean</returns>
        public async Task<bool> Delete(T entity)
        {
            try
            {
                //Type type = entity.GetType();
                //type.GetProperty("IsDeleted").SetValue(entity, true, null);
                //_dataContext.Set<T>().Remove(entity);
                if (entity != null)
                {
                    _dataContext.Set<T>().Remove(entity);
                    return await _dataContext.SaveChangesAsync() > 0;
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Delete(T entity)", ex);
                return false;
            }
        }

        /// <summary>
        /// Delete model in DataBase
        /// </summary>
        /// <param name="id">Key in entity</param>
        /// <returns>Boolean</returns>
        public async Task<bool> Delete(object id)
        {
            try
            {
                T entity = await GetById(id);
                if (entity != null)
                {
                    _dataContext.Set<T>().Remove(entity);
                    return await _dataContext.SaveChangesAsync() > 0;
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Delete(object id)", ex);
                return false;
            }
        }

        /// <summary>
        /// Delete multiple records
        /// </summary>
        /// <param name="list">list entity</param>
        /// <returns>Boolean</returns>
        public async Task<bool> DeleteAll(IList<T> list)
        {
            try
            {
                _dataContext.Set<T>().RemoveRange(list);
                return await _dataContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "DeleteAll(IList<T> list)", ex);
                return false;
            }
        }

        /// <summary>
        /// Get list entity by condition
        /// </summary>
        /// <param name="predicate">condition</param>
        /// <returns>list entity</returns>
        public async Task<IEnumerable<T>> Get(Expression<Func<T, bool>> predicate)
        {
            try
            {
                return await _dataContext.Set<T>().Where(predicate).ToListAsync();
            }
            catch (Exception ex)
            {
                Helper.Logger.CreateLog(Logger.Levels.ERROR, this, "Get(Expression<Func<T, bool>> predicate)", ex);
                return null;
            }
        }

        public async Task<bool> CheckExist(Expression<Func<T, bool>> predicate)
        {
            return await _dataContext.Set<T>().AnyAsync(predicate);
        }

        /// <summary>
        /// Get list entity by condition
        /// </summary>
        /// <param name="predicate">condition</param>
        /// <param name="fieldOrderBy">field order by</param>
        /// <param name="ascending">sort</param>
        /// <returns>list entity</returns>
        public async Task<IEnumerable<T>> Get(Expression<Func<T, bool>> predicate, string fieldOrderBy, bool ascending)
        {
            try
            {
                var p = typeof(T).GetProperty(fieldOrderBy);
                var t = p.PropertyType;
                if (t == typeof(int))
                {
                    var pe = Expression.Parameter(typeof(T), "p");
                    var expr1 = Expression.Lambda<Func<T, int>>(Expression.Property(pe, fieldOrderBy), pe);
                    return await (ascending
                        ? _dataContext.Set<T>().Where(predicate).OrderBy(expr1).ToListAsync()
                        : _dataContext.Set<T>().Where(predicate).OrderByDescending(expr1).ToListAsync());
                }
                else
                {
                    if (t == typeof(int?))
                    {
                        var pe = Expression.Parameter(typeof(T), "p");
                        var expr1 = Expression.Lambda<Func<T, int?>>(Expression.Property(pe, fieldOrderBy), pe);
                        return await (ascending
                            ? _dataContext.Set<T>().Where(predicate).OrderBy(expr1).ToListAsync()
                            : _dataContext.Set<T>().Where(predicate).OrderByDescending(expr1).ToListAsync());
                    }
                    else
                    {
                        var pe = Expression.Parameter(typeof(T), "p");
                        var expr1 = Expression.Lambda<Func<T, String>>(Expression.Property(pe, fieldOrderBy), pe);
                        return await (ascending
                            ? _dataContext.Set<T>().Where(predicate).OrderBy(expr1).ToListAsync()
                            : _dataContext.Set<T>().Where(predicate).OrderByDescending(expr1).ToListAsync());
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Get(Expression<Func<T, bool>> predicate, string fieldOrderBy, bool ascending)", ex);
                return null;
            }
        }

        /// <summary>
        /// Get list entity by condition
        /// </summary>
        /// <param name="predicate">condition</param>
        /// <param name="fieldOrderBy">field order by</param>
        /// <param name="ascending">sort</param>
        /// <param name="skip">skip</param>
        /// <param name="take">take</param>
        /// <returns>list entity</returns>
        public async Task<IEnumerable<T>> Get(Expression<Func<T, bool>> predicate, string fieldOrderBy, bool ascending, int skip, int take)
        {
            try
            {
                var p = typeof(T).GetProperty(fieldOrderBy);
                var t = p.PropertyType;
                if (t == typeof(int))
                {
                    var pe = Expression.Parameter(typeof(T), "p");
                    var expr1 = Expression.Lambda<Func<T, int>>(Expression.Property(pe, fieldOrderBy), pe);
                    return await (ascending ? _dataContext.Set<T>().Where(predicate).OrderBy(expr1).Skip(skip).Take(take).ToListAsync() : _dataContext.Set<T>().Where(predicate).OrderByDescending(expr1).Skip(skip).Take(take).ToListAsync());
                }
                else
                if (t == typeof(DateTime))
                {
                    var pe = Expression.Parameter(typeof(T), "p");
                    var expr1 = Expression.Lambda<Func<T, DateTime>>(Expression.Property(pe, fieldOrderBy), pe);
                    return await (ascending ? _dataContext.Set<T>().Where(predicate).OrderBy(expr1).Skip(skip).Take(take).ToListAsync() : _dataContext.Set<T>().Where(predicate).OrderByDescending(expr1).Skip(skip).Take(take).ToListAsync());
                }
                else
                {
                    var pe = Expression.Parameter(typeof(T), "p");
                    var expr1 = Expression.Lambda<Func<T, string>>(Expression.Property(pe, fieldOrderBy), pe);
                    return await (ascending ? _dataContext.Set<T>().Where(predicate).OrderBy(expr1).Skip(skip).Take(take).ToListAsync() : _dataContext.Set<T>().Where(predicate).OrderByDescending(expr1).Skip(skip).Take(take).ToListAsync());
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Get(Expression<Func<T, bool>> predicate, string fieldOrderBy, bool ascending, int skip, int take)", ex);
                return null;
            }
        }

        /// <summary>
        ///  Get list entity by condition
        /// </summary>
        /// <param name="predicate">condition</param>
        /// <param name="groupBy">group by</param>
        /// <param name="fieldOrderBy">field order by</param>
        /// <param name="ascending">sort</param>
        /// <param name="take">take</param>
        /// <returns>list entity by condition</returns>
        public async Task<IEnumerable<T>> Get(Expression<Func<T, bool>> predicate, string groupBy, string fieldOrderBy, bool ascending, int take)
        {
            try
            {
                var p = typeof(T).GetProperty(fieldOrderBy);
                var t = p.PropertyType;
                if (t == typeof(int))
                {
                    var pe = Expression.Parameter(typeof(T), "p");
                    var expr1 = Expression.Lambda<Func<T, int>>(Expression.Property(pe, fieldOrderBy), pe);

                    var fieldXExpression = Expression.Property(pe, groupBy);
                    var lambda = Expression.Lambda<Func<T, object>>(
                        fieldXExpression,
                        pe);
                    if (ascending)
                    {
                        var data = await _dataContext.Set<T>().Where(predicate).OrderBy(expr1).GroupBy(lambda).Select(x => x.ToList().Take(take)).ToListAsync();
                        var list = new List<T>();
                        foreach (var item in data)
                        {
                            list.AddRange(item);
                        }
                        return list;
                    }
                    else
                    {
                        var data = await _dataContext.Set<T>().Where(predicate).OrderByDescending(expr1).GroupBy(lambda).Select(x => x.ToList().Take(take)).ToListAsync();
                        var list = new List<T>();
                        foreach (var item in data)
                        {
                            list.AddRange(item);
                        }
                        return list;
                    }
                }
                else
                {
                    var pe = Expression.Parameter(typeof(T), "p");
                    var expr1 = Expression.Lambda<Func<T, string>>(Expression.Property(pe, fieldOrderBy), pe);
                    var fieldXExpression = Expression.Property(pe, groupBy);
                    var lambda = Expression.Lambda<Func<T, object>>(
                        fieldXExpression,
                        pe);
                    if (ascending)
                    {
                        var data = await _dataContext.Set<T>().Where(predicate).OrderBy(expr1).GroupBy(lambda).Select(x => x.ToList().Take(take)).ToListAsync();
                        var list = new List<T>();
                        foreach (var item in data)
                        {
                            list.AddRange(item);
                        }
                        return list;
                    }
                    else
                    {
                        var data = await _dataContext.Set<T>().Where(predicate).OrderByDescending(expr1).GroupBy(lambda).Select(x => x.ToList().Take(take)).ToListAsync();
                        var list = new List<T>();
                        foreach (var item in data)
                        {
                            list.AddRange(item);
                        }
                        return list;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Get(Expression<Func<T, bool>> predicate, string groupBy, string fieldOrderBy, bool ascending, int take)", ex);
                return null;
            }
        }

        /// <summary>
        /// Get count list entity by condition
        /// </summary>
        /// <param name="predicate">condition</param>
        /// <returns>count list entity by condition </returns>
        public async Task<int> GetCount(Expression<Func<T, bool>> predicate)
        {
            try
            {
                return await _dataContext.Set<T>().Where(predicate).CountAsync();
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "GetCount(Expression<Func<T, bool>> predicate)", ex);
                return -1;
            }
        }

        /// <summary>
        /// Get all entity
        /// </summary>
        /// <returns>list entity</returns>
        public async Task<IEnumerable<T>> GetAll()
        {
            try
            {
                return _dataContext.Set<T>().AsQueryable();
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "GetAll()", ex);
                return null;
            }
        }

        public IQueryable<T> GetAllIQueryable()
        {
            try
            {
                return _dataContext.Set<T>().AsQueryable();
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "GetAll()", ex);
                return null;
            }
        }

        /// <summary>
        /// Get entity
        /// </summary>
        /// <param name="id">Key in entity</param>
        /// <returns>entity</returns>
        public async Task<T> GetById(object id)
        {
            try
            {
                return await _dataContext.Set<T>().FindAsync(id);
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "GetById(object id)", ex);
                return null;
            }
        }

        /// <summary>
        /// Get entity
        /// </summary>
        /// <param name="predicate">condition</param>
        /// <returns>entity</returns>
        public async Task<T> GetOne(Expression<Func<T, bool>> predicate)
        {
            try
            {
                return await _dataContext.Set<T>().Where(predicate).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "GetOne(Expression<Func<T, bool>> predicate)", ex);
                return null;
            }
        }

        /// <summary>
        /// Get list entity
        /// </summary>
        /// <param name="storedProcedureName">stored procedure name</param>
        /// <param name="parameters">parameters input</param>
        /// <returns>list entity</returns>
        public async Task<IEnumerable<T>> Get(string storedProcedureName, params object[] parameters)
        {
            try
            {
                if (parameters != null)
                {
                    var query = string.Concat("Exec ", storedProcedureName, " ");

                    foreach (var item in parameters)
                    {
                        var itemObject = (SqlParameter)item;

                        if (itemObject.Direction == ParameterDirection.Output)
                        {
                            query += string.Concat(itemObject.ParameterName, " OUT,");
                        }
                        else
                        {
                            query += string.Concat(itemObject.ParameterName, ",");
                        }
                    }
                    query = parameters.Length > 0 ? query.Substring(0, query.Length - 1) : storedProcedureName;

                    return await _dataContext.Database.SqlQuery<T>(query, parameters).ToListAsync();
                }
                else
                {
                    return await _dataContext.Database.SqlQuery<T>(storedProcedureName).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Get(string storedProcedureName, params object[] parameters)", ex);
                return null;
            }
        }
        public async Task<IEnumerable<T>> ExecQuery(string queryString)
        {
            try
            {
                return await _dataContext.Database.SqlQuery<T>(queryString).ToListAsync();
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "ExecQuery(string queryString)", ex);
                return null;
            }
        }

        /// <summary>
        /// Get entity
        /// </summary>
        /// <param name="storedProcedureName">stored procedure name</param>
        /// <param name="parameters">parameters[]</param>
        /// <returns>entity</returns>
        public async Task<T> GetOne(string storedProcedureName, params object[] parameters)
        {
            try
            {
                if (parameters != null)
                {
                    var query = string.Concat("Exec ", storedProcedureName, " ");

                    foreach (var item in parameters)
                    {
                        var itemObject = (SqlParameter)item;
                        query += string.Concat(itemObject.ParameterName, ",");
                    }
                    query = parameters.Length > 0 ? query.Substring(0, query.Length - 1) : storedProcedureName;

                    return await _dataContext.Database.SqlQuery<T>(query, parameters).FirstOrDefaultAsync();
                }
                else
                {
                    return await _dataContext.Database.SqlQuery<T>(storedProcedureName).FirstOrDefaultAsync();
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Get(string storedProcedureName, params object[] parameters)", ex);
                return null;
            }
        }

        /// <summary>
        /// Get Out Put
        /// </summary>
        /// <param name="storedProcedureName">stored procedure name</param>
        /// <param name="parameters">parameters[]</param>
        /// <returns>List SqlParameter</returns>
        public async Task<IEnumerable<SqlParameter>> GetOutPut(string storedProcedureName, params object[] parameters)
        {
            try
            {
                if (parameters != null)
                {
                    var query = string.Concat("", storedProcedureName, " ");

                    var listParameterOutPut = new List<SqlParameter>();

                    foreach (var item in parameters)
                    {
                        var itemObject = (SqlParameter)item;

                        if (itemObject.Direction == ParameterDirection.Output)
                        {
                            listParameterOutPut.Add(itemObject);
                            query += string.Concat(itemObject.ParameterName, " OUT,");
                        }
                        else
                        {
                            query += string.Concat(itemObject.ParameterName, ",");
                        }
                    }
                    query = parameters.Length > 0 ? query.Substring(0, query.Length - 1) : storedProcedureName;

                    await _dataContext.Database.ExecuteSqlCommandAsync(query, parameters);

                    return listParameterOutPut;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Get(string storedProcedureName, params object[] parameters)", ex);
                return null;
            }
        }

        public int Insert(T entity, string identityColumn)
        {
            try
            {
                _dataContext.Set<T>().Add(entity);
                _dataContext.SaveChanges();

                if (!string.IsNullOrEmpty(identityColumn))
                {
                    Type type = entity.GetType();
                    return (int)type.GetProperty(identityColumn).GetValue(entity);
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Insert(T entity, int id)", ex);
                throw;
            }
        }

        public void Update(T entity)
        {
            try
            {
                _dataContext.Set<T>().Attach(entity);
                _dataContext.Entry(entity).State = EntityState.Modified;
                _dataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.CreateLog(Logger.Levels.ERROR, this, "Update(T entity)", ex);
                throw;
            }
        }



        #endregion IRepository<T> Members

    }

}
