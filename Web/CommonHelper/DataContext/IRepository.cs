﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CommonHelper.DataContext
{
    public interface IRepository<T>
    {
        Task<bool> Insert(T entity);
        Task<T> InsertAndGetData(T entity);
        Task<bool> InsertRange(IList<T> list);
        Task<bool> Delete(T entity);
        Task<bool> DeleteAll(IList<T> list);
        Task<IEnumerable<T>> Get(Expression<Func<T, bool>> predicate);
        Task<IEnumerable<T>> Get(Expression<Func<T, bool>> predicate, string fieldOrderBy, bool ascending, int skip, int take);
        Task<IEnumerable<T>> Get(Expression<Func<T, bool>> predicate, string groupBy, string fieldOrderBy, bool ascending, int take);
        Task<IEnumerable<T>> Get(Expression<Func<T, bool>> predicate, string fieldOrderBy, bool ascending);
        Task<IEnumerable<T>> GetAll();
        Task<T> GetById(object id);
        Task<IEnumerable<T>> Get(string query, params object[] parameters);
        Task<T> GetOne(string storedProcedureName, params object[] parameters);
        Task<IEnumerable<SqlParameter>> GetOutPut(string storedProcedureName, params object[] parameters);
        int Insert(T entity, string identityColumn);
        void Update(T entity);
    }
}
